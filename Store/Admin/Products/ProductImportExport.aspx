﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Products_ProductImportExport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .leftBlockdivgrey {
            background: url("../images/UI/exprarror.png") no-repeat scroll right 118px transparent;
            border-right: 1px solid #CCC;
            float: left;
            padding-right: 22px;
            width: 60%;
        }

        .greyBLock {
            background: none repeat scroll 0px 0px #F2F2F2;
            padding: 20px 10px;
        }
    </style>
    <div class="container">
        <ul class="breadcrumb">
           <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Products </a></li>
            <li>Import / Export Products Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container mainContainer ">
            <div class="wrap_container">
                <div class="content ">

                    <div class="col-md-12 relative_data">
                        <h3>Import / Export Products
                        </h3>
                    </div>

                    <div class="colormanagmentBlock">
                        <div class="sucessfully" id="dlStatus" runat="server" visible="false">
                            <asp:Literal ID="ltrMessage" runat="server" />
                        </div>
                        <!-- search customer according start -->
                        <div class="greyBLock">
                            <div class="searchInnerbox clearfix">
                                <div class="leftBlockdivgrey  leftBlockdivgreynew" style="width: 47%; float: left;">
                                    <h2>Excel</h2>
                                    <p class="spaceextra">
                                        Import Products Browse and select the excel file featuring the product details you
                            want to add to the database.
                                    </p>
                                    <div class="bottombtnblock">

                                        <asp:FileUpload ID="flupldProductExcel" CssClass="fileup" runat="server" />
                                        <asp:Button ID="btnSubmit" Value="Upload Product" runat="server" CssClass="btn save import_new"
                                            Text="Import" ValidationGroup="Validate" OnClientClick="ShowLoader();" OnClick="btnSubmit_Click" />
                                    </div>

                                    <span class="infrtext">(.xls or .xlsx file - upto
                            <asp:Label ID="lblMaxFileSize" runat="server"></asp:Label> MB )</span>
                                </div>
                                <div style="width: 47%; float: left; margin-left: 20px;">
                                    <h2>Export Products</h2>
                                    <p class="spaceextra">
                                        Select by category the products you wish to export to an excel file.
                                    </p>
                                      <!-- Changes done By snehal 20 09 2016 -->
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlCategory" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                        </asp:DropDownList>
                                         <asp:HiddenField ID="HIDSelectCatId" runat="server" />
                                    </div>
                                      <!-- End --!>

                                   <!-- Added By snehal 20 09 2016 -->
                                    <div class="select-style selectpicker" id="divSubCategory" runat="server" visible="false">
                                        <asp:DropDownList ID="ddlSubCategory" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSubCategory_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <!-- End --!>

                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlLanguage" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="clearfix"></div>
                                    <asp:Button ID="btnExportProducts" Text="Export Product" runat="server" CssClass="upbtnlod save product_export01" OnClick="btnExportProducts_Click" />
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <!-- search customer according end-->
                        <div class="uphistroyBLock">
                            <h1>Uploaded excel history</h1>
                        </div>
                    </div>
                    <section class="all_customer">
                        <asp:GridView ID="gvUploadedHistory" runat="server" AutoGenerateColumns="false" Width="100%"
                            class="colormangTble colormangTbleEmport" AllowPaging="True" OnPageIndexChanging="gvUploadedHistory_PageIndexChanging"
                            EmptyDataText="No record found." AllowSorting="false"
                            OnRowDataBound="gvUploadedHistory_RowDataBound" PageSize="10">
                            <Columns>
                                <asp:TemplateField HeaderText="Uploaded File" HeaderStyle-CssClass="colorsecond">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkUploadedFile" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "UploadedFileName") %>'
                                            OnClick="lnkUploadedFile_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Uploaded On" HeaderStyle-CssClass="colorthird colorthirdwidth" SortExpression="CreatedDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("ImportDate", "{0:dd-MM-yyyy}") %>'></asp:Label>
                                        <%--  <%# Utilities.Common.ConvertDateTimeToCurrentFormat(Convert.ToString(DataBinder.Eval(Container.DataItem, "CreatedDate")))%>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Total records" HeaderStyle-CssClass="colorfourth colorfourthwidth"
                                    DataField="TotalRecords" SortExpression="TotalRecords">
                                    <ItemStyle />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Failed records" HeaderStyle-CssClass="colorfifth colorfifthwidth"
                                    DataField="FailedRecords" SortExpression="FailedRecords">
                                    <ItemStyle />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Error File" HeaderStyle-CssClass="colorsixth colorsixthwidth">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkErrorFile" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ErrorFileName") %>'
                                            OnClick="lnkErrorFile_Click"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                            <PagerStyle CssClass="paging" />
                        </asp:GridView>
                    </section>
                    <!--mainOutContainer start-->


                </div>
            </div>
        </div>
    </div>
</asp:Content>

