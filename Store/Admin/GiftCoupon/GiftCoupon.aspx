﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_GiftCoupon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link rel="stylesheet" type="text/css" href="../../JS/bootstrap-datetimepicker.js" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <link href="../CSS/ui.theme.css" rel="stylesheet" />
    <style type="text/css">
        .pager span {
            color: #009900;
            font-weight: bold;
            font-size: 16pt;
        }

        .classWidthFull {
            width: 100%;
        }

        .row.paginationBlk {
            border: 1px solid #ccc;
            margin: 10px 0;
            padding: 10px 0 5px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#<%= txtStartDate.ClientID  %>").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#<%= txtEndDate.ClientID  %>").datepicker({ dateFormat: 'yy-mm-dd' });
        });
        function ValidateTextbox(clientId) {
            var obj = document.getElementById(clientId);
            var iChars = "!@#$%^\"&+=[]{}|:<>?"
            //alert(obj.value.length);
            //"!@#$%^&*()+=-[]\\\';,./{}|\":<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Search Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }
        //function ClearOtherRegion() {
        //    debugger;
        //    if (document.getElementById("ContentPlaceHolder1_chkRegion_3").checked == true) {
        //        document.getElementById("ContentPlaceHolder1_chkRegion_0").checked = false;
        //        document.getElementById("ContentPlaceHolder1_chkRegion_1").checked = false;
        //        document.getElementById("ContentPlaceHolder1_chkRegion_2").checked = false;
        //    }

        //}
        function checkAll() {
            var checked_checkboxes = $("[id*=chkRegion] input:checked");
            var message = "";
            checked_checkboxes.each(function () {
                var value = $(this).val();
                if (value == 4)
                {
                    var selectedValues = [];
                    selectedValues.push($(this).val());
                    for (var i = 0; i < selectedValues.length; i++) {
                        if (selectedValues.length>0) {
                            checked_checkboxes[i].checked = false;
                        }
                        alert(checked_checkboxes.length);
                    }
                }
                //var text = $(this).closest("td").find("label").html();
                //message += "Text: " + text + " Value: " + value;
                //message += "\n";
            });
            return false;
        };
    </script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings</a></li>
            <li>Manage Promo Code</li>
        </ul>

    </div>
    <div class="admin_page">
        <div class="container mainContainer ">
            <div class="wrap_container gift_cupon">
                <div class="content ">
                    <div class="row ">
                        <div class="col-md-12 relative_data">
                            <h3>Manage Promo Code
                            </h3>

                        </div>

                        <asp:Panel ID="pnlAdminAddEdit" runat="server">
                            <div class="col-md-4 ">

                                <asp:Label ID="lblCouponName" runat="server" Text="Promo Code :" CssClass="LabelText"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:TextBox ID="txtCouponName" CssClass="classWidthFull" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtCouponName" runat="server" ControlToValidate="txtCouponName"
                                    ErrorMessage="Please enter the Coupon Code." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regtxtCouponName" runat="server" ControlToValidate="txtCouponName"
                                    ErrorMessage="Only AlpaNumeric are allowed in the Coupon Code." ValidationExpression="^[a-zA-Z0-9- ]+$"
                                    Display="None" SetFocusOnError="true" ValidationGroup="Add" CssClass="text-danger"></asp:RegularExpressionValidator>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="lbl1" runat="server" CssClass="LabelText" Text="Minimum / Maximum Order Value:"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <%--<asp:TextBox ID="txtMinOrder" CssClass="classWidthFull" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtMinOrder" runat="server" ControlToValidate="txtMinOrder"
                                    ErrorMessage="Please enter the Minimum Order." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regtxtMinOrder" runat="server" ControlToValidate="txtMinOrder"
                                    ErrorMessage="Only Number and single dot are allowed in the Minimum Order." Display="Dynamic"
                                    SetFocusOnError="true" ValidationExpression="\d*\.?\d*" ValidationGroup="Add"
                                    CssClass="text-danger"></asp:RegularExpressionValidator>--%>
                                <asp:Repeater ID="rptMinMaxOrderValue" runat="server" OnItemDataBound="rptMinMaxOrderValue_ItemDataBound">
                                    <HeaderTemplate>
                                        <asp:Label ID="Label11" runat="server" Text="" Width="100px"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text="Minimum Order Value" Width="200px"></asp:Label>
                                        <asp:Label ID="Label9" runat="server" Text="Maximum Order Value" Width="200px"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                        </br>
                                        <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("CurrencySymbol") %>' Width="100px"></asp:Label>
                                        <asp:TextBox ID="txtMinOrder" runat="server" Width="200px"></asp:TextBox>
                                        <asp:TextBox ID="txtMaxOrder" runat="server" Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtMinOrder" CssClass="text-danger" Text="( Required )" ValidationGroup="add" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtMinOrder" Display="None" runat="server" ValidationGroup="Add" ErrorMessage="Only Number and decimal upto two digits are allowed for code value" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtMaxOrder" Display="None" runat="server" ValidationGroup="Add" ErrorMessage="Only Number and decimal upto two digits are allowed for code value" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>

                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <%-- <asp:Label ID="Label11" runat="server" CssClass="LabelText" Text="Maximum Order Value:"></asp:Label>--%>
                            </div>
                            <div class="col-md-6 ">
                                <%--<asp:TextBox ID="txtMaxOrder" CssClass="classWidthFull" runat="server" MaxLength="20"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMaxOrder"
                                    ErrorMessage="Only Number and single dot are allowed in the Maximum Order." Display="Dynamic"
                                    SetFocusOnError="true" ValidationExpression="\d*\.?\d*" ValidationGroup="Add"
                                    CssClass="text-danger"></asp:RegularExpressionValidator>--%>
                                <%--<asp:Repeater ID="rptMaximumOrderValue" runat="server" OnItemDataBound="rptMaximumOrderValue_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                        <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("CurrencySymbol") %>'>'</asp:Label>
                                        <asp:TextBox ID="txtCurrencyValue" runat="server" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </asp:Repeater>--%>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label1" runat="server" CssClass="classWidthFull" Text="Type of Discount (Behaviour):"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:DropDownList ID="ddlDicount" runat="server" CssClass="classWidthFull" AutoPostBack="true" OnSelectedIndexChanged="ddlDicount_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqddlDicount" runat="server" ControlToValidate="ddlDicount"
                                    ErrorMessage="Please enter the Type of Discount (Behaviour)." InitialValue="0"
                                    Display="None" SetFocusOnError="true" ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                            </div>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label3" runat="server" CssClass="LabelText" Text="Discount Percentage:"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:TextBox ID="txtDiscountValue" runat="server" CssClass="classWidthFull" MaxLength="3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtDiscountValue" runat="server" ControlToValidate="txtDiscountValue"
                                    ErrorMessage="Please enter the Discount Value." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtDiscountValue"
                                    ErrorMessage="Only Number and single dot are allowed in the Discount Order." Display="Dynamic"
                                    SetFocusOnError="true" ValidationExpression="\d*\.?\d*" ValidationGroup="Add"
                                    CssClass="text-danger"></asp:RegularExpressionValidator>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label7" runat="server" CssClass="LabelText" Text="Select Region:"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <!-- Please don't change the sequence, it will affect in the gvList_OnRowCommand on eidt part for binding the checkboxList-->
                                <asp:CheckBoxList ID="chkRegion" CssClass="classWidthFull" runat="server" RepeatDirection="Vertical" onchange="checkAll()">
                                </asp:CheckBoxList>
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-4 ">
                                <asp:Label ID="Label8" runat="server" CssClass="LabelText" Text="Select Shipment:"></asp:Label>
                            </div>
                            <div class="col-md-8">
                                <asp:RadioButtonList ID="rdListShippmentType" Style="width: 70%; padding: 5px;" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Standard" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Express" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Both" Value="0" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label5" runat="server" CssClass="LabelText" Text="Start Date:"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtStartDate" runat="server" Style="width: 100%; padding: 5px;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtStartDate" runat="server" ControlToValidate="txtStartDate"
                                    ErrorMessage="Please enter the Start Date." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label6" runat="server" CssClass="LabelText" Text="End Date:"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:TextBox ID="txtEndDate" runat="server" Style="width: 100%; padding: 5px;"></asp:TextBox>


                                <asp:RequiredFieldValidator ID="reqtxtEndDate" runat="server" ControlToValidate="txtEndDate"
                                    ErrorMessage="Please enter the End Date." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                            </div>

                            <div class="clearfix"></div>

                            <div class="clearfix"></div>

                            <div class="col-md-4 ">
                                <asp:Label ID="Label4" runat="server" CssClass="LabelText" Text="Number of uses:"></asp:Label>
                            </div>
                            <div class="col-md-8 ">
                                <asp:RadioButtonList ID="rdListNoOfUses" Style="width: 100%; padding: 5px;" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Single" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Multiple" Value="0"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label12" runat="server" CssClass="LabelText" Text="Status"></asp:Label>
                            </div>
                            <div class="col-md-8 ">
                                <asp:RadioButtonList ID="rdStatus" runat="server" Style="width: 100%; padding: 5px;" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Inactive" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Active" Value="1" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label10" runat="server" CssClass="LabelText" Text="Select WhiteList User Type:"></asp:Label>
                                <span>
                                    <asp:Label ID="lbNote" runat="server" Text="(Only CSV file is allowed Without Header Text)" ForeColor="Red"></asp:Label>
                                </span>
                            </div>
                            <div class="col-md-8 ">
                                <asp:RadioButtonList ID="RdWhiteList" runat="server" Style="width: 100%; padding: 5px;" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Email" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Domain" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="None (File Not Required.)" Value="0" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                                <span>
                                    <asp:FileUpload ID="UPlWhiteList" runat="server" />
                                </span>
                                <span>
                                    <asp:LinkButton ID="lnkDownload" runat="server" Visible="true" ForeColor="Blue" OnClick="lnkDownload_Click"></asp:LinkButton>
                                </span>
                                <br />
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-8 ">
                                <span>
                                    <asp:Label ID="lbl" runat="server" Text="Note :- Standard shipment will not be applicable in case of other region." ForeColor="Red"></asp:Label>
                                </span>
                            </div>
                            <div class="col-md-12 col-md-offset-6  button_section">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_OnClick"
                                    ValidationGroup="Add" CssClass="btn" OnClientClick="return validateDateSEnd();" />

                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_OnClick"
                                    CssClass="btn gray" />
                                <asp:ValidationSummary ID="valAdmin" runat="server" DisplayMode="List" ShowSummary="true"
                                    ValidationGroup="Add" CssClass="text-danger" />
                            </div>


                        </asp:Panel>

                    </div>

                    <div id="accordionx" class="search2 accordion">
                        <h3>Search Promo Code</h3>
                        <div class="searchInnerbox">
                            <div class="box1 box1Extra" style="background: none !important;">
                                <asp:Panel runat="server" DefaultButton="btnSearch">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="input1"></asp:TextBox>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                </asp:Panel>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <div class="customer" style="margin: 0px !important;">
                        <span class="allcosutomer">All Codes -
                    <asp:Literal ID="ltrNoOfCoupon" runat="server" Text=""></asp:Literal></span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="table-responsive">
                        <asp:Panel ID="pnlList" runat="server">
                            <asp:HiddenField ID="hdfCouponID" runat="server" />
                            <section class=" all_customer dragblnextBlock manage_pro">
                                <asp:GridView ID="gvList" runat="server" OnRowCommand="gvList_OnRowCommand" AutoGenerateColumns="false" AllowPaging="true" PageSize="50" AllowCustomPaging="true"
                                    ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid" OnPageIndexChanging="gvList_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Promo Code">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdfCouponID" runat="server" Value='<%# Eval("CouponID") %>' />
                                                <asp:Label ID="lblCouponCode" runat="server" Text='<%# Eval("CouponCode") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Min Order" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMinOrder" runat="server" Text='<%# Eval("MinimumOrderValue") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Max Order" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMaxOrder" runat="server" Text='<%# Eval("MaximumOrderValue") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Discount Value">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDiscountValue" runat="server" Text='<%# Eval("DiscountPercentage") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate", "{0:dd-MM-yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate","{0:dd-MM-yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Usage">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUsage" runat="server" Text='<%# Eval("Usage") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ValidationType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblValidationType" runat="server" Text='<%# Eval("UserType")%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIsActive" runat="server" Text='<%# Eval("IsActive").ToString().ToLower() == "true" ? "Active" : "InActive" %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order">
                                                            <a>
                                                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("CouponID") %>'
                                                                    CommandName="edits" /></a>
                                                            <a>
                                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandArgument='<%# Eval("CouponID") %>'
                                                                    OnClientClick="javascript:return confirm('Are you sure you want to delete this coupon?');"
                                                                    CommandName="deletes" />
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <div class="action" style="display: none">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="view_order">
                                                            <a>
                                                                <asp:Button ID="btnViewUser" runat="server" Text="View User" CommandArgument='<%# Eval("CouponID") %>'
                                                                    CommandName="ViewUser" Visible='<%# Eval("UserType").ToString().ToLower() == "email" ? true : false %>' />
                                                            </a>
                                                            <a>
                                                                <asp:Button ID="btnViewDomain" runat="server" Text="View Domain" CommandArgument='<%# Eval("CouponID") %>'
                                                                    CommandName="ViewDomain" Visible='<%# Eval("UserType").ToString().ToLower() == "domain" ? true : false %>' />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Center" />
                                        </asp:TemplateField>


                                    </Columns>
                                    <PagerSettings Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />
                                    <PagerStyle CssClass="paging" />
                                    <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                    <HeaderStyle CssClass="GridHeading" />
                                    <AlternatingRowStyle CssClass="GridAlternateItem" />
                                </asp:GridView>
                            </section>

                        </asp:Panel>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
