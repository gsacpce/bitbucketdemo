﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Admin_GiftCoupon_GiftCouponUserList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">

        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li><a href="GiftCoupon.aspx">Promo Code</a></li>
            <li>Promo Code User List</li>
        </ul>

        <div class="admin_page">
            <div class="container mainContainer ">
                <div class="wrap_container ">
                    <div class="content ">
                        <div class="row">
                            <div class="col-md-12 relative_data">
                                <h3>PromoCode User List
                                </h3>
                            </div>

                        </div>
                        <div class="customer" style="margin: 0px !important;">
                            <span class="allcosutomer">All User / Domain -
                    <asp:Literal ID="ltrNoOfuser" runat="server" Text=""></asp:Literal></span>
                            <div class="clear">
                            </div>
                        </div>

                        <div class=" table-responsive ">
                            <asp:Panel ID="pnlList" runat="server">
                                <asp:HiddenField ID="hdfCouponID" runat="server" />

                                <asp:GridView ID="gvUserList" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="50" Width="50%"
                                    ShowHeader="true" CellPadding="1" CellSpacing="1" CssClass="mGrid" AllowCustomPaging="true" OnPageIndexChanging="gvUserList_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="User Name / Domain Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserDomainName" runat="server" Text='<%# Eval("User") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerSettings Position="TopAndBottom" Mode="NextPreviousFirstLast" FirstPageText="<<" LastPageText=">>" NextPageText=">" PreviousPageText="<" />
                                    <PagerStyle CssClass="paging" />
                                    <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                                    <HeaderStyle CssClass="GridHeading" />
                                    <AlternatingRowStyle CssClass="GridAlternateItem" />
                                    <EmptyDataTemplate>No Data Exists</EmptyDataTemplate>
                                </asp:GridView>
                                <asp:Button ID="btnExportTocsv" runat="server" Text="Export" OnClick="btnExportTocsv_Click" />

                            </asp:Panel>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>

