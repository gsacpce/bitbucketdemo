﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Customer_CustomerListing" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Customer </a></li>
            <li>Customer List</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <h3 class="mainHead">Customer Listing</h3>
                    <span class="sectionText">Manage Customer Listing.</span>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Search </label>
                                <div class="col-xs-8">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-xs-2">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn2" OnClick="btnSearch_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group clearfix">
                                Note: Search by Name, Company name, Email address.
                            </div>
                        </div>
                    </div>
                    <div class="butonblock clearfix">



                        <ul class="categories_btn" style="margin: 0;">
                            <%--  <li>
                                <asp:Button runat="server" ID="btnSeq" CssClass="btn1" OnClick="btn_Click" Text="Category Sequence" Style="display: none;" />
                            </li> <li>
                                <asp:Button runat="server" ID="btnAddNew" CssClass="btn3" OnClick="btn_Click" Text="Add New Category" Style="display: none;" />
                            </li>
                            --%>
                            <li>
                                <asp:Button runat="server" ID="btnExportToExcel" CssClass="btn2" OnClick="btnExportToExcel_Click"
                                    Text="Export Users" />
                            </li>
                        </ul>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="categoriesmainBlock">
                        <div id="divMessage" visible="false" runat="server">
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>
                                    <asp:Literal ID="ltrMessage" runat="server" /></strong>
                            </div>
                        </div>
                        <!--butonblock-->
                        <div class="categoryMidlBlock">
                            <div class="midleDrgblecontin">
                                <ul class=" ">
                                    <li>
                                        <table width="100%" class="cust_listing">
                                            <tbody>
                                                <tr>
                                                    <th class="firsttd" style="width: 25%;">Email
                                                    </th>
                                                    <th class="secondtd">First Name
                                                    </th>
                                                    <th class="thirdtd">Last Name
                                                    </th>
                                                    <th class="fourthtd">Company
                                                    </th>
                                                    <th class="fourthtd">Date
                                                    </th>
                                                    <th class="fourthtd">Status
                                                    </th>
                                                    <th class="fourthtd">Register / Guest
                                                    </th>
                                                    <th class="fourthtd">User Type Name</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <asp:Repeater ID="rptUsers" runat="server">
                                        <ItemTemplate>
                                            <li class="firstLitab">
                                                <table width="100%" class="cust_listing">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 25%;">
                                                                <span id="spanEmail" runat="server">
                                                                    <asp:HiddenField ID="hdnUID" Value='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' runat="server" />
                                                                    <%# DataBinder.Eval(Container.DataItem, "EmailID") %>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="spanFirstName" runat="server">
                                                                    <%# DataBinder.Eval(Container.DataItem, "FirstName") %>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="spanLastName" runat="server">
                                                                    <%# DataBinder.Eval(Container.DataItem, "LastName") %>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="spanCompany" runat="server">
                                                                    <%# DataBinder.Eval(Container.DataItem, "PredefinedColumn2") %>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="spanCreatedDate" runat="server">
                                                                    <%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate")).ToString("dd/MM/yyyy") %>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="span1" runat="server">
                                                                    <asp:LinkButton ID="lnkActivate" runat="server" OnClick="lnkActivate_Click" Text='<%# DataBinder.Eval(Container.DataItem,"IsActive").ToString()=="True" ? "Active":"In Active" %>' CssClass="btnAdd" CommandArgument='<%#Eval("UserId")%>'></asp:LinkButton>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="SpanGuestRegidter" runat="server">
                                                                    <%# DataBinder.Eval(Container.DataItem,"IsGuestUser").ToString()=="True" ? "Guest":"Registerd" %>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <span id="span2" runat="server">
                                                                    <%# DataBinder.Eval(Container.DataItem, "UserTypeName") %>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <!--dragblnextBlock-->
                            </div>
                        </div>
                        <!--categoryMidlBlock-->
                    </div>
                    <!--categoriesmainBlock-->
                </div>
            </div>
        </section>
    </div>
</asp:Content>
