﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Customer_GroupListing" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        $(document).ready(function () {
            $('.action').mouseover(function () {
                $('.icon', this).css('z-index', '999');
                $('.icon', this).addClass("iconDark");
                $('.action_hover', this).show();
                return false;


            });

            $('.action').mouseout(function () {
                $('.icon', this).css('z-index', '8');
                $('.icon', this).removeClass("iconDark");
                $('.action_hover', this).hide();
                return false;

            });
        });
    </script>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li>Customers</li>
            <li>Budget Management</li>
        </ul>

    </div>
    <div class="admin_page">
        <section class="container mainContainer role_management role_listing manage_users">
            <div class="wrap_container ">
                <div class="content">
                    <section class="mainContainer">
                        <h3 class="mainHead">Manage Budgets
                        </h3>


                        <div class="customer" style="margin: 0px !important;">
                            <span class="allcosutomer">All Groups -
                            <asp:Literal ID="ltrNoOfRecords" runat="server" Text=""></asp:Literal></span>
                            <div class="add_newrole">
                                <asp:Button ID="btnAddNew" runat="server" CssClass="save" Text="Add New Groups" OnClick="btnAddNew_Click" />
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="pad_top">
                            <section class=" all_customer dragblnextBlock manage_pro">
                                <asp:GridView runat="server" ID="gvGroups" TabIndex="0" CellPadding="3" Width="100%" PageSize="30"
                                    DataKeyNames="GroupId,GroupName" AutoGenerateColumns="False" OnPageIndexChanging="gvGroups_PageIndexChanging"
                                    CssClass="all_customer_inner allcutomerEtracls" EmptyDataText="No Group Exits."
                                    AllowPaging="true">
                                    <HeaderStyle Font-Bold="true" />
                                    <RowStyle HorizontalAlign="Center" Height="30px" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Group Name" SortExpression="GroupName">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "GroupName")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Validity Start Date">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "ValidityStartDate","{0:dd-MM-yyyy}")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Validity End Date">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "ValidityEndDate","{0:dd-MM-yyyy}")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No of Users">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "NoOfUser")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Assigned Budget">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "Budget")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pending Amount">
                                            <ItemTemplate>
                                                <%#DataBinder.Eval(Container.DataItem, "PendingAmt")%>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Active / Inactive">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Eval("IsActive").ToString().ToLower() == "true" ? true : false %>' OnCheckedChanged="chkIsActive_CheckedChanged"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="5%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-VerticalAlign="Top">
                                            <ItemTemplate>
                                                <div class="action">
                                                    <a class="icon" href="#"></a>
                                                    <div class="action_hover">
                                                        <span></span>
                                                        <div class="other_option">

                                                            <asp:LinkButton ID="lnkEditGroups" runat="server" CommandArgument='<%# Eval("GroupId") %>' OnClick="lnkEditGroups_Click">Edit</asp:LinkButton>
                                                            <%--          <asp:LinkButton ID="lnkbtnDelete" runat="server" OnClientClick="javascript:return confirm('Are you sure you want to delete this role?')"
                                                                CommandArgument='<%# Eval("RoleId") %>' CommandName="DeleteRole" Text="">Delete</asp:LinkButton>--%>
                                                        </div>

                                                    </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="paging" />
                                </asp:GridView>
                            </section>
                        </div>
                        <div class="clearfix"></div>
                    </section>

                </div>
            </div>
        </section>
    </div>
</asp:Content>
