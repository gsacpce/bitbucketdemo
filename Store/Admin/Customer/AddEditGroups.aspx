﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_Customer_AddEditGroups" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" type="text/css" href="../../JS/bootstrap-datetimepicker.js" />
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <link href="../CSS/ui.theme.css" rel="stylesheet" />
    <style type="text/css">
        .pager span {
            color: #009900;
            font-weight: bold;
            font-size: 16pt;
        }

        .classWidthFull {
            width: 100%;
        }

        .row.paginationBlk {
            border: 1px solid #ccc;
            margin: 10px 0;
            padding: 10px 0 5px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#<%= txtValidityStartDate.ClientID  %>").datepicker({ dateFormat: 'yy-mm-dd' });
            $("#<%= txtValidityEndDate.ClientID  %>").datepicker({ dateFormat: 'yy-mm-dd' });
        });
        function ValidateTextbox(clientId) {
            debugger;
            var obj = document.getElementById(clientId);
            var iChars = "!@#$%^\"&+=[]{}|:<>?"
            //alert(obj.value.length);
            //"!@#$%^&*()+=-[]\\\';,./{}|\":<>?"
            for (var i = 0; i < obj.value.length; i++) {
                if (iChars.indexOf(obj.value.charAt(i)) != -1) {
                    alert("Search Text has special characters. \nThese are not allowed.\n Please remove them and try again.");
                    obj.value = "";
                    return false;
                }
            }
            return true;
        }
    </script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li>Customers</li>
            <li>Budget Management</li>
        </ul>

    </div>
    <div class="admin_page">
        <div class="container mainContainer ">
            <div class="wrap_container gift_cupon">
                <div class="content ">
                    <div class="row ">
                        <div class="col-md-12 relative_data">
                            <h3>Manage Budgets
                            </h3>

                        </div>

                        <asp:Panel ID="pnlAdminAddEdit" runat="server">
                            <div class="col-md-4 ">

                                <asp:Label ID="lblGroupName" runat="server" Text="Group Name :" CssClass="LabelText"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:TextBox ID="txtGroupName" CssClass="classWidthFull" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtGroupName" runat="server" ControlToValidate="txtGroupName"
                                    ErrorMessage="Please enter the Group Name." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                                <%--  <asp:RegularExpressionValidator ID="regtxtGroupName" runat="server" ControlToValidate="txtGroupName"
                                        ErrorMessage="Only AlpaNumeric are allowed in the G." ValidationExpression="\w*"
                                        Display="None" SetFocusOnError="true" ValidationGroup="Add" CssClass="text-danger"></asp:RegularExpressionValidator>
                                --%>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label5" runat="server" CssClass="LabelText" Text="Validity Start Date:"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtValidityStartDate" runat="server" Style="width: 100%; padding: 5px;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtStartDate" runat="server" ControlToValidate="txtValidityStartDate"
                                    ErrorMessage="Please enter the Validity Start Date." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label6" runat="server" CssClass="LabelText" Text="Validity End Date:"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:TextBox ID="txtValidityEndDate" runat="server" Style="width: 100%; padding: 5px;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqtxtEndDate" runat="server" ControlToValidate="txtValidityEndDate"
                                    ErrorMessage="Please enter the Validity End Date." Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label12" runat="server" CssClass="LabelText" Text="Active / Inactive :"></asp:Label>
                            </div>
                            <div class="col-md-6 ">
                                <asp:CheckBox ID="chkIsactive" runat="server" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 ">
                                <asp:Label ID="Label1" runat="server" CssClass="LabelText" Text="Budget"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtBudget" runat="server" Style="width: 100%; padding: 5px;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtBudget"
                                    ErrorMessage="Please Assing the Budget" Display="None" SetFocusOnError="true"
                                    ValidationGroup="Add" CssClass="text-danger"></asp:RequiredFieldValidator>
                            </div>

                            <div></div>
                            <div class="col-md-4 ">
                            </div>
                            <div class="col-md-8 assign_direction">
                                <div class="col-md-12 budget_div">
                                    <div class="col-md-5 col-sm-5 " style="text-align: center;">
                                        <asp:ListBox ID="lstAllUser" runat="server" Style="width: 100%;" SelectionMode="Multiple"></asp:ListBox>

                                    </div>
                                    <div class="col-md-2 col-sm-2 " style="text-align: center;">
                                        <p class="">
                                            <asp:Button ID="btnMoveRightAll" runat="server" Text=">>" CssClass="btn" OnClick="btnMoveRightAll_Click" />
                                            <asp:Button ID="btnMoveRight" runat="server" Text=">" CssClass="btn" OnClick="btnMoveRight_Click" />
                                            <asp:Button ID="btnMoveLeft" runat="server" Text="<" CssClass="btn" OnClick="btnMoveLeft_Click" />
                                            <asp:Button ID="btnMoveLeftAll" runat="server" Text="<<" CssClass="btn" OnClick="btnMoveLeftAll_Click" />
                                        </p>
                                    </div>
                                    <div class="col-md-5 col-sm-5" style="text-align: center;">
                                        <asp:ListBox ID="lstAssignedUser" runat="server" Style="width: 100%;" SelectionMode="Multiple"></asp:ListBox>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>


                            <div class="clearfix"></div>
                            <div class="col-md-12 col-md-offset-6  button_section">

                                <asp:ValidationSummary ID="valAdmin" runat="server" DisplayMode="List" ShowSummary="false" ShowMessageBox="true"
                                    ValidationGroup="Add" CssClass="text-danger" />
                                <asp:Button ID="btnAdd" runat="server" Text="Save and Assign Users"
                                    ValidationGroup="Add" CssClass="btn" OnClick="btnAdd_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Save and Assign Users"
                                    ValidationGroup="Add" CssClass="btn" OnClick="btnUpdate_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel"
                                    CssClass="btn gray" />
                            </div>
                            <div class="col-md-12 col-md-offset-6  button_section">
                                <asp:Label ID="lblErrorMsg" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
