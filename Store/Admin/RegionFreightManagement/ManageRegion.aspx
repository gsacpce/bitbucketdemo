﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_RegionFreightManagement_ManageRegion" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <script>
            function ShowSuccess(aMsg) {
                $('#mySuccessModal').find('#SuccessMessage').html(aMsg);
                $('#mySuccessModal').modal('show');
            }
    </script>
     <script>
         function ShowWarning(aMsg) {
             $('#myWarningModal').find('#WarningMessage').html(aMsg);
             $('#myWarningModal').modal('show');
         }
    </script>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li>Manage Region</li>
        </ul>
    </div>
    <div class="admin_page">

        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class="mainHead">Manage Region</h3>
                        </div>
                        <div class="fright_div">

                            <div class="form-group clearfix">
                                <div class="col-md-12  text-right">

                                    <label class="col-md-8 text-right sm_select" id="select_lang">
                                        Select Language :
                                    </label>
                                    <div style="float: left" id="divLang" runat="server" class="col-md-2">
                                        <div class="select-style  selectpicker languageDropdown">
                                            <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div>
                                        <ul class="button_section">
                                            <li>
                                                <asp:Button ID="Button1" runat="server" CssClass="btn" Text="Add Region" OnClick="Button1_Click" CausesValidation="false"/>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Select Region</label>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="ddlSelectRegion" runat="server" AutoPostBack="true" CssClass="form-control" Width="50%" ClientIDMode="Static" OnSelectedIndexChanged="ddlSelectRegion_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Region Name</label>
                                <div class="col-xs-8">
                                    <asp:TextBox ID="txtRegionName" runat="server" Width="50%" MaxLength="30" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    <div>
                                        <ul class="button_section">
                                            <li>
                                                <asp:Button ID="BtnChangeRegionName" runat="server" CssClass="btn" Text="Update Region Name" OnClick="BtnChangeRegionName_Click" CausesValidation="false" />
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="CarriageMethod" class="control-label col-xs-2">Available Services</label>
                                <div class="col-xs-8">
                                    <asp:Repeater ID="rptServices" runat="server" OnItemDataBound="rptServices_ItemDataBound">
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <%--<asp:CheckBox ID="chkService" runat="server" />--%>
                                                        <asp:CheckBox ID="chkService" runat="server" Text='<%# Eval("ServiceName") %>'/>
                                                        <asp:HiddenField ID="hdfFreightModeId" runat="server" Value='<%# Eval("FreightModeId") %>' />
                                                    </td>
                                                    <td>&nbsp;&nbsp;</td>
                                                    <td>
                                                        <div class="col-xm-8">
                                                            <asp:TextBox ID="txtFreightModeName" runat="server" MaxLength="50" CssClass="form-control" Text='<%# Eval("FreightModeName") %>'></asp:TextBox>

                                                            <%--<asp:Label ID="lblService" CssClass="form-control" runat="server" Text='<%# Eval("FreightModeName") %>'></asp:Label>--%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>

                                        </ItemTemplate>

                                    </asp:Repeater>
                                </div>
                                <%--                                <div class="col-xs-2">
                                    <asp:CheckBoxList ID="CheckBoxList1" runat="server" Height="90px">
                                        <asp:ListItem>Service 1</asp:ListItem>
                                        <asp:ListItem>Service 2</asp:ListItem>
                                        <asp:ListItem>Service 3</asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                                <div class="col-xs-4">
                                    <asp:Label ID="label1" runat="server" CssClass="form-control" ClientIDMode="Static" Width="50%" Height="25px"></asp:Label><br />
                                    <asp:Label ID="label2" runat="server" CssClass="form-control" ClientIDMode="Static" Width="50%" Height="25px"></asp:Label><br />
                                    <asp:Label ID="label3" runat="server" CssClass="form-control" ClientIDMode="Static" Width="50%" Height="25px"></asp:Label>

                                </div>--%>
                            </div>
                            <div class="form-group clearfix">
                                <div class="buttonPanel container">
                                    <ul class="button_section">
                                        <li>
                                            <asp:Button ID="btnFreightSave" runat="server" CssClass="btn" Text="Save" OnClick="btnFreightSave_Click" CausesValidation="false"/>
                                        </li>
                                        <li>
                                            <asp:Button ID="btnCancel" CssClass="btn gray" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                        </li>
                                        <li>
                                            <asp:Button ID="btnDelete" CssClass="btn" runat="server" Text="Delete" CausesValidation="false" OnClick="btnDelete_Click"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div class="modal fade" id="ModalAddRegion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content customModal">
                <div class="customModal modal-header customPanel">
                    <button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title pageSubSubTitle" id="myModalLabel"></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="form-group clearfix">
                        <label for="CarriageMethod" class="control-label col-xs-2">Region Name</label>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtAddRegionName" runat="server" Width="50%" MaxLength="30" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="form-group clearfix">
                    <div class="buttonPanel container">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSaveRegion" runat="server" CssClass="btn" Text="Save Region" OnClick="btnSaveRegion_Click" CausesValidation="false"/>
                            </li>
                            <li>
                                <asp:Button ID="Button5" CssClass="btn gray" runat="server" Text="Cancel" />
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">

        function ShowModal() {
            $('#ModalAddRegion').modal('show')
        }
    </script>
    <%--ravi Added on 3-02-2017 START--%>
        <div class="modal fade" id="ModalUpdateRegion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content customModal">
                <div class="customModal modal-header customPanel">
                    <button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title pageSubSubTitle" id="myModalLabelUpdate"></h4>
                </div>
                <div class="modal-body clearfix">
                    <div class="form-group clearfix">
                        <label for="CarriageMethod" class="control-label col-xs-2">Update Region Name</label>
                        <div class="col-xs-8">
                            <asp:TextBox ID="txtUpdateRegionName" runat="server" Width="50%" MaxLength="30" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
              <asp:RequiredFieldValidator ID="reqUpdateRegionName"  runat="server" ControlToValidate="txtUpdateRegionName" CssClass="text-danger" Text="( Region Name Can Not Be Blank !! )"></asp:RequiredFieldValidator>

                        </div>
                    </div>

                </div>
                <div class="form-group clearfix">
                    <div class="buttonPanel container">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="BtnUpdateRegion" AutoPostback="true" runat="server" CssClass="btn" Text="Update Region" CausesValidation="false"  OnClick="BtnUpdateRegion_Click"/>
                            </li>
                        
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
     <script type="text/javascript">

         function ShowModalUpdate() {
             $('#ModalUpdateRegion').modal('show')
         }
    </script>
     <%--ravi Added on 3-02-2017 END--%>
</asp:Content>

