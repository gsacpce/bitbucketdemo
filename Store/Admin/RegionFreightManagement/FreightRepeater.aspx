﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" EnableViewStateMac="false"  AutoEventWireup="true" Inherits="Presentation.Admin_RegionFreightManagement_FreightRepeater" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .bootbox-close-button.close {
            display: none;
        }
        .modal-header {
            border-bottom: 1px solid #e5e5e5;
        }
        .modal-body {
            padding: 15px;
        
        }
        .customPanel.panel-heading, .customPanel thead, .customPanel.modal-header, .customPanel .modal-title {
    background-color: #606060 !important;
    color: #b0b0b0 !important;
}
        .close {
    opacity: 1;
}
        .pageSubSubTitle {
    color: #a0a0a0 !important;
    font-size: 14px !important;
    font-style: normal !important;
    
}
        .modal-title {
    line-height: 1.42857;
    margin: 0;
}
#rblFreightModes label{margin: 0 22px 4px 0;overflow: hidden;
    overflow-wrap: break-word;text-overflow: ellipsis;white-space: nowrap;width: 71px;}
        </style>
      <script src="<%=host %>Admin/JS/bootbox.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#aBand0').click();
            $(".bootbox  .modal-content").prepend("<div class='modal-header customPanel'><button type='button' class='close customClose' data-dismiss='modal' aria-hidden='true'>&times;</button><h4 class='modal-title pageSubSubTitle'><span><i class='fa fa-exclamation-triangle fa-1x text-warning'></i>Warning!</span></h4></div>")
        });

        function BandExpand() {
            try {
                var BandCounter = "<%=BandCounter %>";
                var BandId = 'aBand' + BandCounter;
                var i = 1;
                for (i = 1; i <= BandCounter; i++) {
                    document.getElementById("div" + i).style.display = "block";
                }

                $('#' + BandId).click();
                //alert('hi');
            } catch (e) { }
        }

        function myTestFunction() {
            var radioCarriageModel = document.getElementById("<%= rblCarriageMode.ClientID %>");
            var radio1 = radioCarriageModel.getElementsByTagName('input');
            var Parameter;
            var radioMode = document.getElementById("<%= rblFreightModes.ClientID %>");
            var radio = radioMode.getElementsByTagName('input');
            var label = radioMode.getElementsByTagName("label");
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].checked) {
                    var Text = label[i].innerHTML;
                    var ModeId = radio[i].value;
                }
            }
            if (radio1[0].checked) {
                Parameter = "Value";
            }
            else {
                Parameter = "Weight";
            }

            bootbox.confirm({
                message: "There is already data for " + '<strong>' + Text + '</strong>' + " And " + '<strong>' + Parameter + '</strong>' + " Parameter Combination. \n If you Proceed then this combination for all band will get deleted Permanently..!! \n Do You Want to Proceed ?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    //  console.log('This was logged in the callback: ' + result);
                    if (result == true) {
                        var e = document.getElementById('<%=ddlSelectRegion.ClientID%>');
                        $.ajax({
                            url: host + 'Admin/RegionFreighManagement/FreightRepeater.aspx/ChangeRadioSelection',
                            data: "{'region': '" + e.options[e.selectedIndex].value + "','Mode':'" + ModeId + "','Parameter':'" + Parameter + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            cache: false,
                            async: true,
                            success: function (response) {
                                __doPostBack('<%=rblFreightModes.ClientID %>', 'SelectedIndexChanged');

                            },
                            //beforeSend: function (r
                        })
                        }
                        else {
                            if (radio1[0].checked) {
                                radio1[1].checked = true;
                            }
                            else
                                if (radio1[1].checked) {
                                    radio1[0].checked = true;
                                }
                            __doPostBack('<%=rblFreightModes.ClientID %>', '');

                    }
                }
            });




          <%--  if (confirm("There is already data for " + Text +" And " + Parameter +" Parameter Combination. \n If you Proceed then this combination for all band will get deleted Permanently..!! \n Do You Want to Proceed ?")) {
                //return true;
                var e =document.getElementById('<%=ddlSelectRegion.ClientID%>');
                    $.ajax({
                        url: host + 'Admin/RegionFreighManagement/FreightRepeater.aspx/ChangeRadioSelection',
                        data: "{'region': '" + e.options[e.selectedIndex].value + "','Mode':'" + ModeId + "','Parameter':'" + Parameter + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        cache: false,
                        async: true
                        //beforeSend: function (r
                    })
                }
          else {
                if (radio1[0].checked)
                {
                    radio1[1].checked = true;
                }
                else
                    if (radio1[1].checked)
                    {
                        radio1[0].checked = true;
                    }                
              __doPostBack('<%=rblFreightModes.ClientID %>', '');

                return false;
            }--%>
        }
    </script>
    <script type="text/javascript">
        var host = "<%=host %>";

        function CheckRadio(id) {
            debugger

            var HDmethodVal = document.getElementById("ContentPlaceHolder1_rptBands_hiddenModeId_" + id).value;
            var radioMode = document.getElementById("<%= rblFreightModes.ClientID %>");
            var radio = radioMode.getElementsByTagName('input');
            var item = "2";
            debugger
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].value == item) {
                    alert(radio[i].value);
                    alert(HDmethodVal);
                    radio[i].checked = true;

                }
                else {
                    radio[i].checked = false;
                }
            }

        }


        function GetItemId(id) {
            document.getElementById("BandActiveSession").value = id;

            var Div = "ContentPlaceHolder1_rptBands_divCollapse_" + id;
            var count = document.getElementById("HiddenRepeaterItemCount").value
            var HDmethodVal = document.getElementById("ContentPlaceHolder1_rptBands_hiddenModeId_" + id).value;
            var HdWeightPara = document.getElementById("ContentPlaceHolder1_rptBands_HiddenWeightParameter_" + id).value;
            var HDCurrencyPara = document.getElementById("ContentPlaceHolder1_rptBands_HiddenCurrencyParameter_" + id).value;
            for (var i = 0; i <= count; i++) {
                if (id == i) {
                    if (document.getElementById("ContentPlaceHolder1_rptBands_divCollapse_" + i).className == "collapse-in" && document.getElementById("ContentPlaceHolder1_rptBands_divCollapse_" + i).className == "collapse in") {
                        $("ContentPlaceHolder1_rptBands_divCollapse_" + id).removeClass();
                        $("ContentPlaceHolder1_rptBands_divCollapse_" + id).addClass('collapse');
                    }
                    else {
                        $("ContentPlaceHolder1_rptBands_divCollapse_" + id).removeClass();
                        //  document.getElementById("ContentPlaceHolder1_rptBands_divCollapse_" + i).className = "collapse-in";
                    }
                }
                else {
                    $("ContentPlaceHolder1_rptBands_divCollapse_" + id).removeClass();
                    document.getElementById("ContentPlaceHolder1_rptBands_divCollapse_" + i).className = "collapse";
                }
            }


<%--            var radioMode = document.getElementById("<%= rblFreightModes.ClientID %>");
            var radio = radioMode.getElementsByTagName('input');
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].value == HDmethodVal) {
                   radio[i].checked = true;
                    __doPostBack('<%= rblFreightModes.ClientID %>', '');

                }
                else {
                    radio[i].checked = false;
                }
            }
            // setTimeout('__doPostBack(\'' + radio[i].id + '\',\'' + radio[i].value + '\')', 0);

            var radioCarriageModel = document.getElementById("<%= rblCarriageMode.ClientID %>");
            var radio1 = radioCarriageModel.getElementsByTagName('input');
            if (HdWeightPara == "True") {
                if (radio1[0].checked == true) {
                    // $("ContentPlaceHolder1_rptBands_divCollapse_" + id).addClass('collapse-in');
                    radio1[0].checked == true
                    radio1[1].checked = false;
                   __doPostBack('<%=rblCarriageMode.ClientID %>', '');
                }
                else {
                    radio1[0].checked = true;
                    radio1[1].checked = false;
                    __doPostBack('<%=rblCarriageMode.ClientID %>', '');
                }
            }



            if (HDCurrencyPara == "True") {
                if (radio1[1].checked == true) {
                    //document.getElementById("ContentPlaceHolder1_rptBands_divCollapse_" + id).className = "collapse-in";
                    radio1[1].checked == true
                    radio1[0].checked = false;
                    __doPostBack('<%=rblCarriageMode.ClientID %>', '');
                }
                else {
                    radio1[0].checked = false;
                    radio1[1].checked = true;
                     __doPostBack('<%=rblCarriageMode.ClientID %>', '');
                }
            }--%>
        }

        function HideBandDiv() {
            //alert('hi');
            document.getElementById("<%= collapse1.ClientID %>").style.display = "none";
        }


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Settings </a></li>
            <li><a href="#">New Freight</a></li>
            <li></li>
        </ul>
    </div>
    <div class="container admin_page">
        <section class="container mainContainer padbtom15 assing_prodcut">
            <div class="wrap_container ">
                <div class="content row">
                    <div class="col-xs-12 wide">
                        <h3 class="mainHead">FREIGHT MANAGEMENT </h3>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-md-12  text-right">

                            <label class="col-md-8 text-right sm_select" id="select_lang">
                                Select Language :
                            </label>
                            <div style="float: left" id="divLang" runat="server" class="col-md-2">
                                <div class="select-style  selectpicker languageDropdown">
                                    <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 marginTopDiv">
                            <div id="divFreightSourceCountry" runat="server" class="row form-group" visible="false">
                                <div class="col-md-3">
                                    <label class="sm_select pull-right">
                                        <b>Select Freight Source Country &nbsp:</b>
                                    </label>
                                </div>
                                <div class="right_selct_dropdwn relative_data col-md-4 sm_select">
                                    <asp:DropDownList ID="ddlSelectFreightSrcCountry" runat="server" CssClass="form-control" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlSelectFreightSrcCountry_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <label class="sm_select pull-right">
                                        <b>Select Region &nbsp:</b>
                                    </label>
                                </div>
                                <div class="right_selct_dropdwn relative_data col-md-4 sm_select">
                                    <asp:DropDownList ID="ddlSelectRegion" runat="server" CssClass="form-control" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlSelectRegion_SelectedIndexChanged"></asp:DropDownList>

                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <label class="sm_select pull-right">
                                        <b>Carriage Service&nbsp:</b>
                                    </label>
                                </div>
                                <div class="col-md-6" id="dvFreightSelect">
                                    <label class="radio-inline sm_select">
                                        <asp:RadioButtonList runat="server" ClientIDMode="Static" CssClass="carriageServiceRadioBtn" RepeatDirection="Horizontal" ID="rblFreightModes" AutoPostBack="true" OnSelectedIndexChanged="rblMode_SelectedIndexChanged"></asp:RadioButtonList>
                                    </label>

                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-3">
                                    <label class="sm_select pull-right">
                                        <b>Carriage Model&nbsp:</b>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="radio-inline sm_select"> 
                                        <asp:RadioButtonList ID="rblCarriageMode" CssClass="carriageModelRadioBtn" RepeatDirection="Horizontal" ClientIDMode="Static" AutoPostBack="true" runat="server" OnSelectedIndexChanged="rblCarriageMode_SelectedIndexChanged">
                                            <asp:ListItem Value="Weight">Weight</asp:ListItem>
                                            <asp:ListItem Value="Value">Value</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </label>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 panelMargin">
                            <div class="panel-group">
                                <asp:HiddenField ID="HiddenRepeaterItemCount" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="BandActiveSession" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HidenMode" runat="server" ClientIDMode="Static" />
                                <asp:Repeater ID="rptBands" runat="server" OnItemDataBound="rptBands_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hiddenModeId" runat="server" Value='<%#Eval("FreightModeId") %>' />
                                        <asp:HiddenField ID="HiddenFreightId" runat="server" ClientIDMode="Static" Value='<%#Eval("FreightConfigurationId") %>' />
                                        <span runat="server" id="SpnItemId" hidden="hidden"><%#Container.ItemIndex%></span>
                                        <div class="panel panel-default" id="dvMainBand" runat="server">
                                            <div class="panel-heading panelHeading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" runat="server" id="aBand" onclick='<%# "javascript:GetItemId(" + Container.ItemIndex + ")" %>'>
                                         
                                                        <asp:Label ID="lblBand" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="HiddenBandId" runat="server" Value='<%#Eval("BandId") %>' />
                                                    </a>
                                                    <asp:CheckBox ID="checkBandActive" class="pull-right" runat="server" Checked="true" />
                                                </h4>
                                            </div>
                                            <div id="divCollapse" runat="server" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="row form-group">
                                                        <div class="col-md-3">
                                                            <label class="">
                                                                <b>Band Parameters<span class="redColor">*</span>&nbsp:</b>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <asp:HiddenField ID="HiddenWeightParameter" runat="server" Value='<%#Eval("WeightParameter") %>' />
                                                            <ul class="band-form " id="ulWeight" runat="server">
                                                                <li><b>From &nbsp;</b></li>
                                                                <li>
                                                                    <asp:TextBox ID="txtWeightFrom" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvWeightFrom" runat="server" ControlToValidate="txtWeightFrom" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                    kg
                                                                </li>
                                                                <li><b>To &nbsp;</b></li>
                                                                <li>
                                                                    <asp:TextBox ID="txtWeightTo" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvWeightTo" runat="server" ControlToValidate="txtWeightTo" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                    kg
                                                                </li>
                                                            </ul>
                                                            <asp:HiddenField ID="HiddenCurrencyParameter" runat="server" Value='<%#Eval("CurrencyParameter") %>' />

                                                            <ul class="band-form" id="ulValue" runat="server" style="display: none;">
                                                                <li>
                                                                    <asp:Repeater ID="rptValue" runat="server" OnItemDataBound="rptValue_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td><b>From</b> &nbsp;</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtValFrom" runat="server"></asp:TextBox>
                                                                                </td>
                                                                                <asp:RequiredFieldValidator ID="rfvValFrom" runat="server" ControlToValidate="txtValFrom" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                                <td>
                                                                                    <asp:Label ID="lblCurrencyFrom" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                                </td>
                                                                                <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />

                                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                                                                <td><b>To</b> &nbsp;</td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtValTo" runat="server"></asp:TextBox>
                                                                                </td>
                                                                                <asp:RequiredFieldValidator ID="rfvValTo" runat="server" ControlToValidate="txtValTo" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                                <td>
                                                                                    <asp:Label ID="lblCurrencyTo" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate></table></FooterTemplate>

                                                                    </asp:Repeater>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-3">
                                                            <label class="">
                                                                <b>Carriage Method<span class="redColor">*</span>&nbsp:</b>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" CssClass="tableTdWidth" ID="rblFreightMethods" AutoPostBack="true" OnSelectedIndexChanged="rbl_SelectedIndexChanged"></asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-3 ">
                                                            <label class="">
                                                                <b>Carrier Service<span class="redColor">*</span>&nbsp:</b>
                                                            </label>
                                                        </div>
                                                        <div class="right_selct_dropdwn relative_data col-md-4">
                                                            <asp:DropDownList ID="ddlCarrierService" runat="server" CssClass="form-control" onchange="myFunction()" ClientIDMode="Static"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfvddlCarrierService" runat="server" ControlToValidate="ddlCarrierService" InitialValue="0" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please select Carrier Service Text." SetFocusOnError="true" ValidationGroup="valFreightConfig"></asp:RequiredFieldValidator>

                                                        </div>
                                                    </div>
                                                    <div class="row form-group" id="divFreightTable" style="display: none;" runat="server">
                                                        <div class="col-md-3 ">
                                                            <label class="">
                                                                <b>Freight Table<span class="redColor">*</span>&nbsp:</b>
                                                            </label>
                                                        </div>
                                                        <div class="right_selct_dropdwn relative_data col-md-4">
                                                            <asp:DropDownList ID="ddlFreightTable" runat="server" CssClass="form-control" onchange="myFunction()" ClientIDMode="Static"></asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFreightTable" InitialValue="0" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please select Freight Table Text." SetFocusOnError="true" ValidationGroup="valFreightConfig"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-3 ">
                                                            <label class="">
                                                                <b>Transit Time<span class="redColor">*</span>&nbsp:</b>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:TextBox ID="txtTransitTime" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-md-3 ">
                                                            <label class="">
                                                                <b>Custom Text&nbsp:</b>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:TextBox ID="txtCustomText" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group clearfix">

                                                        <asp:Label ID="lblMOV" runat="server" CssClass="control-label col-xs-3 boldText" Text="Minimum Order Value :"></asp:Label>
                                                        <div class="col-xs-3 spanWidth">
                                                            <asp:Repeater ID="rptMinimumOrderValue" runat="server" OnItemDataBound="rptMinimumOrderValue_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                                    <asp:Label ID="lblCurrency" CssClass="currSym" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                    <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                    </div>
                                                    <div class="row form-group clearfix">

                                                        <asp:Label ID="lblVAA" runat="server" CssClass="control-label col-xs-3 boldText" Text="Value Applied Above :"></asp:Label>
                                                        <div class="col-xs-3 spanWidth">
                                                            <asp:Repeater ID="rptValueAppliedAbove" runat="server" OnItemDataBound="rptValueAppliedAbove_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                                    <asp:Label ID="lblCurrency" CssClass="currSym" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                    <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>


                                                    </div>
                                                    <div class="row form-group clearfix">

                                                        <asp:Label ID="lblVAB" runat="server" CssClass="control-label col-xs-3 boldText" Text="Value Applied Below :"></asp:Label>
                                                        <div class="col-xs-3 spanWidth">
                                                            <asp:Repeater ID="rptValueAppliedBelow" runat="server" OnItemDataBound="rptValueAppliedBelow_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                                    <asp:Label ID="lblCurrency" CssClass="currSym" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                    <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-3" id="divPerOrder" runat="server">
                                                            <ul class="bottomUl">
                                                                <li>
                                                                    <span>
                                                                        <asp:RadioButton ID="rdoWeightKG" runat="server" GroupName="PerOrder" Value="PerWeight" />Per
                                                                    </span>
                                                                    <span>
                                                                        <asp:TextBox ID="txtWeight" runat="server"></asp:TextBox>kg
                                                                    </span>
                                                                </li>
                                                                <li></li>

                                                                <li>
                                                                    <asp:RadioButton ID="rdoPerOrder" runat="server" GroupName="PerOrder" Value="PerOrder" />Per Order
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div>
                                                        <asp:CheckBox ID="chkDisallowOrder" runat="server" />&nbsp<span>Disallow Orders below Minimum Value</span>
                                                    </div>
                                                    <div class="col-xs-12 panel-footer">
                                                        <ul class="button_section">
                                                            <li>
                                                                <asp:Button ID="btnNextBand" runat="server" Text="Next Band" class="save" OnClick="btnNextBandRpt_Click" Style="display: none;" CommandArgument=' <%# Container.ItemIndex %>' />
                                                            </li>
                                                            <li>
                                                                <asp:Button ID="btnSaveFreightConfig" runat="server" Text="Save" class="save" OnClick="btnSaveFreightConfigRpt_Click" CommandArgument=' <%# Container.ItemIndex %>' />
                                                            </li>
                                                            <%--            <li>
                                                                <button class="btn  gray" onclientclick="return confirm('are you sure want to cancel ?')" value="" onclick="btnCancel_Click">CANCEL</button>
                                                            </li>--%>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <div class="panel panel-default" id="divBandEditable" runat="server" visible="false">
                                    <div class="panel-heading panelHeading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse1" id="aBand">
                                                <asp:Label ID="lblBand1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="HiddenBandId1" runat="server" />
                                            </a>
                                            <asp:CheckBox ID="checkBandActive1" class="pull-right" runat="server" Checked="true" />
                                        </h4>
                                    </div>
                                    <div id="collapse1" runat="server" class="panel-collapse collapse" clientidmode="Static">
                                        <div class="panel-body">

                                            <div class="row form-group">
                                                <div class="col-md-3">
                                                    <label class="">
                                                        <b>Band Parameters&nbsp:</b>
                                                    </label>
                                                </div>
                                                <div class="col-md-9">
                                                    <ul class="band-form " id="ulWeight1" runat="server">
                                                        <li><b>From &nbsp;</b></li>
                                                        <li>
                                                            <asp:TextBox ID="txtWeightFrom1" runat="server"></asp:TextBox>
                                                            <span>Kg</span>
                                                            <asp:RequiredFieldValidator ID="rfvWeightFrom1" runat="server" ControlToValidate="txtWeightFrom1" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </li>
                                                        <li><b>To &nbsp;</b></li>
                                                        <li>
                                                            <asp:TextBox ID="txtWeightTo1" runat="server"></asp:TextBox>
                                                            <span>Kg</span>
                                                            <asp:RequiredFieldValidator ID="rfvWeightTo1" runat="server" ControlToValidate="txtWeightTo1" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtWeightFrom1" ControlToCompare="txtWeightTo1" Operator="LessThan" Type="Integer" ErrorMessage="The From Weight should be smaller than the To Weight!" ForeColor="Red"></asp:CompareValidator>

                                                        </li>
                                                    </ul>
                                                    <ul class="band-form" id="ulValue1" runat="server" style="display: none;">
                                                        <li>
                                                            <asp:Repeater ID="rptValue" runat="server" OnItemDataBound="rptValue_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><b>From</b> &nbsp;</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtValFrom" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <asp:RequiredFieldValidator ID="rfvValFrom" runat="server" ControlToValidate="txtValFrom" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                        <td>
                                                                            <asp:Label ID="lblCurrencyFrom" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                        </td>
                                                                        <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />

                                                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                                                        <td><b>To</b> &nbsp;</td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtValTo" runat="server"></asp:TextBox>
                                                                        </td>
                                                                        <asp:RequiredFieldValidator ID="rfvValTo" runat="server" ControlToValidate="txtValTo" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>

                                                                        <td>
                                                                            <asp:Label ID="lblCurrencyTo" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>

                                                            </asp:Repeater>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-3">
                                                    <label class="">
                                                        <b>Carriage Method&nbsp:</b>
                                                    </label>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblFreightMethods1" CssClass="tableTdWidth" AutoPostBack="true" OnSelectedIndexChanged="rbl_SelectedIndexChanged"></asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-3 ">
                                                    <label class="">
                                                        <b>Carrier Service&nbsp:</b>
                                                    </label>
                                                </div>
                                                <div class="right_selct_dropdwn relative_data col-md-4">
                                                    <asp:DropDownList ID="ddlCarrierService1" runat="server" CssClass="form-control" onchange="myFunction()" ClientIDMode="Static"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="rfvddlCarrierService" runat="server" ControlToValidate="ddlCarrierService1" InitialValue="0" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please select Carrier Service Text." SetFocusOnError="true" ValidationGroup="valFreightConfig"></asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                            <div class="row form-group" id="divFreightTable1" style="display: none;" runat="server">
                                                <div class="col-md-3 ">
                                                    <label class="">
                                                        <b>Freight Table&nbsp:</b>
                                                    </label>
                                                </div>
                                                <div class="right_selct_dropdwn relative_data col-md-4">
                                                    <asp:DropDownList ID="ddlFreightTable1" runat="server" CssClass="form-control" onchange="myFunction()" ClientIDMode="Static"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlFreightTable1" InitialValue="0" CssClass="text-danger" Display="Dynamic" ErrorMessage="Please select Freight Table Text." SetFocusOnError="true" ValidationGroup="valFreightConfig"></asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-3 ">
                                                    <label class="">
                                                        <b>Transit Time&nbsp:</b>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtTransitTime1" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-md-3 ">
                                                    <label class="">
                                                        <b>Custom Text&nbsp:</b>
                                                    </label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtCustomText1" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row form-group clearfix">

                                                <asp:Label ID="lblMOV" runat="server" CssClass="control-label col-xs-3 boldText" Text="Minimum Order Value :"></asp:Label>
                                                <div class="col-xs-3">
                                                    <asp:Repeater ID="rptMinimumOrderValue" runat="server" OnItemDataBound="rptMinimumOrderValue_ItemDataBound">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                            <asp:Label ID="lblCurrency" CssClass="currSym" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                            <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <br />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>

                                            </div>
                                            <div class="row form-group clearfix">

                                                <asp:Label ID="lblVAA" runat="server" CssClass="control-label col-xs-3 boldText" Text="Value Applied Above :"></asp:Label>
                                                <div class="col-xs-3">
                                                    <asp:Repeater ID="rptValueAppliedAbove" runat="server" OnItemDataBound="rptValueAppliedAbove_ItemDataBound">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                            <asp:Label ID="lblCurrency" CssClass="currSym" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                            <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <br />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>


                                            </div>
                                            <div class="row form-group clearfix">

                                                <asp:Label ID="lblVAB" runat="server" CssClass="control-label col-xs-3 boldText" Text="Value Applied Below :"></asp:Label>
                                                <div class="col-xs-3">
                                                    <asp:Repeater ID="rptValueAppliedBelow" runat="server" OnItemDataBound="rptValueAppliedBelow_ItemDataBound">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdfCurrencyID" runat="server" Value='<%#Eval("CurrencyId") %>' />
                                                            <asp:Label ID="lblCurrency" CssClass="currSym" runat="server" Text='<%#Eval("CurrencySymbol") %>'></asp:Label>
                                                            <asp:TextBox ID="txtCurrencyValue" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvMOV" runat="server" ControlToValidate="txtCurrencyValue" CssClass="text-danger" Text="( Required )" ValidationGroup="valFreightConfig" Display="Dynamic"></asp:RequiredFieldValidator>
                                                            <br />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>

                                                <div class="col-md-6 col-md-offset-3" id="divPerOrder1" runat="server">
                                                    <ul class="bottomUl">
                                                        <li>
                                                            <span>
                                                                <asp:RadioButton ID="rdoWeightKG1" runat="server" GroupName="PerOrder" Value="PerWeight" />Per
                                                            </span>
                                                            <span>
                                                                <asp:TextBox ID="txtWeight1" runat="server"></asp:TextBox>kg
                                                            </span>
                                                        </li>
                                                        <li></li>

                                                        <li>
                                                            <asp:RadioButton ID="rdoPerOrder1" runat="server" GroupName="PerOrder" Value="PerOrder" />Per Order
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div>
                                                <asp:CheckBox ID="chkDisallowOrder1" runat="server" />&nbsp<span>Disallow Orders below Minimum Value</span>
                                            </div>
                                            <div class="col-xs-12 panel-footer">
                                                <ul class="button_section">
                                                    <li>
                                                        <asp:Button ID="btnNextBand" runat="server" Text="Next Band" class="save" OnClick="btnNextBand_Click" Style="display: none;" />
                                                    </li>
                                                    <li>
                                                        <asp:Button ID="btnSaveFreightConfig" runat="server" Text="Save" class="save" OnClick="btnSaveFreightConfig_Click1" />
                                                    </li>
                                                    <li>
                                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="save" OnClick="btnCancel_Click" />

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
<%--    <script type="text/javascript">
      $(document).ready(function () {
            $("#rblFreightModes").prop("checked", true).trigger("click");
      });
        </script>--%>
</asp:Content>
