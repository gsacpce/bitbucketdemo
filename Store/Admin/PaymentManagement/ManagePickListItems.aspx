﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"  Inherits="Presentation.Admin_PaymentManagement_ManagePickListItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Payment Management </a></li>
            <li>Manage PickList Item</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">Payment Type PickList Items</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="sm_select">Select Payment Types :</label>
                        </div>
                        <div id="div1" runat="server" class="col-md-3">
                              <div class="select-style  selectpicker languageDropdown">
                                <asp:HiddenField ID="hdnSelectedPaymentTypeId" runat="server" />
                                <asp:DropDownList ID="ddlPaymentTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPaymentTypes_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvPaymentTypes" runat="server" InitialValue="0" ControlToValidate="ddlPaymentTypes" Display="None" ErrorMessage="Please select Payment Types" ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
                              </div>
                        </div>                         
                        <div class="col-md-3  text-right">
                            <label class="text-right sm_select" id="select_lang">Select Language :</label>
                        </div>
                        <div  id="divLang" runat="server" class="col-md-3">
                            <div class="select-style  selectpicker languageDropdown">
                                <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                   </div>
             
                    <br/><br/>
                    <div class="button_section text-center">
                    <div class="">
                        <asp:GridView ID="grdPickList" runat="server" AutoGenerateColumns="False" class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra" DataKeyNames="Id" GridLines="Both">
                            <Columns>
                                <asp:TemplateField HeaderText="Pay Value" >
                                    <ItemTemplate>
                                        <%# Eval("PayValue") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="50%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Display Name">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDisplayName" runat="server" Text='<%# Eval("DisplayName") %>' />
                                        <asp:RequiredFieldValidator ID="rfvPaymentType" runat="server" ControlToValidate="txtDisplayName"
                                            Display="dynamic" Text="(Required)" ForeColor="Red"
                                            ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="center" Width="50%" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Show on Website">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsShow" runat="server" Checked='<%# Eval("IsShowOnWebsite") %>' />
                                    </ItemTemplate>
                                     <ItemStyle HorizontalAlign="center" Width="30%" />
                                </asp:TemplateField>--%>
                            </Columns>
                            <EmptyDataTemplate>
                                <span class="GridHeading">Currently There are no Items.</span>
                            </EmptyDataTemplate>
                             <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom"/>
                            <PagerStyle CssClass="paging" />
                            <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                            <HeaderStyle CssClass="GridHeading" />
                            <AlternatingRowStyle CssClass="GridAlternateItem" />
                        </asp:GridView>
                    </div>
                    </div>
                    <div>
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSubmit" Text="Submit" CssClass="btn" runat="server" Visible="" ValidationGroup="OnClickSubmit" OnClick="btnSubmit_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>     
    </div>
</asp:Content>


