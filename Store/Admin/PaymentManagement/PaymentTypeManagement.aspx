﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_PaymentManagement_PaymentTypeManagement" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>		
	    th, td{text-align: center;width: 10%;}		
	</style>
<script type="text/javascript">

    function CheckOtherIsCheckedByGVID(rdBtnDefault, value) {
        var isChecked = rdBtnDefault.checked;
        var row = rdBtnDefault.parentNode.parentNode;
        if (isChecked) {
            //row.style.backgroundColor = '#B6C4DE';
            //row.style.color = 'black';
        }
        var currentRdbID = rdBtnDefault.id;
        parent = document.getElementById("<%= grdwPayments.ClientID %>");
        var items = parent.getElementsByTagName('input');

        for (i = 0; i < items.length; i++) {
            if (items[i].id != currentRdbID && items[i].type == "radio" && items[i].value == value) {
                if (items[i].checked) {
                    items[i].checked = false;
                    //items[i].parentNode.parentNode.style.backgroundColor = 'white';
                    //items[i].parentNode.parentNode.style.color = '#696969';
                }
            }
        }
    }
</script>

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>Payment Type management</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">Payment Type management</h3>
                    <div id="divMessage" visible="false" runat="server" class="alert alert-success">

                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>
                            <asp:Literal ID="ltrMessage" runat="server" />
                        </strong>
                    </div>
                    <div class="col-md-5  text-right">

                        <label class="col-md-5 text-right sm_select" id="select_lang">
                            Select Language :
                        </label>
                        <div style="float: right" id="divLang" runat="server" class="col-md-7">
                            <div class="select-style  selectpicker languageDropdown">
                                <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div>
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnGetPaymentType" runat="server" CssClass="btn" Text="Get Payment Types"
                                    ToolTip="Get all the payment types assigned to this Store" OnClick="btnGetPaymentType_Click" />
                            </li>
                        </ul>
                    </div>
                    <div class="text-center">
                        <asp:GridView ID="grdwPayments" runat="server" AutoGenerateColumns="False" DataKeyNames="PaymentTypeID" GridLines="Both" OnRowDataBound="grdwPayments_RowDataBound">
                            <Columns>
                                <%--<asp:TemplateField HeaderText="Payment Types">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPaymentType" runat="server" Text='<%# Eval("PaymentTypeRef") %>' />
                                        <asp:RequiredFieldValidator ID="rfvPaymentType" runat="server" ControlToValidate="txtPaymentType"
                                            Display="dynamic" Text="(Required)" ForeColor="Red"
                                            ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="30%" />
                                </asp:TemplateField> /*User Type*/--%>
                                <asp:TemplateField HeaderText="Display Name">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPaymentType" runat="server" Text='<%# Eval("DisplayName") %>' Width="300px" />
                                        <asp:RequiredFieldValidator ID="rfvPaymentType" runat="server" ControlToValidate="txtPaymentType"
                                            Display="dynamic" Text="(Required)" ForeColor="Red"
                                            ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Show on Website">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsShow" runat="server" Checked='<%# Eval("IsShowOnWebsite") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Invoice Account ID">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAccID" runat="server" Checked='<%# Eval("InvoiceAccountId") %>'  />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Default">
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rdBtnDefault" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this,'rdBtnDefault');" Text='<%# Eval("IsDefault") %>' />
                                       <%--  <asp:HiddenField ID="HiddenField1" runat="server"  Value = '<%#Eval("IsDefault")%>' />--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsBudget">
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rdBtnBudget"  runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this,'rdBtnBudget');" Checked='<%# Eval("IsBudget") %>' />
                                       <%--  <asp:HiddenField ID="HiddenField1" runat="server"  Value = '<%#Eval("IsDefault")%>' />--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="IsPoint">
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rdBtnPoint" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this,'rdBtnPoint');" Checked='<%# Eval("IsPoint") %>' />
                                       <%--  <asp:HiddenField ID="HiddenField1" runat="server"  Value = '<%#Eval("IsDefault")%>' />--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <span class="GridHeading">Currently There are no payment type.</span>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div>
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSubmit" Text="Submit" CssClass="btn" runat="server" ValidationGroup="OnClickSubmit" OnClick="btnSubmit_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
