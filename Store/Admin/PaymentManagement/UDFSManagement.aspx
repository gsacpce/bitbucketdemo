﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_PaymentManagement_UDFSManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">

        function CheckOtherIsCheckedByGVID(rdBtnDefault, value) {
            var isChecked = rdBtnDefault.checked;
            var row = rdBtnDefault.parentNode.parentNode;
            if (isChecked) {
                //row.style.backgroundColor = '#B6C4DE';
                //row.style.color = 'black';
            }
            var currentRdbID = rdBtnDefault.id;
            parent = document.getElementById("<%= grdwUDFS.ClientID %>");
            var items = parent.getElementsByTagName('input');

            //if (value == "rdBtnDefault")
            //    {
            for (i = 0; i < items.length; i++) {
                if (items[i].id != currentRdbID && items[i].type == "radio" && items[i].value == value) {
                    if (items[i].checked) {
                        items[i].checked = false;
                        //items[i].parentNode.parentNode.style.backgroundColor = 'white';
                        //items[i].parentNode.parentNode.style.color = '#696969';
                    }
                }
            }
            //}
            //else
            //{
            //    for (i = 0; i < items.length; i++) {
            //        if (items[i].id != currentRdbID && items[i].type == "radio" && items[i].value != "rdBtnDefault") {
            //            if (items[i].checked) {
            //                items[i].checked = false;
            //                //items[i].parentNode.parentNode.style.backgroundColor = 'white';
            //                //items[i].parentNode.parentNode.style.color = '#696969';
            //            }
            //        }
            //    }
            //}
        }
    </script>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="javascript:void(0);">Settings </a></li>
            <li>UDFS management</li>
        </ul>
    </div>
    <div class="admin_page">
        <div class="container ">
            <div class="wrap_container">
                <div class="content">
                    <h3 class="mainHead">UDFS management</h3>
                    <div id="divMessage" visible="false" runat="server" class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>
                            <asp:Literal ID="ltrMessage" runat="server" /></strong>
                    </div>
                    <div class="row">
                    <div class="col-md-5  text-right" style="float: right;">

                        <label class="col-md-5 text-right sm_select" id="select_lang">
                            Select Language :
                        </label>
                        <div style="float: right" id="divLang" runat="server" class="col-md-7">
                            <div class="select-style  selectpicker languageDropdown">
                                <asp:DropDownList ID="ddlLanguage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-5  text-right">
                        <label class="col-md-5 text-right sm_select">
                            Select Payment Types :
                        </label>
                        <div style="float: right" id="div1" runat="server" class="col-md-7">
                            <div class="select-style  selectpicker languageDropdown">
                                <asp:HiddenField ID="hdnSelectedPaymentTypeId" runat="server" />
                                <asp:DropDownList ID="ddlPaymentTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPaymentTypes_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvPaymentTypes" runat="server" InitialValue="0" ControlToValidate="ddlPaymentTypes" Display="None" ErrorMessage="Please select Payment Types"
                                    ValidationGroup="OnClickSubmit"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="button_section text-center">
                        <asp:GridView ID="gridCustomRef" runat="server" AutoGenerateColumns="False" class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra" DataKeyNames="CustomerRefID" GridLines="Both">
                            <Columns>
                                <asp:TemplateField HeaderText="Caption" ItemStyle-Width="300px">
                                    <ItemTemplate>
                                        <asp:Literal ID="lblCaptionCust" runat="server" Text='<%# Eval("Caption") %>'></asp:Literal>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Display Name" ItemStyle-Width="300px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDisplayNameCust" runat="server" Text='<%# Eval("DisplayName") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="30%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Mandatory">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsMandatoryCust" runat="server" Checked='<%# Eval("IsMandatory") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="20%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Visible">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsVisibleCust" runat="server" Checked='<%# Eval("IsVisible") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="20%" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                            <PagerStyle CssClass="paging" />
                            <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                            <HeaderStyle CssClass="GridHeading" />
                            <AlternatingRowStyle CssClass="GridAlternateItem" />
                        </asp:GridView>
                    </div>

                    <div class="button_section text-center">
                        <asp:GridView ID="grdwUDFS" runat="server" AutoGenerateColumns="False" class="all_customer_inner1 inner1newcls  dragbltop dragbltopextra" DataKeyNames="UDFOasisID" GridLines="Both">
                            <Columns>
                                <asp:TemplateField HeaderText="Caption" ItemStyle-Width="300px">
                                    <ItemTemplate>
                                        <asp:Literal ID="lblCaption" runat="server" Text='<%# Eval("Caption") %>'></asp:Literal>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="20%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Display Name" ItemStyle-Width="300px">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDisplayName" runat="server" Text='<%# Eval("DisplayName") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="20%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Mandatory">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsMandatory" runat="server" Checked='<%# Eval("IsMandatory") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="20%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Is Visible">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsVisible" runat="server" Checked='<%# Eval("IsVisible") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" Width="20%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="IsBudget">
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rdBtnBudget"  runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this,'rdBtnBudget');" Checked='<%# Eval("IsBudget") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30%" />
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="IsPoint">
                                    <ItemTemplate>
                                        <asp:RadioButton ID="rdBtnPoint" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this,'rdBtnPoint');" Checked='<%# Eval("IsPoint") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30%" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
                            <PagerStyle CssClass="paging" />
                            <RowStyle CssClass="GridContentText" BorderWidth="1" BorderStyle="Solid" />
                            <HeaderStyle CssClass="GridHeading" />
                            <AlternatingRowStyle CssClass="GridAlternateItem" />
                        </asp:GridView>
                    </div>
                    <div>
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSubmit" Text="Submit" CssClass="btn" runat="server" ValidationGroup="OnClickSubmit" OnClick="btnSubmit_Click" />
                            </li>
                        </ul>
                    </div>
                    <div>
                        <asp:Label ID="lblNoRecords" runat="server"></asp:Label>
                    </div>
                    <asp:ValidationSummary ID="valUDFSDetails" runat="server" CssClass="ErrorText"
                        ShowMessageBox="True" ShowSummary="False" ValidationGroup="OnClickSubmit" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
