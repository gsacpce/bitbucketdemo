﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true"
    Inherits="Presentation.Admin_MasterPageManagement_HeaderConfigure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .gm-content, .column {
            padding: 20px 0;
            margin: 0px;
            text-align: center;
            border: 2px dotted #808080;
        }

        }

        #gm-canvas .gm-editable-region {
            border: 0px;
        }
    </style>
    <style>
        .gridMainContainer {
            width: 98%;
            margin: 0 auto;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <h3>Configure Header Content</h3>
    </div>

    <div class="admin_page">
        <section class="container gridMainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">


                    <div class="container">
                        <br />
                        <br />
                        <div id="myCanvas" runat="server">
                        </div>
                        <br />
                        <br />
                        <div class="button_section">
                            <asp:Button ID="btnSaveLayout" CssClass="btn" runat="server" Text="Save Header configuration >> " OnClientClick="javascript: funGenHTML();return false; " />
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <!--================== JS ================-->
    <script type="text/javascript">


        var hostAdmin = '<%=HostAdmin %>';

        // var x = $(".Column").hasAttribute("itemtype");

        function funContainerMode(curChkBox) {
            //debugger;
            chkBox = $(curChkBox);
            var checked = chkBox.prop('checked');
            if (checked == true) {
                chkBox.parent().find('.containermode').val('container');
            }
            else {
                chkBox.parent().find('.containermode').val('containerfullwidth');
                //chkBox.parent().parent().attr('class').replace('container','container-fluid');
            }

        }

        function funInnerContainer(curChkBox) {
            //debugger;
            chkBox = $(curChkBox);
            var checked = chkBox.prop('checked');
            if (checked == true) {
                chkBox.parent().find('.innercontainermode').val('nav');
            }
            else {
                chkBox.parent().find('.innercontainermode').val('HEADER_CATEGORIES');
                //chkBox.parent().parent().attr('class').replace('container','container-fluid');
            }

        }

        $(".column").each(function () {


            var attr = $(this).attr('itemtype');

            if (typeof attr !== typeof undefined && attr !== false) {
                // ...
                //debugger;
                var ddl = $(this).find('select');
                if (ddl != null)
                    $(this).addClass('gm-content');

            }



        })


        function funGenHTML(jsondata) {
            //debugger;
            var _data = JSON.stringify({ 'PageLayout': $("#" + '<%= myCanvas.ClientID %>').html(), 'jsondata': jsondata });
            $.ajax({
                type: "POST",
                url: "HeaderConfigure.aspx/SavePageHeader",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: OnSuccess,
                failure: function (response) {
                    alert("There is an error saving Home page header configurations.");
                }
            });
        }

        function OnSuccess(response) {

            alert("Home page header configurations saved successfully!");
            //var responseData = JSON.parse(response);
            window.location = '<%=HostAdmin %>Dashboard/Dashboard.aspx';
            //alert(responseData.d);
            //if (responseData.d != "")
            //    alert(responseData.d);
        }

        $(document).ready(function () {
            funGridManagerHide();
        });

        function funGridManagerHide() {
            $("#gm-controls").hide();
            $(".pull-left").hide();
            $(".gm-0_btn").hide();
            $(".gm-1_btn").hide();
            $(".pull-right").hide();
            $("#gm-canvas .gm-editable-region .gm-controls-element").hide();
            $("#gm-canvas .gm-tools").hide();
            $(".gm-controls-element").hide();
            $(".gm-content").attr("contenteditable", false);
        }

        function funSaveDDLValue(ddl, hdn) {
            $("#" + hdn + "").val($("#" + ddl + "").val());
            if ($("#" + ddl + "").val() != '0')
                BindPopupBox($("#" + ddl + "").val(), $("#" + hdn + "").parent().attr("id"));
        }

        function BindPopupBox(type, placeholderId) {
            $('#myModal').find('.modal-body').html('');
            try {
                $.ajax({
                    type: "POST",
                    url: hostAdmin + 'SliderManagement/SliderListing_ajax.aspx',
                    data: {
                        itemtype: type,
                        placeholder: placeholderId
                    },
                    cache: false,
                    async: false
                }).done(function (response) {
                    $('#myModal').find('.modal-body').html(response);
                    $('#myModal').find('.modal-title').html('SlideShow');
                    $('#myModal').find('.modal-footer').hide();
                    $('#myModal').modal('show');
                });
            } catch (e) { }
            finally { }
        }

        function funValidateConfiguration() {
            var allSectionsConfigured = true;
            $(".ddlContent").each(function (i) {
                if ($(this).val() == '0') {
                    allSectionsConfigured = false;
                }
            });
            var jsondata = '';
            $(".gm-content").each(function (i) {
                if ($(this).attr('data-value') == undefined || $(this).attr('itemtype') == undefined) {
                    allSectionsConfigured = false;
                }
                else {
                    jsondata += '{ "ConfigSectionName":"' + $(this).attr('id') + '" , "ConfigElementName":"' + $(this).attr('itemtype') + '" , "ConfigElementID":"' + $(this).attr('data-value') + '" },';
                }
            });
            if (allSectionsConfigured == true) {
                jsondata = jsondata.substring(0, jsondata.length - 1);
                jsondata = '[' + jsondata + ']';
                funGenHTML(jsondata);
            }
            else
                alert("Kindly select required elements in all sections");
        }

        $(document).on('click', '.itemclick', function () {
            $('#' + $(this).attr('placeholder')).attr('data-value', $(this).attr('data-value'));
            $('#' + $(this).attr('placeholder')).attr('itemtype', $(this).attr('itemtype'));
            $('#' + $(this).attr('placeholder')).find('span').html($(this).attr('itemtype'));
            $('#myModal').modal('hide');
        });

    </script>
</asp:Content>
