﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_MasterPageManagement_FooterManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <title>Home page header - Layout manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="dist/css/jquery.gridmanager.css" rel="stylesheet">
    <link href="css/demo.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="dist/js/jquery.gridmanager.js"></script>
    <style>
        .gm-addRow,
        .gm-0_btn,
        .gm-1_btn,
        .gm-rowSettings,
        .gm-colSettings,
        .gm-removeRow,
        #gm-canvas .gm-editable-region .gm-controls-element,
        /*#gm-controls*/
        /*
            Sachin Chauhan Start : 17 03 2016 : Footer management feature is modified to have new elments add / remove feature
             .gm-removeCol,
        .gm-addColumn,
        
        */
        #gm-addnew {
            /* display: none !important;*/
        }

        #gm-canvas .gm-editable-region {
            border-color: #444;
        }
    </style>

    <style>
        .gridMainContainer {
            width: 1250px;
            margin: 0 auto;
        }

        .gm-edit-mode, .gm-preview, .gm-save, .gm-resetgrid, #gm-canvas .gm-editable-region .gm-controls-element {
            display: none !important;
        }
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Home Page</a></li>
            <li>Manage Footer Layout</li>
        </ul>
    </div>
    <div class="container">
        <h3>Manage Footer Layout</h3>
    </div>
    <div class="admin_page">
        <section class="container gridMainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content ">

                    <div class="myContainer" clientidmode="Static" runat="server">
                        <div id="myCanvas" clientidmode="Static" runat="server">
                        </div>
                    </div>
                    <div class="button_section">
                        <asp:Button ID="btnSaveLayout" CssClass="btn" runat="server" Text="Save Footer Layout >> " OnClientClick="javascript: funGenHTML();return false; " />&nbsp;&nbsp;
              <%--<asp:Button ID="btnConfigureLayout" CssClass="btn" runat="server" Text="Configure Footer Contents" PostBackUrl="FooterConfigure.aspx" />--%>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <!--================== JS ================-->
    <script>
        $(document).ready(function () {

            $("#myCanvas").gridmanager({
                debug: 1
            }
             );


        });

        function funGenHTML() {
            //debugger;
            var gm = jQuery("#myCanvas").gridmanager().data('gridmanager');
            gm.cleanup();
            gm.deinitCanvas();

            var _data = JSON.stringify({ 'PageLayout': $("#gm-canvas").html() });
            //var _data = JSON.stringify({ 'PageLayout': $("#gm-canvas").html(), 'Container': $("#chkContainer").prop('checked') });
            //alert(_data);

            $.ajax({
                type: "POST",
                url: "FooterManager.aspx/SavePageFooter",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: OnSuccess,
                failure: function (response) {
                    alert("There is an error saving page footer layout.");
                }
            });
        }
        function OnSuccess(response) {
            var responseData = response.d;
            alert("The page footer layout saved successfully!");
            window.location.href = "FooterConfigure.aspx";
            if (responseData[0] != "")
                alert(responseData[0]);
        }
    </script>
</asp:Content>
