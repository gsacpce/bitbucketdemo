﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" Inherits="Presentation.Admin_HomePageContentListing" %>

<script runat="server">

    protected void gvStaticImage_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Home Page</a></li>
            <li>Home Page Content Management</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContaJiner padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Home Page Content Management</h3>
                    <span class="sectionText">Manage Static Text , Images & Video links for Home Page.</span>
                    <!-- Website tab start -->

                    <div class="butonblock nospace clearfix ">
                        <ul class="categories_btn" style="margin: 0;">
                            <li>
                                <div class="select-style selectpicker">
                                    <asp:DropDownList ID="ddlContentType" OnSelectedIndexChanged="ddlContentType_SelectedIndexChanged" runat="server" AutoPostBack="true">
                                        <asp:ListItem Text="Static Image" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Static Text" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Site Logo" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Footer Logo" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Footer Text" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Footer Copy Right" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Upload Document" Value="8"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </li>
                            <li>
                                <asp:Button runat="server" ID="btnAddNew" CommandArgument="0" CssClass="btn3" OnClick="Btn_Click" CommandName="Add" Text="Add New Home Page Content" />
                            </li>

                        </ul>
                        <div class="clear">
                        </div>

                    </div>

                    <!--mainOutContainer start-->
                    <div class="midleDrgblecontin">
                        <div id="divStaticImage" runat="server" clientidmode="static">
                            <h4>Static Image</h4>
                            <!--removed dragblnextBlock for few section of ul-->
                            <ul class="dragblnextBlock">
                                <li>
                                    <asp:GridView ID="gvStaticImage" runat="server" AutoGenerateColumns="false" Width="100%"
                                        class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                        OnPageIndexChanging="gvStaticImage_PageIndexChanging" EmptyDataText="No record found."
                                        AllowSorting="false" PageSize="30" OnRowDataBound="gvStaticImage_RowDataBound" OnRowDeleting="gvStaticImage_RowDeleting">
                                        <Columns>
                                            <asp:BoundField HeaderText="Title" HeaderStyle-CssClass="bg" DataField="Title"
                                                ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                                <ItemStyle />
                                            </asp:BoundField>

                                            <asp:BoundField HeaderText="Alt Name" HeaderStyle-CssClass="bg" DataField="Content"
                                                ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                                <ItemStyle />
                                            </asp:BoundField>


                                            <asp:TemplateField HeaderText="Navigate Url">
                                                <ItemTemplate>

                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Url")' Text='<%# DataBinder.Eval(Container.DataItem,"Url") %>' Style="word-wrap: break-word; word-break: break-all;"></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Thumb Nail">
                                                <ItemTemplate>
                                                    <asp:Image ID="ImgThumbNail" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem,"ThumbNail") %>' Height="100" Width="100" />
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                                SortExpression="Status" HeaderStyle-Width="7%">
                                                <ItemTemplate>
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="view_order other_option">

                                                                <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                    CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>' />
                                                                <asp:LinkButton Text="Delete" runat="server" OnCommand="Btn_Click" ID="lnkbtnDelete" OnClientClick="return confirm('Are you sure deleting item?')"
                                                                    CommandName="<%# DBAction.Delete.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>' />

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Such Record Found !!!
                                        </EmptyDataTemplate>
                                        <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                            LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                        <PagerStyle CssClass="paging" />
                                    </asp:GridView>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="divVideo" runat="server" clientidmode="static">
                            <h4>Video</h4>
                            <ul class="dragblnextBlock">
                                <li>
                                    <asp:GridView ID="gvVideo" runat="server" AutoGenerateColumns="false" Width="100%"
                                        class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                        OnPageIndexChanging="gvVideo_PageIndexChanging" EmptyDataText="No record found."
                                        AllowSorting="false" PageSize="30" OnRowDataBound="gvVideo_RowDataBound" OnRowDeleting="gvStaticImage_RowDeleting">
                                        <Columns>
                                            <asp:BoundField HeaderText="Title" HeaderStyle-CssClass="bg" DataField="Title"
                                                ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                                <ItemStyle />
                                            </asp:BoundField>


                                            <asp:TemplateField HeaderText="Video Link">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="HLinkVideoLink" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Url") %>' Text='<%# DataBinder.Eval(Container.DataItem,"Url") %>'></asp:HyperLink>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField HeaderText="Alt Name" HeaderStyle-CssClass="bg" DataField="Content"
                                                ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                                <ItemStyle />
                                            </asp:BoundField>

                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                                SortExpression="Status" HeaderStyle-Width="7%">
                                                <ItemTemplate>
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="view_order other_option">

                                                                <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                    CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>' />
                                                                <asp:LinkButton Text="Delete" runat="server" OnCommand="Btn_Click" ID="lnkbtnDelete" OnClientClick="return confirm('Are you sure deleting item?')"
                                                                    CommandName="<%# DBAction.Delete.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>' />

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Such Record Found !!!
                                        </EmptyDataTemplate>
                                        <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                            LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                        <PagerStyle CssClass="paging" />
                                    </asp:GridView>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <br />

                        <div id="divStaticText" runat="server" clientidmode="static">
                            <h4>Static Text</h4>
                            <ul class="dragblnextBlock">
                                <li>
                                    <asp:GridView ID="gvStaticText" runat="server" AutoGenerateColumns="false" Width="100%"
                                        class="all_customer_inner dragbltop  " BorderWidth="0" CellSpacing="0" CellPadding="0" AllowPaging="true"
                                        OnPageIndexChanging="gvStaticText_PageIndexChanging" EmptyDataText="No record found."
                                        AllowSorting="false" PageSize="30" OnRowDataBound="gvStaticText_RowDataBound" OnRowDeleting="gvStaticImage_RowDeleting">
                                        <Columns>
                                            <asp:BoundField HeaderText="Title" HeaderStyle-CssClass="bg" DataField="Title"
                                                ItemStyle-Width="30px" HeaderStyle-Width="20%">
                                                <ItemStyle VerticalAlign="Top" />
                                            </asp:BoundField>

                                            <asp:TemplateField HeaderText="Content" HeaderStyle-CssClass="bg">
                                                <ItemTemplate>
                                                    <div id="divStaticContent" runat="server"></div>
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                                SortExpression="Status" HeaderStyle-Width="7%">
                                                <ItemTemplate>
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="view_order other_option">

                                                                <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate"
                                                                    CommandName="<%# DBAction.Update.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>' />
                                                                <asp:LinkButton Text="Delete" runat="server" OnCommand="Btn_Click" ID="lnkbtnDelete" OnClientClick="return confirm('Are you sure deleting item?')"
                                                                    CommandName="<%# DBAction.Delete.ToString() %>" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HomePageStaticContentId") %>' />

                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Such Record Found !!!
                                        </EmptyDataTemplate>
                                        <PagerSettings Mode="NumericFirstLast" Position="Top" FirstPageText="First" PageButtonCount="6"
                                            LastPageText="Last" NextPageText="Next" PreviousPageText="Prev" />
                                        <PagerStyle CssClass="paging" />
                                    </asp:GridView>
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="divSiteLogo" runat="server" clientidmode="static">
                            <h4>Site Logo</h4>


                            <ul class="dragblnextBlock">
                                <li>

                                    <asp:GridView ID="Gv_imgs" Width="100%" class="all_customer_inner dragbltop " BorderWidth="0" CellSpacing="0" CellPadding="0" runat="server" AutoGenerateColumns="false" ShowHeader="true">
                                        <Columns>

                                            <asp:BoundField DataField="Text" HeaderText="Name" HeaderStyle-Width="20%" />
                                            <asp:ImageField ControlStyle-Height="150px" HeaderStyle-Width="45%" DataImageUrlField="value" HeaderText="Logo" />

                                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="bg" AccessibleHeaderText="Action"
                                                SortExpression="Status" HeaderStyle-Width="7%">
                                                <ItemTemplate>
                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="view_order other_option">
                                                                <asp:LinkButton Text="Edit" PostBackUrl="~/Admin/HomePageManagement/HomePageContentManagement.aspx?id=4" runat="server" OnCommand="Btn_Click" ID="lnkbtnUpdate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>


                                </li>

                            </ul>


                            <div class="clearfix"></div>
                        </div>
                        <div id="divFooterLogo" runat="server" clientidmode="static">
                            <h4>Footer Logo</h4>
                            <ul>
                                <li>
                                    <img src="" runat="server" id="ImgFooterLogo" clientidmode="static" />

                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="divFooterText" runat="server" clientidmode="static">
                            <h4>Footer Text</h4>
                            <ul>
                                <li>
                                    <table width="50%" cellspacing="2" cellpadding="2">
                                        <tr>
                                            <td width="85%">
                                                <asp:Literal ID="ltlFooterText" runat="server"></asp:Literal></td>
                                            <td>
                                                <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" CssClass="btn" ID="btnFooterUpdate" rel="FooterText" CommandName="update" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:HiddenField ID="hdnFooterTextId" runat="server" />

                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="divCopyRightInfo" runat="server" clientidmode="static">
                            <h4>Copy Right Info</h4>
                            <ul>
                                <li>
                                    <asp:HiddenField ID="hdnCopyRightId" runat="server" />
                                    <asp:Literal ID="ltlCopyRight" runat="server"></asp:Literal>
                                    <asp:LinkButton Text="Edit" runat="server" OnCommand="Btn_Click" ID="btnCopyRightUpdate" CssClass="btn" rel="FooterCopyRight"
                                        CommandName="update" />
                                </li>

                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div id="divUploadDocument" runat="server" clientmode="static">
                            <h4>Uploaded Document</h4>
                            <ul class="dragblnextBlock">
                                <li>
                                    <asp:GridView ID="grdUploadedDocuments" Width="100%" class="all_customer_inner dragbltop " BorderWidth="0" CellSpacing="0" CellPadding="0" runat="server" AutoGenerateColumns="false" ShowHeader="true" OnRowDataBound="grdUploadedDocuments_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="FileName">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltrFileName" runat="server" Text='<%# Eval("Filename") %>' Visible="false"></asp:Literal>
                                                    <asp:LinkButton ID="lnkFilename" runat="server" Text='<%# Eval("Filename") %>' ForeColor="Blue"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FilePath" HeaderText="FilePath" />
                                            <asp:TemplateField HeaderText="Uploaded PDFs" HeaderStyle-CssClass="bg" AccessibleHeaderText="Uploaded PDFs"
                                                HeaderStyle-Width="7%">
                                                <ItemTemplate>

                                                    <div class="action">
                                                        <a class="icon" href="#"></a>
                                                        <div class="action_hover">
                                                            <span></span>
                                                            <div class="view_order other_option">
                                                                <asp:LinkButton ID="lnkPDFDelete" Text="Delete" CommandName='<%# Eval("Filename") %>' runat="server" OnClick="lnkDelete_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</asp:Content>

