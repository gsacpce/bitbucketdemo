﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_HomePageManagement_LayoutConfigure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">

    <style>
        .gmcontent {
            padding: 20px 0;
            margin: 0px;
            text-align: center;
            border: 2px dotted #808080;
        }

        #gm-canvas .gm-editable-region {
            border: 0px;
        }

        .chkContainer {
            display: block;
            float: left;
            width: 100%;
        }

        .admin_page .wrap_container {
            width: 1160px;
            margin: 0 auto;
        }
    </style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Home Page</a></li>
            <li>Configure Home Page Elements</li>
        </ul>
    </div>
    <div class="container">
        <h3>Configure Home Page Elements - Step 2</h3>
    </div>
    <div class="admin_page">
        <section class="container mainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <div class="">
                        <br />
                        <br />
                        <div id="myCanvas" runat="server">
                        </div>
                        <br />
                        <br />
                        <div class="row">
                            <div class="button_section clearfix">
                                <div class="col-md-12 ">
                                    <%--<asp:Button ID="btnSavePreview" CssClass="btn" runat="server" Text="Preview Home Page >> " OnClientClick="javascript:funValidateConfiguration(true);return false;" />&nbsp;&nbsp;--%>
                                    <asp:Button ID="btnSaveLayout" CssClass="btn" runat="server" Text="Save page configuration >> " OnClientClick="javascript:funValidateConfiguration(false);return false;" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <!--================== JS ================-->
    <script type="text/javascript">


        var hostAdmin = '<%=HostAdmin %>';

        // var x = $(".Column").hasAttribute("itemtype");

        $(".column").each(function () {


            var attr = $(this).attr('itemtype');

            if (typeof attr !== typeof undefined && attr !== false) {
                // ...
                //debugger;
                var ddl = $(this).find('select');
                if (ddl != null)
                    $(this).addClass('gmcontent');

            }



        })



        function funContainerMode(curChkBox) {
            //debugger;
            chkBox = $(curChkBox);
            var checked = chkBox.prop('checked');
            if (checked == true) {
                chkBox.parent().find('.containermode').val('container');
            }
            else {
                chkBox.parent().find('.containermode').val('containerfullwidth');
                //chkBox.parent().parent().attr('class').replace('container','container-fluid');
            }

        }

        function funGenHTML(jsondata, isPreview) {
            var _data = JSON.stringify({ 'PageLayout': $("#" + '<%= myCanvas.ClientID %>').html(), 'jsondata': jsondata, 'isPreview': isPreview });
            $.ajax({
                type: "POST",
                url: "LayoutConfigure.aspx/SavePageLayout",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: function (isPreview) {
                    if (isPreview)
                        alert("Home page layout configurations saved successfully for Preview!");
                    else
                        alert("Home page layout configurations saved successfully!");
                },
                failure: function (response) {
                    alert("There is an error saving Home page layout configurations.");
                }
            });
        }

        function OnSuccess(response) {


            //alert(responseData.d);
            //if (responseData.d != "")
            //    alert(responseData.d);
        }

        $(document).ready(function () {
            funGridManagerHide();
        });

        function funGridManagerHide() {
            $("#gm-controls").hide();
            $(".pull-left").hide();
            $(".gm-0_btn").hide();
            $(".gm-1_btn").hide();
            $(".pull-right").hide();
            $("#gm-canvas .gm-editable-region .gm-controls-element").hide();
            $("#gm-canvas .gm-tools").hide();
            $(".gm-controls-element").hide();
            $(".gm-content").attr("contenteditable", false);
        }

        function funSaveDDLValue(ddl, hdn) {
            $("#" + hdn + "").val($("#" + ddl + "").val());
            if ($("#" + ddl + "").val() != '0')
                BindPopupBox($("#" + ddl + "").val(), $("#" + hdn + "").parent().attr("id"));
        }

        function BindPopupBox(type, placeholderId) {
            $('#myModal').find('.modal-body').html('');
            try {
                $.ajax({
                    type: "POST",
                    url: hostAdmin + 'SliderManagement/SliderListing_ajax.aspx',
                    data: {
                        itemtype: type,
                        placeholder: placeholderId
                    },
                    cache: false,
                    async: false
                }).done(function (response) {
                    $('#myModal').find('.modal-body').html(response);
                    $('#myModal').find('.modal-title').html('SlideShow');
                    $('#myModal').find('.modal-footer').hide();
                    $('#myModal').modal('show');
                });
            } catch (e) { }
            finally { }
        }

        function funValidateConfiguration(isPreview) {
            //debugger;
            var allSectionsConfigured = true;
            $(".ddlContent").each(function (i) {
                if ($(this).val() == '0') {
                    allSectionsConfigured = false;
                }
            });
            var jsondata = '';
            $(".gmcontent").each(function (i) {
                if ($(this).attr('data-value') == undefined || $(this).attr('itemtype') == undefined) {
                    allSectionsConfigured = false;
                }
                else {
                    jsondata += '{ "ConfigSectionName":"' + $(this).attr('id') + '" , "ConfigElementName":"' + $(this).attr('itemtype') + '" , "ConfigElementID":"' + $(this).attr('data-value') + '" },';
                }
            });
            if (allSectionsConfigured == true) {

                if (confirm("Are you sure saving Home page configuration?")) {
                    jsondata = jsondata.substring(0, jsondata.length - 1);
                    jsondata = '[' + jsondata + ']';
                    funGenHTML(jsondata, isPreview);
                }
            }
            else
                alert("Kindly select required elements in all sections");
        }

        $(document).on('click', '.itemclick', function () {
            $('#' + $(this).attr('placeholder')).attr('data-value', $(this).attr('data-value'));
            $('#' + $(this).attr('placeholder')).attr('itemtype', $(this).attr('itemtype'));
            $('#' + $(this).attr('placeholder')).find('span').html($(this).attr('itemtype'));
            $('#myModal').modal('hide');
        });

    </script>
</asp:Content>

