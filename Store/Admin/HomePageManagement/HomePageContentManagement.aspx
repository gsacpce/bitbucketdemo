﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Master/AdminMaster.master" AutoEventWireup="true" ValidateRequest="false"
    Inherits="Presentation.Admin_HomePageContent" %>

<%--<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- MiniColors -->

    <link href="../FineUploader/fineuploaderCategory-3.2.css" rel="stylesheet" />
    <script src="../FineUploader/jquery.fineuploaderStaticImage-3.2.js"></script>
    <script src="../JS/jquery.minicolors.min.js"></script>

    <script>  
        $(document).ready(function () {
       
            $('#btnSetDimension').click(function(){
                var width,WUnit,Height,Hunit;
                if ($('#txtWidth').val() != "")
                {
                    width = $('#txtWidth').val();
                }
                if ($('#ddlWidthUnit').val() != "0")
                {
                    WUnit = $('#ddlWidthUnit option:selected').text();
                }
                if ($('#txtHeight').val() != "")
                {
                    Height = $('#txtHeight').val();
                }
                if ($('#ddlHeightUnit').val() != "0")
                {
                    Hunit = $('#ddlHeightUnit option:selected').text();
                }
          
                var data = {'width': width , 'widthUnit': WUnit, 'height': Height, 'heightUnit': Hunit};
                //alert(JSON.stringify(data));
                $.ajax({
                    type:'POST',
                    url:'HomePageContentManagement.aspx/SetLogoDimensions',
                    contentType: 'application/json;charset=utf-8',
                    data:  JSON.stringify({'width': width , 'widthUnit': WUnit, 'height': Height, 'heightUnit': Hunit, "ActionType": $('#ddlContentType option:selected').text()}),
                    dataType: 'JSON',
                    success:function(data){
                        alert(data.d);
                    },
                    error:function(errorResult){
                        alert(errorResult.responseText)
                    }           
                });
            });
       
            ValidateHeightWidth();
       
            $('#FUSiteLogo').mouseover(function(){
                var status = "";
                if ($('#txtWidth').val() == "")
                {
                    status += "Please enter width \n";
                }
                if ($('#ddlWidthUnit').val() == "0")
                {
                    status += "Please select width unit \n";
                }

                if ($('#txtHeight').val() == "")
                {
                    status += "Please enter height \n";
                }
                if ($('#ddlHeightUnit').val() == "0")
                {
                    status += "Please select height unit \n";
                }
                if(status !="")
                {
                    alert(status);
                }
            });
       
            $('.button_section').css('border-top','none');

            $('#txtNavigateUrl').attr('placeholder','http://www.example.com');
            $('#txtVideoLink').attr('placeholder','http://www.example.com');
       
            $('#txtNavigateUrl').blur(function(){
                if($('#ddlContentType').val() == "1")
                {
                    Validate('txtNavigateUrl');
                }
            });

            $('#txtVideoLink').blur(function(){
                if($('#ddlContentType').val() == "2")
                {
                    Validate('txtVideoLink');
                }
            });
        });
        
        function ValidateHeightWidth()
        {
            $('#ddlWidthUnit').change(function () {

                var widthUnit = $('#ddlWidthUnit').val();
                var width = $('#txtWidth').val();

                if (widthUnit == "1") {
                    $('#txtWidth').attr('maxlength', '3');
                    if (width > 100) {

                        $('#txtWidthError').css('display', 'block');
                      
                        $('#btnSetDimension').hide();
                    }
                    else {
                        $('#txtWidthError').css('display', 'none');
                        if ('<%= SiteLogoImageHtml %>' != '') {
                            $('#btnSetDimension').show();
                        }
                    }
                }
                else if (widthUnit == "2") {

                    if (width > 100) {

                        $('#txtWidthError').css('display', 'none');
                        if ('<%= SiteLogoImageHtml %>' != '') {
                            $('#btnSetDimension').show();
                        }
                    }
                    $('#txtWidth').attr('maxlength', '4');
                }
                else {
                    $('#txtWidthError').css('display', 'none');
                    $('#btnSetDimension').css('display', 'block');
                }
            });

        $('#txtWidth').keydown(function () {
            var widthUnit = $('#ddlWidthUnit').val();
            var width = $('#txtWidth').val();
            if (widthUnit == "1") {
                $('#txtWidth').attr('maxlength', '3');
                if (width > 100) {

                    $('#txtWidthError').css('display', 'block');
                    $('#btnSetDimension').hide();
                }
                else {
                    $('#txtWidthError').css('display', 'none');
                    if ('<%= SiteLogoImageHtml %>' != '') {
                        $('#btnSetDimension').show();
                    }
                }
            }
            else if (widthUnit == "2") {
                if (width > 100) {
                    $('#txtWidthError').css('display', 'none');
                    if ('<%= SiteLogoImageHtml %>' != '') {
                        $('#btnSetDimension').show();
                    }
                }
                $('#txtWidth').attr('maxlength', '4');
            }
        });
    $('#txtWidth').blur(function () {

        var widthUnit = $('#ddlWidthUnit').val();
        var width = $('#txtWidth').val();
        if (widthUnit == "1") {
            $('#txtWidth').attr('maxlength', '3');
            if (width > 100) {

                $('#txtWidthError').css('display', 'block');
                $('#btnSetDimension').hide();
            }
            else {
                $('#txtWidthError').css('display', 'none');
                if ('<%= SiteLogoImageHtml %>' != '') {
                    $('#btnSetDimension').show();
                }
            }
        }
        else if (widthUnit == "2") {

            if (width > 100) {

                $('#txtWidthError').css('display', 'none');
                if ('<%= SiteLogoImageHtml %>' != '') {
                    $('#btnSetDimension').show();
                }
            }
            $('#txtWidth').attr('maxlength', '4');
        }
    });

    $('#ddlHeightUnit').change(function () {
         
        var HeightUnit = $('#ddlHeightUnit').val();          
        var Height = $('#txtHeight').val();
        if (HeightUnit == "1") {
            $('#txtHeight').attr('maxlength', '3');
            if (Height > 100) {

                $('#txtHeightError').css('display', 'block');
                $('#btnSetDimension').hide();
            }
            else {
                $('#txtHeightError').css('display', 'none');
                if ('<%= SiteLogoImageHtml %>' != '') {
                    $('#btnSetDimension').show();
                }
            }
        }
        else if (HeightUnit == "2") {

            if (Height > 100) {

                $('#txtHeightError').css('display', 'none');
                if ('<%= SiteLogoImageHtml %>' != '') {
                    $('#btnSetDimension').show();
                }
            }
            $('#txtHeight').attr('maxlength', '4');
        }
        else {
            $('#txtHeightError').css('display', 'none');
            if ('<%= SiteLogoImageHtml %>' != '') {
                $('#btnSetDimension').show();
            }
        }
    });

    $('#txtHeight').keydown(function () {
        var HeightUnit = $('#ddlHeightUnit').val();
        var Height = $('#txtHeight').val();
        if (HeightUnit == "1") {
            $('#txtHeight').attr('maxlength', '3');
            if (Height > 100) {
                $('#txtHeightError').css('display', 'block');
                $('#btnSetDimension').hide();
            }
            else {
                $('#txtHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtHeightError').css('display', 'none');
            }
            $('#txtHeight').attr('maxlength', '4');
        }
    });

    $('#txtHeight').blur(function () {

        var HeightUnit = $('#ddlHeightUnit').val();
        var Height = $('#txtHeight').val();
        if (HeightUnit == "1") {
            $('#txtHeight').attr('maxlength', '3');
            if (Height > 100) {
                $('#txtHeightError').css('display', 'block');
                $('#btnSetDimension').hide();
            }
            else {
                $('#txtHeightError').css('display', 'none');
                if ('<%= SiteLogoImageHtml %>' != '') {
                    $('#btnSetDimension').show();
                }
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtHeightError').css('display', 'none');
                $('#btnSetDimension').show();
            }
            $('#txtHeight').attr('maxlength', '4');
        }
    });
            //video 

    $('#ddlVideoWidthUnit').change(function () {
        var widthUnit = $('#ddlVideoWidthUnit').val();  
        var width = $('#txtVideoWidth').val();

        if (widthUnit == "1") {
            $('#txtVideoWidth').attr('maxlength', '3');
            if (width > 100) {
                $('#txtVideoWidthError').css('display', 'block');                       
            }
            else {
                $('#txtVideoWidthError').css('display', 'none');
            }
        }
        else if (widthUnit == "2") {
            if (width > 100) {
                $('#txtVideoWidthError').css('display', 'none');
            }
            $('#txtVideoWidth').attr('maxlength', '4');
        }
        else {
            $('#txtVideoWidthError').css('display', 'none');
        }
    });

    $('#txtVideoWidth').keydown(function () {


        var widthUnit = $('#ddlVideoWidthUnit').val();
        var width = $('#txtVideoWidth').val();
        if (widthUnit == "1") {
            $('#txtVideoWidth').attr('maxlength', '3');
            if (width > 100) {

                $('#txtVideoWidthError').css('display', 'block');
            }
            else {
                $('#txtVideoWidthError').css('display', 'none');
            }
        }
        else if (widthUnit == "2") {
            if (width > 100) {
                $('#txtVideoWidthError').css('display', 'none');
            }
            $('#txtVideoWidth').attr('maxlength', '4');
        }
    });

    $('#txtVideoWidth').blur(function () {
        var widthUnit = $('#ddlVideoWidthUnit').val();
        var width = $('#txtVideoWidth').val();
        if (widthUnit == "1") {
            $('#txtVideoWidth').attr('maxlength', '3');
            if (width > 100) {
                $('#txtVideoWidthError').css('display', 'block');
            }
            else {
                $('#txtVideoWidthError').css('display', 'none');
            }
        }
        else if (widthUnit == "2") {
            if (width > 100) {
                $('#txtVideoWidthError').css('display', 'none');
            }
            $('#txtVideoWidth').attr('maxlength', '4');

        }

    });

    $('#ddlVideoHeightUnit').change(function () {         
        var HeightUnit = $('#ddlVideoHeightUnit').val();          
        var Height = $('#txtVideoHeight').val();

        if (HeightUnit == "1") {
            $('#txtVideoHeight').attr('maxlength', '3');
            if (Height > 100) {
                $('#txtVideoHeightError').css('display', 'block');
            }
            else {
                $('#txtVideoHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtVideoHeightError').css('display', 'none');
            }
            $('#txtVideoHeight').attr('maxlength', '4');
        }
        else {
            $('#txtVideoHeightError').css('display', 'none');
        }
    });

    $('#txtVideoHeight').keydown(function () {
        var HeightUnit = $('#ddlVideoHeightUnit').val();
        var Height = $('#txtVideoHeight').val();
        if (HeightUnit == "1") {
            $('#txtVideoHeight').attr('maxlength', '3');
            if (Height > 100) {

                $('#txtVideoHeightError').css('display', 'block');
            }
            else {
                $('#txtVideoHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtVideoHeightError').css('display', 'none');
            }
            $('#txtVideoHeight').attr('maxlength', '4');
        }
    });

    $('#txtVideoHeight').blur(function () {

        var HeightUnit = $('#ddlVideoHeightUnit').val();
        var Height = $('#txtVideoHeight').val();
        if (HeightUnit == "1") {
            $('#txtVideoHeight').attr('maxlength', '3');
            if (Height > 100) {

                $('#txtVideoHeightError').css('display', 'block');
            }
            else {
                $('#txtVideoHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtVideoHeightError').css('display', 'none');
            }
            $('#txtVideoHeight').attr('maxlength', '4');
        }
    });
            //Image 
                       
    $('#ddlImageWidthUnit').change(function () {
        var widthUnit = $('#ddlImageWidthUnit').val();
        var width = $('#txtImageWidth').val();
        if (widthUnit == "1") {
            $('#txtImageWidth').attr('maxlength', '3');
            if (width > 100) {
                $('#txtImageWidthError').css('display', 'block');
            }
            else {
                $('#txtImageWidthError').css('display', 'none');
            }
        }
        else if (widthUnit == "2") {
            if (width > 100) {
                $('#txtImageWidthError').css('display', 'none');
            }
            $('#txtImageWidth').attr('maxlength', '4');
        }
        else {
            $('#txtImageWidthError').css('display', 'none');
        }
    });

    $('#txtImageWidth').keydown(function () {
        var widthUnit = $('#ddlImageWidthUnit').val();
        var width = $('#txtImageWidth').val();
        if (widthUnit == "1") {
            $('#txtImageWidth').attr('maxlength', '3');
            if (width > 100) {
                $('#txtImageWidthError').css('display', 'block');
            }
            else {
                $('#txtImageWidthError').css('display', 'none');
            }
        }
        else if (widthUnit == "2") {
            if (width > 100) {
                $('#txtImageWidthError').css('display', 'none');
            }
            $('#txtImageWidth').attr('maxlength', '4');
        }
    });

    $('#txtImageWidth').blur(function () {
        var widthUnit = $('#ddlImageWidthUnit').val();
        var width = $('#txtImageWidth').val();
        if (widthUnit == "1") {
            $('#txtImageWidth').attr('maxlength', '3');
            if (width > 100) {
                $('#txtImageWidthError').css('display', 'block');
            }
            else {
                $('#txtImageWidthError').css('display', 'none');
            }
        }
        else if (widthUnit == "2") {
            if (width > 100) {
                $('#txtImageWidthError').css('display', 'none');
            }
            $('#txtImageWidth').attr('maxlength', '4');
        }
    });

    $('#ddlImageHeightUnit').change(function () {         
        var HeightUnit = $('#ddlImageHeightUnit').val();          
        var Height = $('#txtImageHeight').val();

        if (HeightUnit == "1") {
            $('#txtImageHeight').attr('maxlength', '3');
            if (Height > 100) {
                $('#txtImageHeightError').css('display', 'block');
            }
            else {
                $('#txtImageHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtImageHeightError').css('display', 'none');
            }
            $('#txtImageHeight').attr('maxlength', '4');
        }
        else {
            $('#txtImageHeightError').css('display', 'none');
        }
    });

    $('#txtImageHeight').keydown(function () {
        var HeightUnit = $('#ddlImageHeightUnit').val();
        var Height = $('#txtImageHeight').val();
        if (HeightUnit == "1") {
            $('#txtImageHeight').attr('maxlength', '3');
            if (Height > 100) {
                $('#txtImageHeightError').css('display', 'block');
            }
            else {
                $('#txtImageHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtImageHeightError').css('display', 'none');
            }
            $('#txtImageHeight').attr('maxlength', '4');
        }
    });

    $('#txtImageHeight').blur(function () {
        var HeightUnit = $('#ddlImageHeightUnit').val();
        var Height = $('#txtImageHeight').val();
        if (HeightUnit == "1") {
            $('#txtImageHeight').attr('maxlength', '3');
            if (Height > 100) {
                $('#txtImageHeightError').css('display', 'block');
            }
            else {
                $('#txtImageHeightError').css('display', 'none');
            }
        }
        else if (HeightUnit == "2") {
            if (Height > 100) {
                $('#txtImageHeightError').css('display', 'none');
            }
            $('#txtImageHeight').attr('maxlength', '4');
        }
    });
}
///for validation of url link
function Validate(txtId) {
    return true; 
    var sMessage = '';          
    var myRegExp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
    if ($('#'+txtId).val() != '') {
        if (!myRegExp.test($('#'+txtId).val())) {
            sMessage += 'Please Enter Valid Link.\n';                       
        }
    }
    if (sMessage != "") {
        alert(sMessage);
        return false;   
    }
    else {
        return true;
    }
}
      
function ValidateOnSave()
{       
    var contentType = $('#ddlContentType').val();
    if(contentType == "1")
    {
        if($('#hdnStaticImageSrc').val() == "")
        {
            alert('Please upload image');
            //document.getElementById('btnSave').disabled = true;
            return false;                  
        }
        if($('#hdnStaticImageSrc').val() == null)
        {
            alert('Please upload image');
            return false;
            //document.getElementById('btnSave').disabled = true;
        }
        if(Validate('txtNavigateUrl'))
        {
            return true;
        }
        else
        {             
            return false;
        }
    }
    else if (contentType == "2")
    {
        if(Validate('txtVideoLink'))
        {
            return true;
        }
        else
        {
            return false;
        }               
    }           
}
    </script>
    <style>
        .minicolors-theme-default .minicolors-input {
            height: auto;
            line-height: 20px;
        }
    </style>
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Home Page</a></li>
            <li>Update Home Page Content</li>
        </ul>
    </div>
    <div class="admin_page ">
        <div class="container ">
            <div id="divMessage" visible="false" runat="server">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>
                        <asp:Literal ID="ltrMessage" runat="server" /></strong>
                    <input id="hdnWidth" type="hidden" />
                </div>
            </div>
            <div class="wrap_container ">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 relative_data">
                            <h3>Home Page Content</h3>
                            <div class="right_selct_dropdwn">
                                <div style="float: right" id="divLang" runat="server" visible="false">
                                    <label class="sm_select">Select Language <span style="color: red">*</span>:</label>
                                    <div class="select-style selectpicker">
                                        <asp:DropDownList ID="ddlLanguage" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slideshow select_data hm_pg_manag ">
                        <ul>
                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        <br />
                                        Select the Content Type. This name is only for your indicative purpose, and will not be displayed to users.
                                    </p>
                                </div>

                                <div class="col-md-3">
                                    <br />
                                    <h6>Content Type<span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <div class="select-style selectpicker wide01">
                                        <asp:DropDownList ID="ddlContentType" ClientIDMode="Static" OnSelectedIndexChanged="ddlContentType_SelectedIndexChanged" runat="server" AutoPostBack="true">
                                            <asp:ListItem Text="Static Image" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Video" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Static Text" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Site Logo" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Footer Logo" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="Footer Text" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="Copyright Info" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="Upload Document" Value="8"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </li>
                        </ul>
                        <ul id="ulTitle" runat="server">
                            <li>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        <br />
                                        Enter the title . This title is only for your indicative purpose, and will not be displayed to users.
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <h6>Title<span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <br />
                                    <asp:TextBox ID="txtTitle" runat="server" MaxLength="50" CssClass="size01"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvtxtTitle" runat="server" ControlToValidate="txtTitle" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                </div>
                            </li>
                            <li id="liAlt" runat="server">
                                <div class="col-md-3">
                                    <h6>Alt Text<span style="color: red">*</span>:</h6>
                                </div>
                                <div class="col-md-9">
                                    <asp:TextBox ID="txtAltName" runat="server" CssClass="size01"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvTxtAltName" runat="server" ControlToValidate="txtAltName" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                </div>
                            </li>
                        </ul>

                        <div id="divStaticImage" runat="server" clientidmode="static">
                            <ul>
                                <li>
                                    <div class="col-md-3">
                                        <h6>Navigate URL<span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9">

                                        <asp:TextBox ID="txtNavigateUrl" ClientIDMode="Static" runat="server" CssClass="size01"></asp:TextBox>
                                    </div>
                                </li>
                                <%--<li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the width you want for your Image?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>Width <span style="color: red">*</span>:</h6>
                                    </div>


                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtImageWidth" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01" Width="50px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalTxtImageWidth" runat="server" ControlToValidate="txtImageWidth" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>

                                        <asp:DropDownList ID="ddlImageWidthUnit" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="%"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="px"></asp:ListItem>

                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalImageWidthUnit" InitialValue="0" runat="server" ControlToValidate="ddlImageWidthUnit" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgvalImageWidth" ControlToValidate="txtImageWidth" ValidationGroup="formValid" runat="server" ErrorMessage="Only Numbers Allowed." ForeColor="#ff0000" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        <span style="color: Red; display: none;" id="txtImageWidthError" runat="server" clientidmode="static">Should not be > 100 %</span>


                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the height you want for your Image?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>Height<span style="color: red">*</span>:</h6>
                                    </div>


                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtImageHeight" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01" Width="50px"></asp:TextBox>
                                        <<asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalTxtImageHeight" runat="server" ControlToValidate="txtImageHeight" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>

                                        <asp:DropDownList ID="ddlImageHeightUnit" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="%"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="px"></asp:ListItem>

                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalTxtImageHeightUnit" InitialValue="0" runat="server" ControlToValidate="ddlImageHeightUnit" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgvImageHeightUnit" ControlToValidate="txtImageHeight" ValidationGroup="formValid" runat="server" ErrorMessage="Only Numbers Allowed." ForeColor="#ff0000" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        <span style="color: Red; display: none;" id="txtImageHeightError" runat="server" clientidmode="static">Should not be > 100 %</span>


                                    </div>
                                </li>--%>
                                <li>
                                    <div class="col-md-3">
                                        <h6>Image Upload<span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9" id="FUStaticImage">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="uploaderMsg">[Use .jpg, .jpeg , .png, .gif, .svg file]</div>
                                    </div>
                                    <input type="hidden" class="staticimagesrc" clientidmode="static" id="hdnStaticImageSrc" runat="server" />
                                </li>
                            </ul>
                        </div>
                        <div id="divVideo" runat="server" clientidmode="static">
                            <ul>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the Video Link?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        <h6>Video Link <span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9">
                                        <br />
                                        <asp:TextBox ID="txtVideoLink" ClientIDMode="Static" runat="server" CssClass="size01"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvtxtVideoLink" runat="server" ControlToValidate="txtVideoLink" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </li>
                                <%--<li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the width you want for your video?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>Width<span style="color: red">*</span>:</h6>
                                    </div>


                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtVideoWidth" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01" Width="50px"></asp:TextBox>
                                         <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalTxtVideoWidth" runat="server" ControlToValidate="txtVideoWidth" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>

                                        <asp:DropDownList ID="ddlVideoWidthUnit" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="%"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="px"></asp:ListItem>

                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalVideoWidthUnit" InitialValue="0" runat="server" ControlToValidate="ddlVideoWidthUnit" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgvalVideoWidth" ControlToValidate="txtVideoWidth" ValidationGroup="formValid" runat="server" ErrorMessage="Only Numbers Allowed." ForeColor="#ff0000" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        <span style="color: Red; display: none;" id="txtVideoWidthError" runat="server" clientidmode="static">Should not be > 100 %</span>


                                    </div>
                                </li>--%>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the height you want for your video?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>Height<span style="color: red">*</span>:</h6>
                                    </div>


                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtVideoHeight" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01" Width="50px"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvalTxtVideoHeight" runat="server" ControlToValidate="txtVideoHeight" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="divStaticText" runat="server" clientidmode="static">
                            <ul>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the Static Text Content?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        <h6>Static Text Content <span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9">
                                        <br />
                                        <asp:TextBox ID="txtStaticContents" runat="server" Width="700px" Height="1000px" TextMode="MultiLine"></asp:TextBox>

                                        <%--<CKEditor:CKEditorControl ID="txtStaticContent" BasePath="~/admin/ckeditor/" runat="server"></CKEditor:CKEditorControl>--%>
                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvStaticTextContent" runat="server" ControlToValidate="txtStaticContents" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div id="divSiteLogo" runat="server" clientidmode="static">
                            <ul>
                                <!--<li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the width you want for your logo?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>Width<span style="color: red">*</span>:</h6>
                                    </div>


                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtWidth" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01" Width="50px"></asp:TextBox>
                                        
                                        <asp:DropDownList ID="ddlWidthUnit" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="%"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="px"></asp:ListItem>

                                        </asp:DropDownList>

                                        <span style="color: Red; display: none;" id="txtWidthError" runat="server" clientidmode="static">Should not be > 100 %</span>


                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the height you want for your logo?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <h6>Height<span style="color: red">*</span>:</h6>
                                    </div>


                                    <div class="col-md-9">
                                        <asp:TextBox ID="txtHeight" ClientIDMode="Static" runat="server" MaxLength="4" CssClass="size01" Width="50px"></asp:TextBox>
                                       
                                        <asp:DropDownList ID="ddlHeightUnit" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="%"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="px"></asp:ListItem>

                                        </asp:DropDownList>
                                        <input type="button" id="btnSetDimension" value="Set Dimension" class="btn" />
                                        <span style="color: Red; display: none;" id="txtHeightError" runat="server" clientidmode="static">Should not be > 100 %</span>


                                    </div>
                                </li>-->
                                <li>
                                    <div class="col-md-3">
                                        <h6>Desktop Image Upload:</h6>
                                    </div>
                                    <div class="col-md-9" id="FUSiteLogo">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="uploaderMsg">[Use .jpg, .jpeg , .png , .gif file with  <=2.00 MB size only]</div>
                                    </div>
                                    <input type="hidden" class="sitelogosrc" clientidmode="static" id="hdnsitelogosrc" runat="server" />
                                </li>

                                <li runat="server" id="mobileimage">
                                    <div class="col-md-3">
                                        <h6>Mobile Image Upload:</h6>
                                    </div>
                                    <div class="col-md-9" id="mobileSiteLogo">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="uploaderMsg">[Use .jpg, .jpeg , .png , .gif file with  <=2.00 MB size only]</div>
                                    </div>
                                    <input type="hidden" class="sitelogosrc" clientidmode="static" id="hdnmobilelogosrc" runat="server" />
                                </li>
                            </ul>
                        </div>
                        <div id="divFooterText" runat="server" clientidmode="static">
                            <ul>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>Enter footer text</p>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        <h6>Footer Text <span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9">
                                        <br />
                                        <%--<CKEditor:CKEditorControl ID="txtFooter" ClientIDMode="Static" CssClass="size01" BasePath="~/admin/ckeditor/" runat="server"></CKEditor:CKEditorControl>--%>
                                        <asp:TextBox ID="txtFooters" runat="server" Width="700px" Height="1000px" TextMode="MultiLine"></asp:TextBox>
                                        <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvFooterTxt" runat="server" ControlToValidate="txtFooters" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div id="divCopyRightInfo" runat="server" clientidmode="static">
                            <ul>
                                <li>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            What is the Copyright Info?
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        <h6>Copyright Info <span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9">
                                        <br />
                                        <asp:TextBox ID="txtCopyRight" runat="server" Width="100%"></asp:TextBox>

                                        <%--  <asp:RequiredFieldValidator ValidationGroup="formValid" ID="rfvTxtCopyRightInfo" runat="server" ControlToValidate="txtCopyRight" ErrorMessage="*" ForeColor="#ff0000"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div id="divUploadDocument" runat="server" clientmode="static">
                            <ul>
                                <li>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <p>
                                            <br />
                                            Upload Document with .pdf extension.
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                        <br />
                                        <h6>Upload Document (.pdf) <span style="color: red">*</span>:</h6>
                                    </div>
                                    <div class="col-md-9">
                                        <asp:FileUpload ID="pdfFileUploader" runat="server" AllowMultiple="true" /><br />
                                        <asp:Button ID="btnUploadDocument" runat="server" Text="Upload PDF" CssClass="btn" OnClick="btnUploadDocument_Click" />
                                    </div>
                                    <div class="col-md-9">
                                        <div class="uploaderMsg">[Use .pdf file extension only]</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="buttonPanel container" id="divSave" runat="server">
                        <ul class="button_section">
                            <li>
                                <asp:Button ID="btnSave" ClientIDMode="Static" ValidationGroup="formValid" runat="server" CssClass="btn" Text="Save" AccessKey="S" OnClientClick="return ValidateOnSave()" OnClick="Btn_Click" CommandArgument="Save" />
                            </li>
                            <li>
                                <asp:Button ID="btnCancel" runat="server" CssClass="btn gray" Text="Cancel" AccessKey="C" OnClick="Btn_Click" OnClientClick="return confirm('are you sure want to cancel ?')" CommandArgument="Cancel" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var host ='<%=host %>';
            $(document).ready(function () {
                $('#FUStaticImage').fineUploader({
                    request: {
                        endpoint: host+'Admin/fineUploader/UploadStaticImage.aspx?type=staticimage&staticimageid='+ '<%=TempStaticImageId %>' +'&action=add'
                    },
                    multiple: false,
                    validation: {
                        //sizeLimit: <%= ImageSize %>,
                        allowedExtensions: [<%=ImageExtension%>]
                    },
                    text: {
                        uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                    },
                    retry: {
                        enableAuto: false
                    },
                    chunking: {
                        enabled: false,
                        partSize: <%= ImageSize %>
                        },
                    showMessage: function (message) {
                        // Using Bootstrap's classes
                        $('#FUStaticImage .fineError').hide();
                        $('#FUStaticImage .FailureDiv').hide();
                        $('#FUStaticImage').append('<div class="alert alert-error fineError">' + message + '</div>');
                    },
                    debug: true
                });           
            });

            jQuery(document).ready(function () {
            
                if ('<%= ImageHtml %>' != '') {
                    $('#FUStaticImage .qq-upload-button').hide();
                    $('#FUStaticImage .qq-upload-list').html('<%= ImageHtml  %>');
                    $('#FUStaticImage .uploadedimage').css('maxHeight',200);
                    $('#FUStaticImage .uploadedimage').css('maxWidth', 600);
                    ActionType = 'staticimage';                   
                }
            });

            $(document).ready(function () {
                $('#txtWidth').blur(function(){
                    if($('#txtWidth').val() == "")
                    {
                        $('#txtWidth').val('100');
                        $('#hdnWidth').val('100');
                    }                
                });
                $('#txtHeight').blur(function(){
                    if($('#txtHeight').val() == "")
                    {
                        $('#txtHeight').val('100');
                        $('#hdnHeight').val('100');
                    }                
                });
                $('#txtVideoWidth').blur(function(){
                    if($('#txtVideoWidth').val() == "")
                    {
                        $('#txtVideoWidth').val('100');
                    }
                });

                $('#txtVideoHeight').blur(function(){
                    if($('#txtVideoHeight').val() == "")
                    {
                        $('#txtVideoHeight').val('100');
                  

                    }
                
                });
                $('#txtImageWidth').blur(function(){
                    if($('#txtImageWidth').val() == "")
                    {
                        $('#txtImageWidth').val('100');
                  

                    }
                
                });

                $('#txtImageHeight').blur(function(){
                    if($('#txtImageHeight').val() == "")
                    {
                        $('#txtImageHeight').val('100');
                  

                    }
                
                });


           
                $('#ddlWidthUnit').change(function(){
                
                
                    if($('#txtWidth').val() == "")
                    {
                        $('#txtWidth').val('100');
                
                    }
                    if($('#ddlWidthUnit').val() != 0)
                    {
                        //var width =  $('#txtWidth').val();
                        var widthUnit = $('#ddlWidthUnit option:selected').text();

                    
                        $.ajax({
                       
                            type:'POST',
                            url:host+'/Admin/fineUploader/UploadStaticImage.aspx/SaveWidthNUnit',
                            contentType: 'application/json;charset=utf-8',
                            dataType:'JSON',
                            data: JSON.stringify({'Width':$('#txtWidth').val(),'WidthUnit':$('#ddlWidthUnit option:selected').text()}),
                        
                            success:function(data){
                                //alert(data.d);
                            },
                            error:function(errorResult){
                                alert(errorResult.responseText);
                            }

                        });
                    }
                });
                $('#ddlHeightUnit').change(function(){
                
                
                    if($('#txtHeight').val() == "")
                    {
                        $('#txtHeight').val('100');
                
                    }
                    if($('#ddlHeightUnit').val() != 0)
                    {
                        //var width =  $('#txtWidth').val();
                        var widthUnit = $('#ddlHeightUnit option:selected').text();

                    
                        $.ajax({
                       
                            type:'POST',
                            url:host+'/Admin/fineUploader/UploadStaticImage.aspx/SaveHeightNUnit',
                            contentType: 'application/json;charset=utf-8',
                            dataType:'JSON',
                            data: JSON.stringify({'Height':$('#txtHeight').val(),'HeightUnit':$('#ddlHeightUnit option:selected').text()}),
                        
                            success:function(data){
                                //alert(data.d);
                            },
                            error:function(errorResult){
                                alert(errorResult.responseText);
                            }

                        });
                    }
                
                
                });

                $('#FUSiteLogo').fineUploader({

                    request: {
                
                        endpoint:  host+'Admin/fineUploader/UploadStaticImage.aspx?type=<%=Logo.ToLower()%>&sitelogoimage=<%=Logo.ToLower()%>&action=add'
                    },
                    multiple: false,
                    validation: {
                        sizeLimit: <%= ImageSize %>,
                        allowedExtensions: [<%=ImageExtension%>]
                    },
                    text: {
                        uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                    },
                    retry: {
                        enableAuto: false
                    },
                    chunking: {
                        enabled: false,
                        partSize: <%= ImageSize %>
                        },
                    showMessage: function (message) {
                        // Using Bootstrap's classes
                        $('#FUSiteLogo .fineError').hide();
                        $('#FUSiteLogo .FailureDiv').hide();
                        $('#FUSiteLogo').append('<div class="alert alert-error fineError">' + message + '</div>');
                    },
                    debug: true
                });
              
                $('#mobileSiteLogo').fineUploader({

                    request: {
                
                        endpoint:  host+'Admin/fineUploader/UploadStaticImage.aspx?type=mobilelogo&mobilelogoimage=<%=MobileLogo.ToLower()%>&action=add'
                    },
                    multiple: false,
                    validation: {
                        sizeLimit: <%= ImageSize %>,
                        allowedExtensions: [<%=ImageExtension%>]
                    },
                    text: {
                        uploadButton: 'Click here to upload image<br>or<br>Drag and drop file here to upload'
                    },
                    retry: {
                        enableAuto: false
                    },
                    chunking: {
                        enabled: false, 
                        partSize: <%= ImageSize %>
                        },
                    showMessage: function (message) {
                        // Using Bootstrap's classes
                        $('#mobileSiteLogo .fineError').hide();
                        $('#mobileSiteLogo .FailureDiv').hide();
                        $('#mobileSiteLogo').append('<div class="alert alert-error fineError">' + message + '</div>');
                    },
                    debug: true
                });

                
          
                jQuery(document).ready(function () {
                   
                    if ('<%= SiteLogoImageHtml %>' != '') {
                       
                        
                        $('#FUSiteLogo .qq-upload-button').hide();
                        $('#FUSiteLogo .qq-upload-list').html('<%= SiteLogoImageHtml  %>');
                        $('#FUSiteLogo .uploadedimage').css('maxHeight',200);
                        $('#FUSiteLogo .uploadedimage').css('maxWidth', 600);
                        $('#btnSetDimension').show();                      
                        if($('#ddlContentType').val() == '5')
                        {
                            ActionType = 'footerlogo';
                        }
                        else
                        {
                            ActionType = 'sitelogo';
                        }
                                          
                     
                        
                    }
                    else
                    {
                       
                        $('#btnSetDimension').hide();
                    
                    }

                    if ('<%= mobileLogoImageHtml %>' != '') {
                        
                        $('#mobileSiteLogo .qq-upload-button').hide();
                        $('#mobileSiteLogo .qq-upload-list').html('<%= mobileLogoImageHtml  %>');
                        $('#mobileSiteLogo .uploadedimage').css('maxHeight',200);
                        $('#mobileSiteLogo .uploadedimage').css('maxWidth', 600);
                        $('#btnSetDimension').show();
                        
                        ActionType = 'mobilelogo';                                         
                    }
                    else
                    {                        
                        $('#btnSetDimension').hide();                    
                    }
                });
            });
        </script>
    </div>
</asp:Content>
