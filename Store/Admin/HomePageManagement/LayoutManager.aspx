﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Presentation.Admin_HomePageManagement_LayoutManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="utf-8">
    <title>Home page - Layout manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="dist/css/jquery.gridmanager.css" rel="stylesheet">
    <link href="css/demo.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="dist/js/jquery.gridmanager.js"></script>

    <style>
        .gridMainContainer {
            width: 1250px;
            margin: 0 auto;
        }

        .gm-edit-mode, .gm-preview, .gm-save, .gm-resetgrid, #gm-canvas .gm-editable-region .gm-controls-element {
            display: none !important;
        }
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Home Page</a></li>
            <li>Manage Home Page Layout</li>
        </ul>
    </div>
    <div class="container">
        <h3>Manage Home Page Layout - Step 1</h3>
    </div>
    <div class="admin_page">
        <section class="container gridMainContainer padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <div class="myContainer" clientidmode="Static" runat="server">
                        <div id="myCanvas" clientidmode="Static" runat="server">

                            <iframe src="GMDemo.html" width="100%" height="400"></iframe>


                        </div>

                    </div>
                    <div class="container">

                        <div class="button_section">

                            <asp:Button ID="btnSaveLayout" CssClass="btn" runat="server" Text="Save Page layout >> " OnClientClick="javascript: funGenHTML();return false; " />

                            <%--<asp:Button ID="btnConfigureLayout" runat="server" CssClass="btn" Text="Configure Page Contents" PostBackUrl="LayoutConfigure.aspx" />--%>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>

    <!--================== JS ================-->
    <script>
        $(document).ready(function () {

            $("#myCanvas").gridmanager({
                debug: 1
            }
             );


        });
    </script>
    <script type="text/javascript">
        /*function funGenHTML()
        {
            alert( "<html>" + $("body").html() + "</html>" );
        }*/
        function funContainerMode() {
            //debugger;
            var checked = $("#chkContainer").prop('checked');
            if (checked == true) {
                $("#dvMainContent").attr('class', 'container');
            }
            else {
                $("#dvMainContent").attr('class', '');
            }

        }

        function funGenHTML() {
            //debugger
            //console.log($("#myCanvas").html());
            //alert("<html>" + $("#myCanvas").html() + "</html>");

            // alert(gm);
            var gm = jQuery("#myCanvas").gridmanager().data('gridmanager');
            gm.cleanup();
            gm.deinitCanvas();

            //var _data = JSON.stringify({ 'PageLayout': $("#myCanvas").html() });
            var _data = JSON.stringify({ 'PageLayout': $("#gm-canvas").html() });
            //alert(_data);

            $.ajax({
                type: "POST",
                url: "LayoutManager.aspx/SavePageLayout",
                data: _data,
                // data: '{PageLayout: "' + _data + '"}', //
                contentType: "application/json; charset=utf-8",
                dataType: "text",
                success: OnSuccess,
                failure: function (response) {
                    alert("There is an error saving page layout.");
                    //$('#myErrorModal').find('#SuccessMessage').html("There is an error saving page layout.");
                    //$('#myErrorModal').modal('show');
                }
            });
            //return false;
            //window.location.href = 'LayoutConfigure.aspx';
        }
        function OnSuccess(response) {
            //debugger;
            var responseData = response.d;
            alert("The page layout saved successfully!");
            //$('#mySuccessModal').find('#SuccessMessage').html("There is an error saving page layout.");
            //$('#mySuccessModal').modal('show');
            window.location.href = "LayoutConfigure.aspx";
            //if (responseData[0] != "")
            //    alert(responseData[0]);
            //$("#lblfinaltotal").html(responseData[1]);
            //$("#lblGrossTotalbootstrap").html(responseData[1]);
        }
    </script>
</asp:Content>
