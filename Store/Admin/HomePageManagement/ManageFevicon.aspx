﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin/Master/AdminMaster.master" Inherits="Admin_HomePageManagement_ManageFevicon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="../Dashboard/Dashboard.aspx">Home</a></li>
            <li><a href="#">Manage Favicon</a></li>
            <li>Manage Favicon</li>
        </ul>
    </div>
    <div class="admin_page">
        <section class="container mainContaJiner padbtom15">
            <div class="wrap_container ">
                <div class="content">

                    <h3 class="mainHead">Manage Favicon</h3>
                    <span class="sectionText">Manage Favicon for Home Page.</span>
                </div>
                <div class="butonblock nospace clearfix ">
                    <ul class="categories_btn">
                        <li>
                            <div>
                                <asp:FileUpload ID="fluFevicon" runat="server" />
                            </div>
                        </li>
                        <li>
                            <div>
                                <asp:Button runat="server" ID="btnupload" CssClass="btn3" Text="Upload Fevicon" OnClick="btnupload_Click" />
                            </div>
                        </li>
                    </ul>
                    <div class="clear">
                    </div>
                </div>
                <div>
                    <asp:Image ID="imgFevicon" runat="server" />
                </div>
            </div>
        </section>


    </div>
</asp:Content>

