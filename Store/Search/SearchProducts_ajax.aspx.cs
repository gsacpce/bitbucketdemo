﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application;
using PWGlobalEcomm.BusinessLogic;

public partial class Search_SearchProducts_ajax : System.Web.UI.Page
{
    public string host = GlobalFunctions.GetVirtualPath();
    protected void Page_Load(object sender, EventArgs e)
    {
        string jsondata = string.Empty;
        try
        {
            if (!System.IO.Directory.Exists(ProductSearchBL._luceneDir)) System.IO.Directory.CreateDirectory(ProductSearchBL._luceneDir);

            // perform Lucene search
            IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
            searchResults = ProductSearchBL.Search(Sanitizer.GetSafeHtmlFragment(Request.QueryString["q"].Trim()));

            if (searchResults != null && searchResults.Count() > 0)
            {
                //searchResults = searchResults.Where(x => x.LanguageId == GlobalFunctions.GetLanguageId() && x.CurrencyId == GlobalFunctions.GetCurrencyId());

                if (searchResults.Count() > 0)
                {
                    searchResults = searchResults.Take(10);
                    //foreach (var item in searchResults)
                    //{
                    //    jsondata += "'" + item.ProductName.Replace("'", "''") + "',";
                    //}
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(searchResults);
                    //jsondata = "[" + jsondata.Substring(0, jsondata.Length - 1) + "]";
                }
            }

            Response.Clear();
            Response.Write(jsondata);
            Response.End();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!System.IO.Directory.Exists(ProductSearchBL._luceneDir)) System.IO.Directory.CreateDirectory(ProductSearchBL._luceneDir);

            // perform Lucene search
            IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();

            searchResults = ProductSearchBL.Search(Sanitizer.GetSafeHtmlFragment(txtKeyword.Text.Trim()));

            //if (searchResults != null && searchResults.Count() > 0)
            //{
            //    searchResults = searchResults.Where(x => x.LanguageId == GlobalFunctions.GetLanguageId() && x.CurrencyId == GlobalFunctions.GetCurrencyId());
            //}

            lvSearchIndex.DataSource = searchResults;
            lvSearchIndex.DataBind();
            txtKeyword.Focus();
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {

        List<object> objProductSearchBE = ProductBL.GetAllProductsForLucene();

        List<ProductSearchBE> lstProductSearchBE = new List<ProductSearchBE>();


        lstProductSearchBE = objProductSearchBE.Cast<ProductSearchBE>().ToList();

        if (lstProductSearchBE != null && lstProductSearchBE.Count > 0)
        {
            ProductSearchBL.ClearLuceneIndex();

            ProductSearchBL.AddUpdateLuceneIndex(lstProductSearchBE);

            IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();

            searchResults = ProductSearchBL.GetAllIndexRecords();

            litResult.Text = "Search index was created successfully! Total: " + lstProductSearchBE.Count + " Saved: " + searchResults.Count();
        }
        else
        {
            litResult.Text = "There is some error. Please try after sometime!";
        }
    }

    [System.Web.Services.WebMethod]
    public static string GetProductData(string Keyword)
    {
        string jsondata = string.Empty;
        try
        {
            if (!System.IO.Directory.Exists(ProductSearchBL._luceneDir)) System.IO.Directory.CreateDirectory(ProductSearchBL._luceneDir);

            // perform Lucene search
            IEnumerable<ProductSearchBE> searchResults = new List<ProductSearchBE>();
            searchResults = ProductSearchBL.Search(Sanitizer.GetSafeHtmlFragment(Keyword.Trim()));

            if (searchResults != null && searchResults.Count() > 0)
            {
                //searchResults = searchResults.Where(x => x.LanguageId == GlobalFunctions.GetLanguageId() && x.CurrencyId == GlobalFunctions.GetCurrencyId());

                if (searchResults.Count() > 0)
                {
                    searchResults = searchResults.Take(10);
                    //foreach (var item in searchResults)
                    //{
                    //    jsondata += "'" + item.ProductName.Replace("'", "''") + "',";
                    //}
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    jsondata = jSearializer.Serialize(searchResults);
                    //jsondata = "[" + jsondata.Substring(0, jsondata.Length - 1) + "]";
                }
            }
        }
        catch (Exception ex)
        {
            Exceptions.WriteExceptionLog(ex);
        }
        return jsondata;
    }
}

