﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/MasterPage.master" CodeFile="SearchProducts_ajax.aspx.cs" Inherits="Search_SearchProducts_ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= host %>js/jquery-ui-1.10.2.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <div class="container" style="min-height: 500px; padding-top: 20px;">
        <div class="searchBox col_12">
            <div class="content_right margin_top8">
                <asp:Button ID="btnCreate" Text="Create Index" OnClick="btnCreate_Click" runat="server" />
            </div>
            <div class="content_right margin_top8" style="display: none;">
                Total Results:<input type="text" name="name" value="0" id="lblCount" />
            </div>
            <div class="content_left margin_right20">
                <asp:TextBox ID="txtKeyword" Width="650px" ClientIDMode="Static" autocomplete="off" CssClass="big" runat="server" />
            </div>

            <div class="content_right margin_top8">
                <asp:Button ID="btnSearch" Text="Search" OnClick="btnSearch_Click" runat="server" />
            </div>
            <asp:Literal ID="litResult" runat="server" />
            <div class="clear"></div>
        </div>

        <asp:ListView ID="lvSearchIndex" DataKeyNames="ProductId" runat="server">
            <LayoutTemplate>
                <table class="grid">
                    <tr>
                        <th>ProductCode</th>
                        <th>ProductName</th>
                    </tr>
                    <tr id="itemPlaceHolder" runat="server"></tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ProductCode") %></td>
                    <td><%# Eval("ProductName") %></td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <br />
                No search index records found...<br />
            </EmptyDataTemplate>
        </asp:ListView>
    </div>
    <script type="text/javascript">
        //$(document).on('keyup', '#txtKeyword', function () {
     <%--    $(document).ready(function () {
           $("#txtKeyword").on('input', function () {
                var keyword = $("#txtKeyword").val();
                if (keyword.length > 1) {
                    $.ajax({
                        url: '<%= host %>search/SearchProducts_ajax.aspx/GetProductData',
                        data: "{'Keyword': '" + keyword + "' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        cache: false,
                        async: false
                    }).done(function (html) {
                        var json = $.parseJSON(html.d);
                        var jsondata = '';
                        if (json != "") {
                            var availableTags = [];
                            for (var i = 0; i < json.length; i++) {
                                availableTags[i] = json[i].ProductCode + ' : ' + json[i].ProductName;
                                jsondata = json[i].ProductCode + ' : ' + json[i].ProductName + ',';
                            }
                            $('#lblCount').val(json.length + jsondata);
                            $("#txtKeyword").autocomplete({
                                source: availableTags
                            });
                        }
                    });
                }
            });

            
        });--%>

        $(function () {

            //attach autocomplete
            $("#txtKeyword").autocomplete({
                //define callback to format results
                minLength: 3,
                delay: 500,
                source: function (req, add) {
                    var keyword = $("#txtKeyword").val();
                    //pass request to server
                    //$.getJSON('<%= host %>search/SearchProducts_ajax.aspx/GetProductData?Keyword=' + keyword, req, function (data) {
                    var myVars = { 'Keyword': keyword };
                    $.ajax({
                        url: '<%= host %>search/SearchProducts_ajax.aspx/GetProductData',
                        data: "{'Keyword': '" + keyword + "' }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        cache: false,
                        async: false
                    }).done(function (data) {
                        //create array for response objects
                        var suggestions = [];
                        if (data.d == "")
                        { add(suggestions); return false; }
                        var json = $.parseJSON(data.d);
                        //process response

                        $.each(json, function (i, val) {
                            suggestions.push(val.ProductName);
                        });

                        //pass array to callback
                        add(suggestions);
                        //var json = $.parseJSON(html.d);
                        //var jsondata = '';
                        //if (json != "") {
                        //    var availableTags = [];
                        //    for (var i = 0; i < json.length; i++) {
                        //        availableTags[i] = json[i].ProductCode + ' : ' + json[i].ProductName;
                        //        jsondata = json[i].ProductCode + ' : ' + json[i].ProductName + ',';
                        //    }
                        //    $('#lblCount').val(json.length + jsondata);
                        //    $("#txtKeyword").autocomplete({
                        //        source: availableTags
                        //    });
                        //}
                    });
                },

                //define select handler
                select: function (e, ui) {

                    //create formatted friend
                    var friend = ui.item.value,
                        span = $("<span>").text(friend),
                        a = $("<a>").addClass("remove").attr({
                            href: "javascript:",
                            title: "Remove " + friend
                        }).text("x").appendTo(span);

                    //add friend to friend div
                    span.insertBefore("#to");
                },

                ////define select handler
                //change: function () {

                //    //prevent 'to' field being updated and correct position
                //    $("#txtKeyword").val("").css("top", 2);
                //}
            });
        });
    </script>

</asp:Content>
