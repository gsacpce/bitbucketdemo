﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" CodeFile="twitterTypeAhead.aspx.cs" Inherits="Search_twitterTypeAhead" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    

         
        <div class="Typeahead Typeahead--twitterUsers">
            <div class="u-posRelative">
                <input class="form-control typeahead tt-query tt-input" id="demo-input" type="text" name="q" placeholder="Search Twitter users...">
                <%--<img class="Typeahead-spinner" src="http://twitter.github.io/typeahead.js/img/spinner.gif">--%>
            </div>
            <%--<div class="Typeahead-menu"></div>--%>
        </div>


   
    <script src="../js/typeahead.bundle.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var productsearcher = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace('ProductName');
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                replace: function (url, uriEncodedQuery) {
                    return url + "#" + uriEncodedQuery;
                    // the part after the hash is not sent to the server
                },
                remote: {
                    url: "../Products/ProductListing.aspx?q=%QUERY",
                    wildcard: '%QUERY'
                }
            });

            // initialize the bloodhound suggestion engine
            productsearcher.initialize();

            // instantiate the typeahead UI
            $('#demo-input').typeahead(null, {
                displayKey: 'ProductName',
                source: productsearcher.ttAdapter()
            });
        });


    /*    $(document).ready(function () {
            var productsearcher = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace('ProductName');
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                replace: function (url, uriEncodedQuery) {
                    return url + "#" + uriEncodedQuery;
                    // the part after the hash is not sent to the server
                },
                remote: {
                    url: "../Products/ProductListing.aspx?q=%QUERY",
                    wildcard: '%QUERY'
                }
            });

            // initialize the bloodhound suggestion engine
            productsearcher.initialize();

            $('#txtKeyword').typeahead({
                hint: true,
                highlight: true,
                minLength: 3
            },
            {
                //name: 'ProductName',
                limit: 10,
                displayKey: 'ProductName',
                source: productsearcher.ttAdapter() //substringMatcher()
            });
        });*/
    </script>
</asp:Content>

