﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Products_Products_ajax" %>


<div id="productdata">
    <asp:repeater id="rptProducts" runat="server" onitemdatabound="rptProducts_ItemDataBound">
        <ItemTemplate>
            
               <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="liProducts" runat="server">               
                <div class="prod_list_cell">
                    <div class="prod_list_image_outer">
                        <a id="aProductImage" class="indeedProductImage" runat="server" href="javascript:void(0);" style="position:relative">
                            <img class="prod_list_image_inner" id="imgProduct" runat="server" />
                             <%-- <div class="indeedProduct">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                                <label class="indeedQuickView">Quick View</label>
                            </div>--%>
                        </a>
                        <div id="dvSectionIcons" class="product_list_icon_row" runat="server"></div>
                    </div>
                     <div class="prod_list_text_outer">
                    <div class="prod_list_text_title productSmlName" id="aProductName" runat="server">
                    </div>
                    <div class="prod_list_text_code productCode"><asp:Literal id="ltrProductCode"  runat="server"></asp:Literal></div>
                    <div class="prod_list_text_price productPrice">
                         <div class="price_was" id="spnPrice1"  runat="server"></div>
                         <div class="prod_list_text_price productPrice" id="spnor"  runat="server"></div> 
                        <div class="price_was" id="spnPrice"  runat="server"></div>    
                        <div class="price_now" id="spnstrike"  runat="server"></div>
                    </div>
                    <div id="spnstock" class="prod_list_text_price productPrice" runat="server" ></div> 
                    <div class="star product_rating_stars" id="rating" runat="server" >
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating1" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating2" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating3" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating4" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating5" runat="server"></span>&nbsp;
                    </div>
                    <div class="product_rating_stars star" id="ratingtext" runat="server">  </div>           
                        <div class="prod_list_text_desc productDescription" id="dvDescription" runat="server"></div>
                        <div class="compareContainer ">
                            <label id="lblProductComparison" class="prod_list_checkbox" runat="server" visible="false">
                                <span class="comparecheckboxgrid">
                                    <input id="chkCompare"  runat="server" ClientIdMode="static" type="checkbox">
                                </span>
                                <span id="spnCompareText" runat="server"></span>
                            </label>
                            <%-- <span><a id="aProductDetail" class="customButton" runat="server" href="javascript:void(0);"></a>
                            </span>--%>    
                        </div>
                        <div class="prod_list_button"><a href="javascript:void(0);" id="aQuickView" ClientIDmode="static" runat="server" class="btn btn-primary customActionBtn"></a></div>
                    </div>
                </div>    
               
               </li>
            
        </ItemTemplate>
    </asp:repeater>

    <input type="hidden" class="totalrecords" id="hidTotalRecords" runat="server" />
</div>

<%--<script type="text/javascript">
    //Added by Sripal to maintain the view of the productListing on Postback(lazy loading / Postback)
    function ShowProductListingPageView() {
        debugger;
        if ($('#hdcurview').val() == "grid") {
            $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
                $('#dvProductData').removeClass('list');
                $('#dvProductData').addClass('grid');
                $('#dvProductData li').addClass('col-lg-4 col-md-4 col-sm-6');
                $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
                $('#alstView').removeClass('hidden');
                $('#agrdView').addClass('hidden');
                debugger;
                $('#hdcurview').val("grid");
                alert($('#hdcurview').val());
               
            });
        }
        else {
            $('#dvProductData').animate({ opacity: 0 }, 1000, function () {
                $('#dvProductData').removeClass('grid');
                $('#dvProductData').addClass('list');
                $('#dvProductData li').removeClass('col-lg-4 col-md-4 col-sm-6');
                $('#dvProductData').stop().animate({ opacity: 1 }, 1000);
                $('#agrdView').removeClass('hidden');
                $('#alstView').addClass('hidden');
                debugger;
                $('#hdcurview').val("list");
                alert($('#hdcurview').val());
        });
        }

    }
</script>--%>
<%--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="prod_list_cell">
        <div class="prod_list_image_outer">
            <a href="product.html">
                <img class="prod_list_image_inner" src="../Images/UI/Category/category1.png"></a>
            <div class="product_list_icon_row">
                <img class="product_list_icon" src="../Images/ProductIcons/product_icon1.png">
                <img class="product_list_icon" src="../Images/ProductIcons/product_icon2.png">
                <img class="product_list_icon" src="../Images/ProductIcons/product_icon3.png">
                <img class="product_list_icon" src="../Images/ProductIcons/product_icon4.png">
                <img class="product_list_icon" src="../Images/ProductIcons/product_icon5.png">
            </div>
        </div>
        <div class="prod_list_text_outer">
            <div class="prod_list_text_title productSmlName">Product title with 60 characters xxxxx xxxxx xxxxx xxxxx xx</div>
            <div class="prod_list_text_code productCode">QL001</div>
            <div class="prod_list_text_price productPrice">&pound;00.00</div>
            <div class="prod_list_text_desc productDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu quam quis tortor mollis suscipit. Nam ut lorem justo, at eleifend nulla. Fusce porta, mauris id pellentesque mollis, augue sapien mattis quam, sit amet ullamcorper mi orci quis risus.</div>
            <div class="prod_list_button"><a class="btn btn-primary customActionBtn" data-toggle="modal" href="#ModalQuickview">Quickview</a></div>
        </div>
    </div>
</div>--%>
