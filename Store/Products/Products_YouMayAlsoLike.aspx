﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Products_Products_YouMayAlsoLike" %>

<div class="row  customTabs" id="dvYouMayAlsoLike">
    <div class="col-xs-12">

        <ul class="nav nav-tabs" role="tablist">
            <!-- Nav tabs -->
            <li role="presentation" visible="false" id="liRelatedProduct" runat="server">
                <a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">
                    <asp:literal id="ltrRelatedProduct" text="Similar" runat="server"></asp:literal>
                </a>
            </li>
            <li role="presentation" visible="false" id="liRelatedPrice" runat="server">
                <a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab">
                    <asp:literal id="ltrRelatedPrice" text="Similar price" runat="server"></asp:literal>
                </a>
            </li>
            <li role="presentation" visible="false" id="liRelatedCategory" runat="server">
                <a href="#tab3" aria-controls="messages" role="tab" data-toggle="tab">
                    <asp:literal id="ltrRelatedCategory" text="Other ideas" runat="server"></asp:literal>
                </a>
            </li>
        </ul>

        <div class="tab-content tabBg">
            <!-- Tab panes -->
            <div role="tabpanel" class="tab-pane fade in active" id="tab1" runat="server" clientidmode="static">
                <div id="products" class="row grid">
                    <!-- PRODUCTS START -->
                    <asp:repeater id="rptProducts1" runat="server" onitemdatabound="rptProducts1_ItemDataBound">
                            <ItemTemplate>
                                <a id="lnkProductImage" runat="server">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="dvProducts" runat="server">
                                        <div class="prod_list_cell">
                                            <div class="prod_list_image_outer">
                                                <img class="prod_list_image_inner" id="imgProduct" runat="server">
                                                <div id="dvSectionIcons" class="product_list_icon_row" runat="server">
                                                </div>
                                            </div>
                                            <div class="prod_list_text_outer">
                                                <div class="prod_list_text_title productSmlName" id="aProductName" runat="server">
                                                </div>
                                                <div class="prod_list_text_code productCode">
                                                    <asp:Literal ID="ltrProductCode" Text='<%# Eval("ProductCode") %>' runat="server"></asp:Literal>
                                                </div>
                                                <%--<div id="spnPrice" class="prod_list_text_price productPrice" runat="server"></div>--%>
                                                 <div class="row prod_list_text_price productPrice">
                                                    <div class="price_was" id="spnPrice"  runat="server"></div>
                                                      <div class="price_was" id="spnor"  runat="server"></div> 
                                                        <div class="price_was" id="spnPrice1"  runat="server"></div>         
                                                    <div class="price_now" id="spnstrike"  runat="server"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </ItemTemplate>
                        </asp:repeater>
                </div>
                <!-- PRODUCTS END -->
            </div>

            <div role="tabpanel" class="tab-pane fade" id="tab2" runat="server" clientidmode="static">
                <div id="products" class="row grid">
                    <!-- PRODUCTS START -->
                    <asp:repeater id="rptProducts2" runat="server" onitemdatabound="rptProducts2_ItemDataBound">
                            <ItemTemplate>
                                <a id="lnkProductImage" runat="server">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="dvProducts" runat="server">
                                        <div class="prod_list_cell">
                                            <div class="prod_list_image_outer">
                                                <img class="prod_list_image_inner" id="imgProduct" runat="server">
                                                <div id="dvSectionIcons" class="product_list_icon_row" runat="server"></div>
                                            </div>
                                            <div class="prod_list_text_outer">
                                                <div class="prod_list_text_title productSmlName" id="aProductName" runat="server">
                                                </div>
                                                <div class="prod_list_text_code productCode">
                                                    <asp:Literal ID="ltrProductCode" Text='<%# Eval("ProductCode") %>' runat="server"></asp:Literal>
                                                </div>
                                               <%-- <div id="spnPrice" class="prod_list_text_price productPrice" runat="server"></div>--%>
                                                 <div class="row prod_list_text_price productPrice">
                                                    <div class="price_was" id="spnPrice"  runat="server"></div>
                                                      <div class="price_was" id="spnor"  runat="server"></div> 
                                                        <div class="price_was" id="spnPrice1"  runat="server"></div>        
                                                    <div class="price_now" id="spnstrike"  runat="server"></div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </ItemTemplate>
                        </asp:repeater>
                </div>
                <!-- PRODUCTS END -->
            </div>

            <div role="tabpanel" class="tab-pane fade " id="tab3" runat="server" clientidmode="static">
                <div id="products" class="row grid">
                    <!-- PRODUCTS START -->
                    <asp:repeater id="rptProducts3" runat="server" onitemdatabound="rptProducts_ItemDataBound">
                            <ItemTemplate>
                                <a id="lnkProductImage" runat="server">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id="dvProducts" runat="server">
                                        <div class="prod_list_cell">
                                            <div class="prod_list_image_outer">
                                                <img class="prod_list_image_inner" id="imgProduct" runat="server">
                                                <div id="dvSectionIcons" class="product_list_icon_row" runat="server"></div>
                                            </div>

                                            <div class="prod_list_text_outer">
                                                <div class="prod_list_text_title productSmlName" id="aProductName" runat="server">
                                                </div>
                                                <div class="prod_list_text_code productCode">
                                                    <asp:Literal ID="ltrProductCode" Text='<%# Eval("ProductCode") %>' runat="server"></asp:Literal>
                                                </div>
                                                  <div class="row prod_list_text_price productPrice">
                                                    <div class="price_was" id="spnPrice"  runat="server"></div>
                                                        <div class="price_was" id="spnor"  runat="server"></div> 
                                                        <div class="price_was" id="spnPrice1"  runat="server"></div>      
                                                    <div class="price_now" id="spnstrike"  runat="server"></div>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </ItemTemplate>
                        </asp:repeater>
                </div>
                <!-- PRODUCTS END -->
            </div>
        </div>
    </div>
</div>

