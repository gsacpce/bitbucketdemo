﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Products_Products_CustomRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- PAGE CONTENT ------------------------------------------------------------------------------------------------------------------------------------------ -->
<style>
    #datetimepicker1 {padding:0 15px !important}
    .input-group-addon {height: 34px;
    line-height: 1;
    padding: 8px 20px;
    position: absolute;
    right: 14px;
    text-align: center;
    top: 0;
    z-index: 999;}
    .input-group-addon .glyphicon-calendar{right: 5px;}
    .datepickerContainer {margin-top:20px;}

</style>
<section id="CUSTOM_REQUEST_PAGE">
    <div class="container">
    
        <div class="row">
            <div class="col-xs-12">
                <h1  class="pageTitle"><asp:Literal ID="ltrTitle" runat="server"></asp:Literal></h1>
            </div>
        </div>
    
        <div class="row">  
            <div class="col-xs-12">
            <p class="pageText"><asp:Literal ID="ltrDetailPara1" runat="server"></asp:Literal></p>
         	<p class="pageText"><asp:Literal ID="ltrDetailPara2" runat="server"></asp:Literal></p>
        	<p class="pageText"><asp:Literal ID="ltrDetailPara3" runat="server"></asp:Literal></p>         
         	<p class="pageText"><asp:Literal ID="ltrDetailPara4" runat="server"></asp:Literal></p>
            </div><!-- COLUMN END -->
        </div><!-- ROW END -->
        
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                               <asp:Label ID="lblContactName" runat="server" Text="Contact name" CssClass="col-sm-3 customLabel control-label"> </asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvContactName" CssClass="pageSmlText red" runat="server" ControlToValidate="txtContactName" Display="Static" 
                                      ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                            <div class="form-group">
                                <asp:Label ID="lblEmail" runat="server" Text="*Email" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtEmail" runat="server" type="email" CssClass="form-control customInput" autocomplete="off"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvEmail" CssClass="pageSmlText red" runat="server" ControlToValidate="txtEmail" 
                                       Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                                 <%-- <asp:RegularExpressionValidator ID="rxvEmail" CssClass="pageSmlText red" runat="server" ControlToValidate="txtEmail" Text="Enter Valid Email Address"
                                Display="Static" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ValidateCustomRequest">
                            </asp:RegularExpressionValidator>--%>
                              </div>
                          </div>
                            <div class="form-group">
                                <asp:Label ID="lblPhoneNumber" runat="server" Text="Phone number" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvPhoneNumber" CssClass="pageSmlText red" runat="server" ControlToValidate="txtPhoneNumber" Display="Static"
                                      ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                          <div class="form-group">
                              <asp:Label ID="lblEvent" runat="server" Text="Event" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtEvent" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvEvent"  CssClass="pageSmlText red" runat="server" ControlToValidate="txtEvent" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                            <div class="form-group">
                                <asp:Label ID="lblTargetAud" runat="server" CssClass="col-sm-3 control-label customLabel">Target audience(who are you giving it to)</asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtTargetAud" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvTargetAud" CssClass="pageSmlText red" runat="server" ControlToValidate="txtTargetAud" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                            <div class="form-group">
                                <asp:Label ID="lblIdeas" runat="server" Text="Ideas you have already thought about" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtIdeas" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvIdeas" CssClass="pageSmlText red" runat="server" ControlToValidate="txtIdeas" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                          <div class="form-group">
                              <asp:Label ID="lblQty" runat="server" Text="Quantity" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtQty" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvQty" CssClass="pageSmlText red" runat="server" ControlToValidate="txtQty" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                          <div class="form-group">
                              <asp:Label ID="lblBudget" runat="server" Text="Budget" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtBudget" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator id="rfvBudget" CssClass="pageSmlText red" runat="server" ControlToValidate="txtBudget" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                          <div class="form-group">
                              <asp:Label ID="lblColour" runat="server" Text="Colour" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtColour" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvColour" CssClass="pageSmlText red" runat="server" ControlToValidate="txtColour" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                          <div class="form-group">
                              <asp:Label ID="lblBranding" runat="server" Text="Branding" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                  <asp:TextBox ID="txtBranding" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <asp:RequiredFieldValidator id="rfvBranding" CssClass="pageSmlText red" runat="server" ControlToValidate="txtBranding" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                              </div>
                          </div>
                          <div class="form-group">
                              <asp:Label ID="lblDelAdd" runat="server" Text="Delivery address" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                 <textarea class="form-control customInput" rows="5" runat="server" id="txtareaDelAdd"></textarea>
                              </div>
                          </div>
                            <%--<div class="form-group">
                                <asp:Label ID="lblDelDate"  runat="server" Text="Delivery date" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9 date" id="datetimepicker1">
                                  <asp:TextBox ID="txtDelDate" ClientIDMode="Static" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                              </div>
                          </div>--%>
                            <div class="form-group datepickerContainer">
                                        <asp:Label ID="lblDelDate"  runat="server" Text="Delivery date" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                                        <div class='col-sm-9 input-group date' id='datetimepicker1'>
                                            <asp:TextBox ID="txtDelDate" ClientIDMode="Static" runat="server" CssClass="form-control customInput"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDelDate" CssClass="pageSmlText red" runat="server" ControlToValidate="txtDelDate" Display="Static" ValidationGroup="ValidateCustomRequest"></asp:RequiredFieldValidator>
                                            <span class="input-group-addon customInput">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                        <%--<script type="text/javascript">$(function () { $('#datetimepicker1').datetimepicker({ format: 'LL' }); $('#datepicker').datepicker({ startDate: '+1d' }) });</script>--%>
                                    </div>
                              <%--<div class="form-group">

                                  <div class="col-sm-12 col-md-offset-3">
                                      
                                  </div>
                                  </div>--%>

                          <div class="form-group">
                              <asp:Label ID="lblAddCommnets" runat="server" Text="Additional comments" CssClass="col-sm-3 control-label customLabel"></asp:Label>
                              <div class="col-sm-9">
                                 <textarea class="form-control customInput" rows="10" runat="server" id="txtareaAddCommnets"></textarea>
                              </div>
                          </div>
                            <div class="form-group">
                              <div class="col-sm-3 hidden-xs">&nbsp;</div>
                              <div class="col-sm-9">
                                  <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary customActionBtn" Text="Submit" ValidationGroup="ValidateCustomRequest"   OnClick="btnSubmit_Click" />
                                  <asp:CustomValidator ID="CustomValidator1" runat="server" Text="(Required)" ErrorMessage="Please enter valid email address"
                                            ClientValidationFunction="validateEmailId" Display="Dynamic" ForeColor="Red" ValidationGroup="ValidateCustomRequest"></asp:CustomValidator>
                              </div>
                          </div>
                        </div>
                  </div>
                </div>
                
            </div><!-- COLUMN END -->
        
        
  	</div><!-- ROW END -->  
        
    </div><!-- CONTAINER END -->
</section>
<!-- PAGE CONTENT ENDS ------------------------------------------------------------------------------------------------------------------------------------- -->
    <link href="<%=host %>CSS/bootstrap-datetimepicker.css" rel="stylesheet" />
    <script src="<%=host %>JS/moment.js"></script>
    <script src="<%=host %>JS/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(function () {
                $('#datetimepicker1').datetimepicker({ format: 'LL' });
                //$('#datepicker').datepicker({ startDate: '+1d' });
            });
        })
        function validateEmailId() {
            if (!IsValidEmailId($("#" + '<%= txtEmail.ClientID %>').val())) {
                $('#myWarningModal').find('#WarningMessage').html('Please enter valid email id');
                $('#myWarningModal').modal('show');
                return false;
            }
            else {
                return true;
            }
        }

        </script>
</asp:Content>

