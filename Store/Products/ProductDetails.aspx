﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Products_ProductDetails" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divmaincontent">
        <script type="text/javascript">
            var host = '<%=host %>';
            var VideoBoxTitle = '<%=strVideoBoxTitle %>';
            var ProductId = '<%=ProductId %>';
            var ShowRecentlyViewedProducts = '<%=blnShowRecentlyViewedProducts %>';
            var ReviewPageSize = '<%=ReviewPageSize %>';
        </script>
        <script type="text/javascript">
            function ShowBasket() {
                $("#quickbasket").stop();
                $("#quickbasket").fadeIn(50, function () { $("#quickbasket").fadeOut(15000); });
            }

            $(function () {

                $('.BasketLink').mouseenter(function () {
                    $('#quickbasket').css({
                        'opacity': '1',
                        'display': 'block'
                    });
                });
                $('.BasketLink').mouseleave(function () {
                    $('#quickbasket').css({
                        'opacity': '0',
                        'display': 'none'
                    });
                });
                $("#quickbasket").mouseover(function () {
                    $("#quickbasket").stop();
                    $("#quickbasket").css("opacity", "1");
                });
                $("#quickbasket").mouseout(function () {
                    $("#quickbasket").stop();
                    $("#quickbasket").fadeIn(50, function () { $("#quickbasket").fadeOut(50); });
                });

                if ($(window).width() <= 767) {
                    $('.productBtnPanel').appendTo('.formobile');
                }
                $(window).resize(function () {
                    if ($(window).width() <= 767) {
                        $('.productBtnPanel').appendTo('.formobile');
                    }
                    if ($(window).width() > 767) {
                        $('.productBtnPanel').appendTo('.blank');
                    }
                });
            });
            function playme(link) {
                //alert('hi');
                // document.getElementById("video1").src = '//www.youtube.com/embed/9B7te184ZpQ?autoplay=1';
                document.getElementById("video").src = link;
                //  document.getElementById("video1").src = 'http://www.w3schools.com/tags/mov_bbb.mp4?autoplay=1';
            }
        </script>


        <section id="PRODUCT_PAGE" class="container">
            <!--PRODUCT NAME -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="pageTitle"><span id="spnProductName" runat="server"></span></h2>
                </div>
            </div>
            <!--PRODUCT NAME END-->
            <!--PRODUCT DETAIL CONTAINER -->
            <div class="row product_outer">
                <!--PRODUCT DETAIL LEFT CONTAINER-->
                <div id="zoom" class="col-lg-5 col-md-5 col-sm-5 col-xs-12 product_image_panel">
                    <!--PRODUCT DETAIL IMAGE WITH ZOOM-->
                    <span id="productZoomImage" runat="server" class="zoom" style="position: relative; overflow: hidden;" clientidmode="Static">
                        <img id="imgProduct" clientidmode="Static" style="width: 100%;" class="product_feature" runat="server" alt="product-img" />
                    </span>
                    <span id="productZoomImageVideo" runat="server" class="zoom" style="position: relative; overflow: hidden; display: none;" clientidmode="Static">
                        <iframe id="video" width="100%;" height="400" clientidmode="Static" frameborder="0" runat="server" allowfullscreen></iframe>
                    </span>
                    <p class="product_feature_message pageSmlText"><%=ZoomMoveText %></p>
                    <div class="blank">
                        <!--PRODUCT BUTTON PANEL-->
                        <div class="productBtnPanel clearfix">
                            <ul class="NonPrintable row">
                                <li class="col-md-2 col-xs-2 text-center " runat="server" id="lstPrint" visible="false">
                                    <a title="Print" class="printIcon text-center proddeticon" id="aPrintProductDetails">
                                        <p class="glyphicon glyphicon-print text-center "></p>
                                        <p>
                                            <asp:Literal ID="ltrPrint" runat="server"></asp:Literal>
                                        </p>
                                        <!--Added By Snehal - For Print Function 14 02 2017 -->
                                        <!-- START -->
                                        <div id="divcontent" style="display: none;">
                                            <asp:Literal ID="ltrPrintContent" runat="server"></asp:Literal>
                                        </div>
                                        <!-- END -->
                                    </a>
                                </li>
                                <li class="col-md-2 col-xs-2 text-center" runat="server" id="lstEmail" visible="false">
                                    <a title="Email" class="emailIcon proddeticon" data-toggle="modal" data-target="#emailThisPage">
                                        <p class="glyphicon glyphicon-envelope"></p>
                                        <p>
                                            <asp:Literal ID="ltrEmail" runat="server"></asp:Literal>
                                        </p>
                                    </a>
                                </li>
                                <li class="col-md-4 col-xs-5 text-center" runat="server" id="lstSaveImage" visible="false">
                                    <asp:LinkButton ID="lnkDownloadImg" runat="server" OnClientClick="window.document.forms[0].target='_blank';" OnClick="lnkDownloadImg_Click" CssClass="cloudIcon proddeticon">
                                        <p class="glyphicon glyphicon-cloud-download"></p>
                                        <p>
                                            <asp:Literal ID="ltrDownloadImage" runat="server"></asp:Literal>
                                        </p>

                                    </asp:LinkButton>
                                </li>
                                <li class="col-md-2 col-xs-2 text-center" runat="server" id="lstWishlist" visible="false">
                                    <asp:LinkButton ID="lnkAddtoWishList" class="requestIcon proddeticon" runat="server" OnClick="lnkAddtoWishList_Click">
                                        <asp:HiddenField ID="hdnSKUCount" runat="server" />
                                        <p class="glyphicon glyphicon-cloud-download"></p>
                                        <p><%=strAddToWishList %></p>
                                    </asp:LinkButton>
                                </li>
                                <li class="col-md-2 col-xs-2 text-center" runat="server" id="lstSavePDF" visible="false">
                                    <asp:LinkButton ID="lnkPDF" runat="server" class="requestIcon proddeticon" OnClick="lnkPDF_Click">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>

                                        <asp:Literal ID="ltrPDF" runat="server"></asp:Literal>
                                    </asp:LinkButton>

                                </li>
                            </ul>
                        </div>
                        <!--END PRODUCT BUTTON PANEL-->
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 product_text_panel_top">
                    <!--SHOTS & COLOR THUMBNAILS-->
                    <div class="row">
                        <div class="col-xs-1 vedioIcon" id="divVideo" runat="server" visible="false">
                            <a id="lnkVideoLink" runat="server" onserverclick="lnkVideoLink_ServerClick">
                                <i class="fa fa-play-circle " aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="col-xs-10">
                            <asp:Repeater ID="rptVariantTypeList" runat="server" OnItemDataBound="rptVariantTypeList_ItemDataBound">
                                <ItemTemplate>
                                    <div id="productVariantTitle" runat="server" class="product_detail_images customTableHead" visible="false">
                                        <%# Eval("ProductVariantTypeName") %>
                                    </div>
                                    <div class="product_swatches_block">
                                        <asp:Repeater ID="rptVariantList" runat="server" OnItemDataBound="rptVariantList_ItemDataBound">
                                            <ItemTemplate>
                                                <%--<div id="liProductVariant" runat="server">--%>
                                                <a href="" id="lnkVariant" rel='<%# Eval("ProductId")+"|"+Eval("ProductVariantTypeId") %>' onserverclick="lnkVariant_ServerClick" runat="server">
                                                    <img id="imgVariant" class="product_swatch" runat="server"></a>
                                                <%--</div>--%>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <!--END SHOTS & COLOR THUMBNAILS-->
                </div>
                <!--END PRODUCT DETAIL LEFT CONTAINER-->
                <!--PRODUCT DETAIL RIGHT TABLE-->
                <div class="formobile">
                </div>
                <div id="pdDetailsPID" class="col-lg-7 col-md-7 col-sm-7 col-xs-12 product_text_panel_top">
                    <!--NEEDS TO FOLLOW BELOW HTML FOR COLORS THUMBNAILS-->

                    <!--NEEDS TO FOLLOW BELOW HTML FOR COLORS THUMBNAILS-->
                    <!--SHOTS & COLOR THUMBNAILS-->
                    <div id="dvSectionIcons" runat="server" visible="false" class="product_swatches_block"></div>
                    <!--PRODUCT DESCRIPTION-->
                    <p class="productID productName"><span id="spnProductCode" runat="server"></span></p>
                    <div class="productInfoTable clearfix">
                        <div class="productTableTitle customTableHead">
                            <asp:Literal ID="ltrDescription" Visible="false" runat="server"></asp:Literal>
                        </div>
                        <p class="descTableTxt"><span id="spnProductDescription" runat="server" class="productDescription"></span></p>
                    </div>
                    <!--END PRODUCT DESCRIPTION-->
                    <!--PRODUCT RATING -->
                    <div id="dvReviewRating" class="product_rating_stars" runat="server" visible="false">
                        <div id="dvReadReview" class="showReviewBlock" style="display: none;">
                            <a role="button" class="hyperLink" data-toggle="collapse" href="#collapseReviews" aria-expanded="false" aria-controls="#collapseReviews"><span id="spnReadReview" runat="server"></span><span id="spnReviewCount"></span></a>&nbsp;
                                <span id="spnAvgRating1" class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                            <span id="spnAvgRating2" class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                            <span id="spnAvgRating3" class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                            <span id="spnAvgRating4" class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                            <span id="spnAvgRating5" class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true"></span>&nbsp;
                        </div>
                        &nbsp;
                                <a class="reviewContainerLink" data-toggle="modal" data-target="#ModalWriteReview" id="aReview" style="margin-left: -7.5px;"><span id="spnWriteReview" runat="server"></span></a>


                    </div>
                    <!--END PRODUCT RATING -->
                    <!--PRODUCT SHARE -->
                    <div id="dvSocialMedia" runat="server" visible="false">
                        <div class="product_available_colours customTableHead"><%=SocialMediaText %></div>
                        <div class="product_social_icon_row">
                            <asp:Repeater ID="rptSocialMedia" runat="server" OnItemDataBound="rptSocialMedia_ItemDataBound">
                                <ItemTemplate>
                                    <a id="aSocialMedia" target="_blank" runat="server">
                                        <img id="imgSocialMedia" runat="server" class="product_social_icon" /></a>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <!--END PRODUCT SHARE -->

                    <%-- PRODUCT TABLE Size Tab  --%>
                    <div id="dvProductSize" class="product_sizes_guide" runat="server"></div>
                    <div class="clearfix"></div>
                    <%--PRODUCT TABLE  Pricing--%>
                    <div id="dvProductPriceTable" runat="server">
                    </div>
                    <div class="clearfix"></div>
                    <%-- PRODUCT TABLE Details Tab--%>
                    <div id="dvTableProductDetail" runat="server" visible="false"></div>
                    <div class="clearfix"></div>
                    <asp:HiddenField ID="hdnFirstPriceBrk" runat="server" ClientIDMode="Static" />

                    <!--PROUCT ENQUIERY -->

                    <!-- Product Enquiry Start -->
                    <div id="dvProductEnquiry" runat="server">
                        <div class="product_bottom_button">
                            <a id="enquiry_button" class="btn btn-primary pull-left">
                                <asp:Literal ID="ltrEnquireText" runat="server"></asp:Literal></a>
                            <div id="made_to_order_submitted">
                                <asp:Literal ID="ltrEnquirySentMsg" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div id="made_to_order" class="collapse">
                            <!-- COLLAPSE START -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <a href="javascript:close_MadeToOrder()" type="button" class="close customClose">&times;</a>
                                    <p class="pageText">
                                        <asp:Literal ID="ltrEnquireProductTitle" runat="server"></asp:Literal>
                                    </p>
                                    <div class="form-group">
                                        <asp:Label ID="lblEmailAddress" CssClass="customLabel" runat="server"></asp:Label><span class="text-danger">*</span>
                                        <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="form-control input-sm customInput" onblur="return ValidateEmailProdEnq()"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvEmailAddress" CssClass="text-danger errorMsg" runat="server" ControlToValidate="txtEmailAddress"
                                            Display="Static" ValidationGroup="ValidateEnquiry"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblQtyRequired" runat="server" CssClass="customLabel"></asp:Label><span class="text-danger">*</span>
                                        <asp:TextBox ID="txtQtyRequired" CssClass="form-control input-sm customInput" runat="server" TextMode="Number"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvQtyrequired" runat="server" class="text-danger errorMsg" ControlToValidate="txtQtyRequired" Display="Static"
                                            ValidationGroup="ValidateEnquiry"></asp:RequiredFieldValidator>

                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblDeliveryDate" runat="server" CssClass="customLabel"></asp:Label><span class="text-danger">*</span>
                                        <div class='input-group date' id='datetimepicker1'>

                                            <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control input-sm customInput"></asp:TextBox>

                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>

                                        </div>
                                        <asp:RequiredFieldValidator ID="rfvDeliveryDate" class="text-danger errorMsg" runat="server" ControlToValidate="txtDeliveryDate" Display="Static"
                                            ValidationGroup="ValidateEnquiry"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lblBranding" CssClass="customLabel" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtBranding" runat="server" CssClass="form-control input-sm customInput" Rows="3"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqBranding" runat="server" class="text-danger errorMsg" ControlToValidate="txtBranding" Display="Static"
                                            ValidationGroup="ValidateEnquiry"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group" id="dvColorEnquiry" runat="server" visible="false">
                                        <asp:Label ID="lblColor" CssClass="customLabel" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtColor" runat="server" CssClass="form-control input-sm customInput" Rows="3"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqColour" runat="server" class="text-danger errorMsg" ControlToValidate="txtColor" Display="Static"
                                            ValidationGroup="ValidateEnquiry" Enabled="false"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group" runat="server" id="dvfileupload">
                                        <asp:Label ID="lblLogo" CssClass="customLabel" runat="server"></asp:Label>
                                        <div class='input-group form-control input-sm customInput'>
                                            <asp:FileUpload ID="fuLogo" runat="server" class="form-control " BorderWidth="0" Style="padding: 0px" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label></label>
                                        <asp:Label ID="lblAdditionalInfo" CssClass="customLabel" runat="server"></asp:Label>
                                        <textarea id="txtareaAdditionalInfo" runat="server" class="form-control input-sm customInput" rows="8"></textarea>
                                        <asp:RequiredFieldValidator ID="ReqAdditionalInfo" runat="server" class="text-danger errorMsg" ControlToValidate="txtareaAdditionalInfo" Display="Static"
                                            ValidationGroup="ValidateEnquiry"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <div id="dvConfigurableFields" runat="server" visible="false"></div>
                                    </div>
                                    <div class="made_to_order_button">
                                        <asp:Button ID="btnSubmitEnquiry" ClientIDMode="Static" runat="server" ValidationGroup="ValidateEnquiry" CssClass="btn btn-primary pull-right customActionBtn" OnClick="btnSubmitEnquiry_Click" />
                                    </div>
                                </div>
                                <!-- PANEL BODY END -->
                            </div>
                            <!-- PANEL END -->
                        </div>
                        <!-- COLLAPSE END -->
                    </div>
                    <!-- Product Enquiry End -->
                    <!-- END PROUCT ENQUIERY-->
                    <div id="dvProductOrder" runat="server" clientidmode="static">
                        <div class="col-md-12 col-sm-12 product_text_panel_bottom" style="padding: 0px; margin-top: 15px">
                            <div class="product_swatches_block">
                                <div class="product_swatch_selected">
                                    <img id="product_colour_selected" runat="server" width="100" alt="" />
                                </div>



                            </div>

                            <div class="container hidden-sm hidden-md hidden-lg" style="display: none" id="product_small">
                                <!-- CONTAINER START -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table>
                                            <tr>
                                                <td width="28%"></td>
                                                <td width="52%"></td>
                                                <td width="20%"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="sml_product_info"><%=SizeQuantityText %></td>
                                            </tr>
                                            <tr>
                                                <td class="sml_product_data">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="sml_product_data">&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- COL END -->
                                </div>
                                <!-- ROW END -->
                            </div>

                            <!--PRODUCT SWATCH TO DISPLAY AS PER COLOR SELETION-->

                            <!--END PRODUCT SWATCH-->
                            <div class="row text-left" id="divProducPdf" runat="server" visible="false">
                                <div class="col-xs-2">
                                    <asp:LinkButton ID="LinkButton1" runat="server" class="requestIcon proddeticon" OnClick="lnkProductPDF_Click">
                                        <p class=""><i class="fa fa-file-pdf-o" aria-hidden="true"></i></p>
                                        <asp:Literal ID="ltrProductPDF" runat="server"></asp:Literal>
                                    </asp:LinkButton>
                                </div>
                            </div>
                            <div class="container">
                                <%=SizeQuantityText %><br />

                                <div class="product_bottom_titles">
                                    <div class="product_bottom_product"><%= strProductTitle %></div>
                                    <div class="product_bottom_stock"><%=strStockTitle %></div>
                                    <div class="product_bottom_stock_due"><%=strStockDueText %></div>
                                    <div class="product_bottom_price"><%=strPriceTitle %></div>
                                    <div class="product_bottom_quantity"><%=strQuantityTitle %></div>
                                </div>


                                <div id="dvRptReportSKU">
                                    <asp:Repeater ID="rptReportSKU" runat="server" OnItemDataBound="rptReportSKU_ItemDataBound">
                                        <ItemTemplate>


                                            <div class="standard_basket_row">
                                                <div class="product_bottom_xs_titles"><%= strProductTitle %></div>
                                                <div class="product_bottom_product">
                                                    <asp:Label ID="lblProductId" CssClass="BodyNormalText_New12 customTableText" runat="server" Visible="false"
                                                        Text='<%#Eval("productId")%>'></asp:Label>
                                                    <asp:Label ID="lblItemCode" CssClass="BodyNormalText_New12 customTableText" runat="server" Text='<%#Eval("SKU")%>'></asp:Label><br />
                                                    <asp:Label ID="lblProductName" CssClass="BodyNormalText_New12 customTableText" runat="server" Text='<%#Eval("SKUName")%>'></asp:Label>
                                                    <asp:Label ID="lblSKUId" runat="server" Text='<%#Eval("ProductSKUId")%>' Visible="false"></asp:Label>
                                                </div>
                                                <div class="product_bottom_xs_titles"><%=strStockTitle %></div>
                                                <div class="product_bottom_stock">
                                                    <asp:HiddenField ID="hdnMaximumOrderQuantity" runat="server" Value='<%# Eval("MaximumOrderQuantity")%>' />
                                                    <asp:HiddenField ID="hdnMinimumQuantity" runat="server" Value='<%#Eval("MinimumOrderQuantity")%>' />
                                                    <asp:Label ID="lblStock" CssClass="BodyNormalText_New12 customTableText" Text='' runat="server"></asp:Label>

                                                </div>
                                                <div class="product_bottom_xs_titles"><%=strStockDueText %></div>
                                                <div class="product_bottom_stock_due">
                                                    <asp:Label ID="lblStatus" CssClass="BodyNormalText_New12 customTableText" runat="server" Text='' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblDate" CssClass="BodyNormalText_New12 customTableText" Text='n/a' runat="server" Visible="true"></asp:Label>
                                                </div>
                                                <div class="product_bottom_xs_titles"><%=strPriceTitle %></div>
                                                <div class="product_bottom_price">
                                                    <div>
                                                        <asp:Label ID="lblSKuCurrencySymbolstrike" CssClass="price_was" runat="server"></asp:Label>
                                                        <asp:Label ID="lblstrickprice" runat="server" CssClass="price_was"></asp:Label>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblSKuCurrencySymbol" CssClass="BodyNormalText_New12 customTableText" runat="server"></asp:Label>
                                                        <asp:Label ID="lblPrice" runat="server" CssClass="BodyNormalText_New12 customTableText FpriceBreak"></asp:Label>
                                                        <asp:Label class="BodyNormalText_New12 customTableText" ID="lblspnor" runat="server"></asp:Label>
                                                        <asp:Label class="BodyNormalText_New12 customTableText" ID="lblsPrice" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="product_bottom_xs_titles"><%=strQuantityTitle %></div>
                                                <div class="product_bottom_quantity">
                                                    <asp:TextBox ID="txtQuantity" runat="server" MaxLength="6" CssClass="form-control input-sm dissable customInput PriceBrkOnKeyUp" Width="110" autocomplete="off"
                                                        onkeypress="return isNumbers(event)"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnSKUId" runat="server" Value='<%#Eval("ProductSKUId")%>' />
                                                    <input type="hidden" id="hdnStockStatus" runat="server" />
                                                </div>
                                            </div>



                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>

                            </div>
                            <asp:HiddenField ID="hdnSKUIdAddWishList" runat="server" />
                            <div class="btnShopping">
                                <asp:Button ID="btnShopping" runat="server"
                                    CssClass="btn btn-primary customActionBtn shoppingbtn" ToolTip="Click here to keep shopping" OnClick="btnShopping_Click" />
                                <div class="row" runat="server" id="divUserInput" clientid="static">
                                    <div class="col-xs-6">
                                        <asp:Label class="BodyNormalText_New12 customTableText" ID="lblUserInput" runat="server"> :</asp:Label>
                                    </div>
                                    <div class="col-xs-6">
                                        <asp:TextBox ID="txtUserInput" ClientIDMode="Static" runat="server" MaxLength="30" CssClass="form-control input-sm dissable customInput" Width="110" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <asp:Button ID="imgbtnAddtoCart" runat="server"
                                    CssClass="btn btn-primary customActionBtn" OnClick="imgbtnAddtoCart_Click" OnClientClick="return ValidateQuantity()" />
                                <asp:Button ID="btnOrder" runat="server" Text="Add to basket" CssClass="btn btn-primary"
                                    Visible="false" />

                            </div>

                        </div>
                        <!-- COLUMN END -->
                    </div>
                </div>

                <!--END PRODUCT DETAIL RIGHT-->
            </div>
            <%-- PRODUCT REVIEW --%>
            <div class="product_reviews collapse" id="collapseReviews">
                <div class="row">

                    <div class="col-xs-9">
                        <h3 class="pageSubTitle"><span id="spnProductReviews" runat="server"></span>(<span id="spnPageStartRowNumber"></span>-<span id="spnPageEndRowNumber"></span>)</h3>
                    </div>
                    <div class="col-xs-3 product_reviews_paging"><a class="ReviewPrev"><span id="spnreviewprev" clientidmode="Static" runat="server"></span></a>&nbsp;l&nbsp;<a class="ReviewNext"><span id="spnreviewnext" runat="server" clientidmode="Static"></span></a></div>

                    <div id="dvReviewAndRating" class="col-xs-12">
                    </div>
                </div>
            </div>
            <%-- End Read Product Review --%>

            <!-- PRODUCT DETAIL CONTAINER END -->
            <%--Recently Viewed Products--%>
            <div class="container recentlyViewContainer" id="dvRecentlyViewContainer" visible="false" clientidmode="Static" runat="server">
            </div>
            <%--You May Also Like--%>
            <div id="dvYouMayAlsoLike" visible="false" clientidmode="Static" runat="server" class="container youmayLikeContainer">
            </div>
        </section>
        <!--OLD  PRODUCT DETAIL PAGE -->
        <!--OLD  PRODUCT DETAIL PAGE -->
        <!--PRODUCT DETAIL CONTAINER-->
        <div class="container productsDetailContainer">
            <div class="row productListCont">
                <!--PRODUCT DETAIL LEFT CONTAINER-->
                <div class="col-md-4">
                    <div class="productPreviewContainer">
                        <!--PRODUCT IMAGE & ZOOM IMAGE-->
                        <!--END PRODUCT IMAGE & ZOOM IMAGE-->
                        <!--VIRTUAL SAMPLE BUTTON-->

                        <!--END SOCIAL SHARE-->
                    </div>
                </div>
                <!--END PRODUCT DETAIL LEFT CONTAINER-->
                <!--PRODUCT TABLE-->
                <div class="col-md-8 table-responsive">
                    <%-- Review --%>
                </div>
                <!--END PRODUCT TABLE-->
            </div>
        </div>
        <!--END PRODUCT DETAIL CONTAINER-->
        <!-- Product Youtube Video -->

        <!-- END Product Youtube Video  -->
        <!-- MODAL WRITE REVIEW START -->
        <div class="modal fade" id="ModalWriteReview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="customModal modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">
                            <asp:Literal ID="ltrReviewHeading" runat="server"></asp:Literal>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div role="search">
                            <div class="form-group">
                                <asp:Label ID="lblEmailId" runat="server" CssClass="customLabel"></asp:Label>
                                <asp:TextBox ID="txtReviewEmailId" CssClass="form-control customInput" runat="server" MaxLength="250"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvReviewEmailId" runat="server" CssClass="customLabel" ControlToValidate="txtReviewEmailId" Display="Static" ForeColor="Red"
                                    ValidationGroup="ValidateReview"></asp:RequiredFieldValidator>
                                <!-- <asp:RegularExpressionValidator ID="rxvReviewEmaiId" CssClass="customLabel" runat="server" ControlToValidate="txtReviewEmailId" ForeColor="Red"
                                    Display="Static" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="ValidateReview">
                                </asp:RegularExpressionValidator> -->
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lblReviewTitle" runat="server" CssClass="customLabel"></asp:Label>
                                <asp:TextBox ID="txtReviewTitle" CssClass="form-control customInput" runat="server" MaxLength="60"></asp:TextBox>
                            </div>
                            <div class="form-group">

                                <asp:Label ID="lblReviewComment" runat="server" CssClass="customLabel"></asp:Label>
                                &nbsp;(<span id="charNum" class="customLabel">500</span> <span id="spnLetterRemaining" class="customLabel" runat="server"></span>)
                        <textarea class="form-control customInput" runat="server" clientidmode="Static" rows="6" id="txtareaCommentfield" maxlength="500"></textarea>
                            </div>
                            <div class="form-group">

                                <asp:Label ID="lblRating" runat="server" CssClass="customLabel"></asp:Label>
                            </div>

                            <div class="form-group">
                                <div class="review_block">
                                    <div class="review_stars">
                                        <div><a href="javascript:rating_clicked(1)"><span id="rating1" class="rating-stars glyphicon glyphicon-star-empty" aria-hidden="true"></span></a></div>
                                        <div><a href="javascript:rating_clicked(2)"><span id="rating2" class="rating-stars glyphicon glyphicon-star-empty" aria-hidden="true"></span></a></div>
                                        <div><a href="javascript:rating_clicked(3)"><span id="rating3" class="rating-stars glyphicon glyphicon-star-empty" aria-hidden="true"></span></a></div>
                                        <div><a href="javascript:rating_clicked(4)"><span id="rating4" class="rating-stars glyphicon glyphicon-star-empty" aria-hidden="true"></span></a></div>
                                        <div><a href="javascript:rating_clicked(5)"><span id="rating5" class="rating-stars glyphicon glyphicon-star-empty" aria-hidden="true"></span></a></div>
                                    </div>
                                    <div id="review_text" class="pageText">
                                        <asp:Literal ID="ltrRatingText" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnReviewSubmit" runat="server" CssClass="btn btn-primary customActionBtn" OnClientClick="return ValidateReview();" OnClick="btnReviewSubmit_Click" />
                        <%--OnClientClick="return ValidateReview();"--%>
                        <%-- ValidationGroup="ValidateReview"--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL WRITE REVIEW END -->
        <!--Email This Page -->
        <div id="emailThisPage" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="customPanel modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="customPanel modal-title">
                            <asp:Literal ID="ltrheaderEmail" runat="server"></asp:Literal></h4>
                    </div>
                    <div class="modal-body form-horizontal">
                        <p class="emailTitle pageSubTitle">
                            <asp:Literal ID="ltrSenderdetail" runat="server"></asp:Literal>
                        </p>
                        <div class="form-group clearfix">
                            <label class="control-label col-sm-3" for="emailName"><span class="red">*</span><asp:Literal ID="ltrSenderName" runat="server"></asp:Literal></label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtSenderName" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-sm-3" for="yourEmailID"><span class="red">*</span><asp:Literal ID="ltrSenderEmail" runat="server"></asp:Literal></label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtSenderEmail" runat="server" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvEmail" Display="None" ControlToValidate="txtSenderEmail" runat="server" ValidationGroup="SendEmail"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator
                                    ID="revEmail" runat="server" ControlToValidate="txtSenderEmail"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                                    CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="SendEmail"></asp:RegularExpressionValidator>

                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label class="control-label col-sm-3" for="emailMessage">
                                <asp:Literal ID="ltrSenderMsg" runat="server"></asp:Literal></label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtSenderMsg" runat="server" class="form-control" Rows="2"></asp:TextBox>
                            </div>
                        </div>
                        <p class="emailTitle pageSubTitle clearfix">
                            <asp:Literal ID="ltrReciepientdetail" runat="server"></asp:Literal>
                        </p>
                        <div class="AddRecipients">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="recipientName"><span class="red">*</span><asp:Literal ID="ltrRecipientName" runat="server"></asp:Literal></label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtReciepientName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label col-sm-3" for="recipientEmailID"><span class="red">*</span><asp:Literal ID="ltrRecipientEmail" runat="server"></asp:Literal></label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtReciepientEmail" runat="server" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvRecipientEmail" Display="None" ControlToValidate="txtSenderEmail" runat="server" ValidationGroup="SendEmail"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="revRecipientEmail" runat="server" ControlToValidate="txtSenderEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"
                                        CssClass="errortext" ForeColor="" SetFocusOnError="True" ValidationGroup="SendEmail"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <%--   <asp:Button ID="btnAddRecipient" runat="server" class="btn btn-default submitFormBtn customActionBtn" Text="Add Recipient" />--%>
                        <asp:Button ID="btnSend" runat="server" class="btn btn-default customResetBtn" Text="Send" OnClick="btnSend_Click" ValidationGroup="SendEmail" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-default customResetBtn" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hdnImageName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnRating" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnimagepath" runat="server" ClientIDMode="Static" />
        <!-- END Email This Page -->
        <script type="text/javascript">

            function swap_zoomed(filename_new) {
                //debugger;
                var arr = filename_new.split('/');
                var arrlength = arr.length;

                $('#imgProduct').attr('src', filename_new);
                $('.zoomImg').attr('src', filename_new);
                $('#hdnimagepath').attr('Value', filename_new);
                document.getElementById("<%= productZoomImage.ClientID %>").Style.display = "block";
                document.getElementById("<%= productZoomImageVideo.ClientID %>").Style.display = "none";

                return false;

            }
        </script>

        <div class="modal fade" id="ModalWishList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content customModal">
                    <div class="customModal modal-header customPanel">
                        <button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title pageSubSubTitle" id="WishlistModalLabel">WishList</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div id="divLabel" runat="server" class="control-label col-sm-3">
                            <%=Wish_Choose_SKU_Item_Text%>
                        </div>
                        <div id="divDropDownSKU" runat="server">
                            <asp:DropDownList ID="ddlSKU" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnAdd" runat="server" class="btn btn-default customButton" OnClick="btnAdd_Click" />
                        <button type="button" class="btn btn-default customButton" data-dismiss="modal"><%=Close_Btn%></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ModalAddRegion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content customModal">
                    <div class="customModal modal-header customPanel">
                        <button type="button" class="close customClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title pageSubSubTitle" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="form-group clearfix">
                            <label for="CarriageMethod" class="control-label col-xs-2">Region Name</label>

                        </div>

                    </div>
                    <div class="form-group clearfix">
                        <div class="buttonPanel container">
                            <ul class="button_section">
                                <li>
                                    <%--   <asp:Button ID="btnSaveRegion" runat="server" CssClass="btn" Text="Save Region" OnClick="btnSaveRegion_Click" />--%>
                                    <asp:Button ID="btnSaveRegion" runat="server" CssClass="btn" Text="Ok" />
                                <li>
                                    <asp:Button ID="Button5" CssClass="btn gray" runat="server" Text="Cancel" />
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Component CSS & JS -->
        <link href="<%=host %>CSS/star-rating.min.css" rel="stylesheet" />
        <link href="<%=host %>CSS/bootstrap-datetimepicker.css" rel="stylesheet" />

        <script src="<%=host %>JS/star-rating.min.js"></script>
        <script src="<%=host %>JS/jquery.zoom.min.js"></script>
        <script src="<%=host %>JS/moment.js"></script>
        <script src="<%=host %>JS/bootstrap-datetimepicker.js"></script>


        <!-- Page CSS & JS -->
        <!--<link href="<%=host %>CSS/product-detail.css" rel="stylesheet" />-->
        <script src="<%=host %>JS/product-detail.js"></script>
        <script src="<%=host %>JS/custom.js"></script>

        <!-- CAROUSEL OwlSlider CSS-->
        <link href="<%=host %>CSS/owl.carousel.css" rel="stylesheet">
        <link href="<%=host %>CSS/owl.theme.css" rel="stylesheet">
        <link href="<%=host %>CSS/owl.transitions.css" rel="stylesheet">

        <!-- CAROUSEL OwlSlider JS -->
        <script src="<%=host %>JS/owl.carousel.js"></script>

        <!-- Block UI JS -->
        <script src="<%=host %>/JS/jquery.blockUI.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                $(function () {
                    $('#datetimepicker1').datetimepicker({ format: 'LL' });

                });
            })




        </script>
        <script type="text/javascript">
            if (ShowRecentlyViewedProducts == "True") {
                BindRecentlyViewedProducts();
            }
            BindYouMayAlsoLike();


            function EncodeImageName(ImageName) {
                ////debugger;
                ImageName = ImageName.replace(".", "-dot-");
                ImageName = ImageName.replace("'", "-quote-");
                ImageName = ImageName.replace("&", "-and-");
                ImageName = ImageName.replace("+", "-plus-");
                ImageName = ImageName.replace("/", "-slash-");
                ImageName = ImageName.replace("\"", "-dbq-");
                return ImageName;
            };

            $(function () {
                var controls = $(".dissable");
                controls.bind("paste", function () {
                    return false;
                });
            });

            function isNumbers(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            /* Added By Hardik For price break changes */
            $('#dvRptReportSKU .PriceBrkOnKeyUp').keyup(function () {
                debugger;
                //var lblPrice = $("[id*='rptReportSKU_lblPrice_']");
                var ProductId = '<%=ProductId %>';
                var QuantityVal = $(this).val();
                var totalQty = gettxtQuantity(QuantityVal)
                var obj = {};
                //obj.ProductId = ProductId;
                obj.totalQty = totalQty;
                //alert(totalQty);
                try {
                    $.ajax({
                        type: "POST",
                        url: host + 'Products/ProductDetails.aspx/getPriceBreakforProducts',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        cache: false,
                        async: false,
                        
                        success: function (response) {
                            var Res = response.d;                            
                            $(".FpriceBreak").html(Res);
                            //return Res;
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                } catch (e) { }
                //$(".FpriceBreak").html(Res);
            });

            function gettxtQuantity(QuantityVal) {
                //debugger;
                var totalQuantity = 0;
                var quantity = $("[id*='rptReportSKU_txtQuantity_']");
                if (quantity != null) {
                    var count = quantity.length;
                    for (var i = 0; i < count; i++) {
                        if (quantity[i].value != "") {
                            totalQuantity = parseInt(quantity[i].value) + parseInt(totalQuantity);
                        }
                    }
                    return totalQuantity;
                }
            }

            //function isPriceBreak() {
            //    debugger;
            //    var MinPB = $('#hdnFirstPriceBrk').val();
            //    //var price = $(this).val();
            //    alert(price);
            //}

            function ValidateQuantity() {
                debugger;
                var _return = false;
                var MinMaxProductQuantity = "<%=MinMaxProductQuantity%>"
                var AddQuantityAlert = "<%=AddProductQuantity%>"
                var BlockUI_Message = "<%=BlockUI_Message%>"
                var spn = document.getElementById('WarningMessage');
                var quantity = $("[id*='rptReportSKU_txtQuantity_']");
                var stock = $("[id*='rptReportSKU_lblStock_']");
                var minOrder = $("[id*='rptReportSKU_hdnMinimumQuantity_']");
                var maxOrder = $("[id*='rptReportSKU_hdnMaximumOrderQuantity_']");
                var maxO = 0;
                var minO = 0;
                var totalQuantity = 0;
                var MinProdPrcBrk = "<%=MinPriceBreak%>"
                var MinPB = $('#hdnFirstPriceBrk').val();

                if (quantity != null) {

                    var count = quantity.length;
                    for (var i = 0; i < count; i++) {
                        if (quantity[i].value != "") {

                            maxO = parseInt(maxOrder[i].value);
                            minO = parseInt(minOrder[i].value);
                            MinMaxProductQuantity = MinMaxProductQuantity.replace("{min}", minO).replace("{max}", maxO);
                            totalQuantity = parseInt(quantity[i].value) + parseInt(totalQuantity);
                            quantity[i].style.borderColor = "#ccc";
                            //Sachin Chauhan Start: 04 01 2015 : Added by pass condition where min & max order qty comes zero 
                            if (minO == 0 && maxO == 0)
                                return true;
                            //Sachin Chauhan End : 04 01 2015
                            if (totalQuantity < minO || totalQuantity > maxO) {
                                quantity[i].style.borderColor = "red";
                                error = MinMaxProductQuantity;
                                spn.innerHTML = error;
                                alert("Hi");
                                $('#myWarningModal').modal('show');
                                return false;
                            }
                            _return = true;
                            if (stock > quantity) {
                                alert("Hi");
                                //quantity[i].style.borderColor = "red";
                                //error = MinMaxProductQuantity;
                                //spn.innerHTML = error;
                                //$('#myWarningModal').modal('show');
                                $('#ModalAddRegion').modal('show')
                                return false;
                            }
                        }
                        else {
                            quantity[i].style.borderColor = "#ccc";
                        }
                    }
                    //Added By Hardik for Price brk - 17/May/2017
                    MinProdPrcBrk = MinProdPrcBrk.replace("{minP}", MinPB);

                    if (totalQuantity < MinPB) {
                        error = MinProdPrcBrk;
                        spn.innerHTML = error;
                        $('#myWarningModal').modal('show');
                        return false;
                    }
                }
                if (_return == false) {
                    spn.innerHTML = AddQuantityAlert;
                    $('#myWarningModal').modal('show');
                    return false;
                }
                return _return;
            }

            $('#enquiry_button').on('click', function (e) {
                $('#enquiry_button').hide();
                $('#made_to_order').collapse('show');
            });

            function close_MadeToOrder() {
                $('#made_to_order').collapse('hide');
                $('#enquiry_button').show();
            }

            function submit_MadeToOrder() {
                $('#made_to_order').collapse('hide');
                $('#enquiry_button').show();
                $('#made_to_order_submitted').fadeIn(2000, function () { $('#made_to_order_submitted').fadeOut(7000); });
            }

            function Validation_MadeToOrder() {
                $('#made_to_order').collapse('show');
                $('#enquiry_button').hide();
            }

            $('#collapseReviews').on('shown.bs.collapse', function () { $('html, body').animate({ scrollTop: $(collapseReviews).offset().top }, 1500); });


            $("#txtareaCommentfield").keyup(function () {
                el = $(this);
                if (el.val().length >= 500) {
                    el.val(el.val().substr(0, 500));
                } else {
                    $("#charNum").text(500 - el.val().length);
                }
            });

            var strStarRating1, strStarRating2, strStarRating3, strStarRating4, strStarRating5;
            strStarRating1 = '<%=strStarRating1 %>';
            strStarRating2 = '<%=strStarRating2 %>';
            strStarRating3 = '<%=strStarRating3 %>';
            strStarRating4 = '<%=strStarRating4 %>';
            strStarRating5 = '<%=strStarRating5 %>';

            function rating_clicked(which_rating) {
                which_rating = parseInt(which_rating);

                if ($("#rating1").hasClass("glyphicon-star")) {
                    $('#rating1').removeClass('glyphicon-star');
                    $('#rating1').addClass('glyphicon-star-empty');
                }
                if ($("#rating2").hasClass("glyphicon-star")) {
                    $('#rating2').removeClass('glyphicon-star');
                    $('#rating2').addClass('glyphicon-star-empty');
                }
                if ($("#rating3").hasClass("glyphicon-star")) {
                    $('#rating3').removeClass('glyphicon-star');
                    $('#rating3').addClass('glyphicon-star-empty');
                }
                if ($("#rating4").hasClass("glyphicon-star")) {
                    $('#rating4').removeClass('glyphicon-star');
                    $('#rating4').addClass('glyphicon-star-empty');
                }
                if ($("#rating5").hasClass("glyphicon-star")) {
                    $('#rating5').removeClass('glyphicon-star');
                    $('#rating5').addClass('glyphicon-star-empty');
                }



                if ($("#rating1").hasClass("glyphicon-star-empty")) {
                    $('#rating1').removeClass('glyphicon-star-empty');
                    $('#rating1').addClass('glyphicon-star');
                }

                if (which_rating > 1) {
                    if ($("#rating2").hasClass("glyphicon-star-empty")) {
                        $('#rating2').removeClass('glyphicon-star-empty');
                        $('#rating2').addClass('glyphicon-star');

                    }
                }

                if (which_rating > 2) {
                    if ($("#rating3").hasClass("glyphicon-star-empty")) {
                        $('#rating3').removeClass('glyphicon-star-empty');
                        $('#rating3').addClass('glyphicon-star');

                    }
                }

                if (which_rating > 3) {
                    if ($("#rating4").hasClass("glyphicon-star-empty")) {
                        $('#rating4').removeClass('glyphicon-star-empty');
                        $('#rating4').addClass('glyphicon-star');

                    }
                }

                if (which_rating > 4) {
                    if ($("#rating5").hasClass("glyphicon-star-empty")) {
                        $('#rating5').removeClass('glyphicon-star-empty');
                        $('#rating5').addClass('glyphicon-star');

                    }
                }

                switch (which_rating) {
                    case 1:
                        $("#review_text").html(strStarRating1);
                        $('#hdnRating').val("1");
                        break
                    case 2:
                        $("#review_text").html(strStarRating2);
                        $('#hdnRating').val("2");
                        break
                    case 3:
                        $("#review_text").html(strStarRating3);
                        $('#hdnRating').val("3");
                        break
                    case 4:
                        $("#review_text").html(strStarRating4);
                        $('#hdnRating').val("4");
                        break
                    case 5:
                        $("#review_text").html(strStarRating5);
                        $('#hdnRating').val("5");
                        break
                }
            }

            $('.reviewContainerLink').click(function () {
                $(this).removeAttr('data-dismiss');
            });
            var PageIndex = 0;

            if (PageIndex == 0)
                BindProductReviewAndRating(ProductId, PageIndex, ReviewPageSize);

            $('.ReviewNext').click(function () {
                PageIndex++;
                BindProductReviewAndRating(ProductId, PageIndex, ReviewPageSize);
            });

            $('.ReviewPrev').click(function () {
                PageIndex--;
                BindProductReviewAndRating(ProductId, PageIndex, ReviewPageSize);
            });

            function BindProductReviewAndRating(ProductId, PageIndex, ReviewPageSize) {
                ////debugger;
                $('#dvReviewAndRating').html('');
                try {
                    $.ajax({
                        type: "POST",
                        url: host + 'Products/ProductReview.aspx',
                        data: {
                            ProductId: ProductId,
                            PageIndex: PageIndex,
                            PageSize: ReviewPageSize
                        },
                        cache: true,
                        async: true,
                        success: function (response) { Test(response) },
                        beforeSend: function (request) {
                            $.blockUI({ message: '<h2><img class="newloader" src="' + host + 'images/UI/ajax-loader.gif" /> <%=BlockUI_Message%></h2>' });

                        }
                    });
                    } catch (e) { }
                    finally { $.unblockUI(); }
                }
                function Test(response) {
                    ///  //debugger;
                    $('#dvReviewAndRating').html(response);
                    var pagestartrownumber = $('#hdnPageStartRowNumber').val();
                    var pageendrownumber = $('#hdnPageEndRowNumber').val();
                    var TotalRows = $('#hdnTotalReview').val();
                    var AvgReviewRating = $('#hdnAvgRating').val();
                    $('#spnReviewCount').text('(' + TotalRows + ')');
                    $('#spnPageStartRowNumber').text(pagestartrownumber);
                    $('#spnPageEndRowNumber').text(pageendrownumber);

                    if (parseInt(TotalRows) == 0)
                        $('#dvReadReview').hide();
                    else
                        $('#dvReadReview').show();
                    if (parseInt(pageendrownumber) >= parseInt(TotalRows))
                        $('#spnreviewnext').hide();
                    else
                        $('#spnreviewnext').show();
                    if (parseInt(pagestartrownumber) <= 1)
                        $('#spnreviewprev').hide();
                    else
                        $('#spnreviewprev').show();

                    switch (AvgReviewRating) {
                        case "5":
                            $('#spnAvgRating5').removeClass('glyphicon-star-empty');
                            $('#spnAvgRating5').addClass('glyphicon-star');
                        case "4":
                            $('#spnAvgRating4').removeClass('glyphicon-star-empty');
                            $('#spnAvgRating4').addClass('glyphicon-star');
                        case "3":
                            $('#spnAvgRating3').removeClass('glyphicon-star-empty');
                            $('#spnAvgRating3').addClass('glyphicon-star');
                        case "2":
                            $('#spnAvgRating2').removeClass('glyphicon-star-empty');
                            $('#spnAvgRating2').addClass('glyphicon-star');
                        case "1":
                            $('#spnAvgRating1').removeClass('glyphicon-star-empty');
                            $('#spnAvgRating1').addClass('glyphicon-star');
                            break;
                        default:
                            $('#spnAvgRating1').addClass('glyphicon-star-empty');
                            $('#spnAvgRating2').addClass('glyphicon-star-empty');
                            $('#spnAvgRating3').addClass('glyphicon-star-empty');
                            $('#spnAvgRating4').addClass('glyphicon-star-empty');
                            $('#spnAvgRating5').addClass('glyphicon-star-empty');
                            break;

                    }

                }
                function ValidateReview() {

                    var strRatingErrorMsg = '<%=strRatingErrorMsg %>';
                    var strReviewValidateEmail = "<%=strReviewValidateEmail %>";
                    var IsRating = document.getElementById('<%= hdnRating.ClientID %>');
                    var ReviewEmailId = document.getElementById('<%= txtReviewEmailId.ClientID %>');
                    if (ReviewEmailId.value.trim() == "") {
                        //alert(strReviewValidateEmail);
                        $('#myErrorModal').find('#ErrorMessage').html('' + strReviewValidateEmail + '');
                        $('#myErrorModal').modal('show');
                        return false;
                    }
                    if (!IsValidEmailId($("#ContentPlaceHolder1_txtReviewEmailId").val())) {
                        //alert(strReviewValidateEmail);
                        $('#myErrorModal').find('#ErrorMessage').html('' + strReviewValidateEmail + '');
                        $('#myErrorModal').modal('show');
                        return false;
                    }
                    else {
                        if (IsRating.value.trim() == "") {
                            //alert(strRatingErrorMsg);
                            $('#myErrorModal').find('#ErrorMessage').html('' + strRatingErrorMsg + '');
                            $('#myErrorModal').modal('show');
                            return false;
                        }
                        else { return true; }
                    }
                    //return true;
                }
                function ShowModalWishList() {
                    $('#ModalWishList').modal('show');
                }
                function ValidateEmail(email) {
                    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                    return expr.test(email);
                };
                function ShowReviewA() {
                    $("#aReview").click();
                }
                $(function () {
                    $('.thumbnails').click(function () {
                        $(this).addClass('active');
                    })
                });

                var strReqEmailValidator = '<%=strReqEmailValidator%>';

        </script>

        <%--    <script type="text/javascript">

            function ShowModal() {
                $('#ModalAddRegion').modal('show')
            }
    </script>--%>
    </div>

</asp:Content>

