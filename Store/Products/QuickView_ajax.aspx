﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Products_QuickView_ajax" %>

<div class="row">
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 product_image_panel">
        <div class="col-md-12" id="slider-thumbs">
            <img id="imgProduct" class="product_feature" runat="server" />
        </div>
    </div>
    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 product_text_panel_top">
        <div class="pageText">
            <asp:literal id="ltrDescription" runat="server"></asp:literal>
        </div>
        <div class="product_detail_button">
            <a id="aViewDetails" runat="server" class="btn btn-default pull-right customButton text-right"></a>
        </div>

    </div>
</div>
<input type="hidden" id="hidProductName" runat="server" />

