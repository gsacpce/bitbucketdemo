﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Products_Products_RecentlyViewedProducts" %>

<div class="container recentlyViewContainer" id="dvRecentlyViewContainer" runat="server">
    <p class="pageSubSubTitle">
        <asp:literal id="ltrRecentlyViewedProducts" runat="server"></asp:literal>
    </p>



    <div class="row grid">
        <div class="col-xs-12">
            <ul id="owl-demo" class="owl-carousel owl-theme recentlyView tabBg">
                <!-- OWL CAROUSEL START-->


                <asp:repeater id="rptRecentlyViewed" runat="server" onitemdatabound="rptRecentlyViewed_ItemDataBound">
		  <ItemTemplate>
			  <li class="item text-center ">
				  
                       <a id="lnkProductImage" runat="server">
                           <a class="productName" id="lnkProductName" runat="server">
                               
                                    <div class="prod_list_cell">
                                      <div class="prod_list_image_outer">
                                      <img id="imgRecentlyViewedProduct" class="prod_list_image_inner" runat="server" />
                                          <div id="dvSectionIcons" class="product_list_icon_row" runat="server"></div>
                                      </div>
                                      <div class="prod_list_text_outer">
                                        <div class="prod_list_text_title productSmlName">
                                            <asp:Literal ID="ltrProductName" runat="server" Text='<%# Eval("ProductName") %>'></asp:Literal>
                                        </div>
                                            <div class="prod_list_text_code productCode">
                                                <asp:Literal ID="ltrProductCode" runat="server" Text='<%# Eval("ProductCode") %>' ></asp:Literal>
                                            </div>   
                                           <div class="prod_list_text_price productPrice">
                                               <div class="price price_now" id="dvstrike"  runat="server">
                                                   <asp:Literal ID="ltrstrikeprice"  Text='<%# String.Format("{0}{1}", (GlobalFunctions.GetCurrencySymbol()), Eval("MinstrikePrice")) %>' runat="server"></asp:Literal>
                                                
                                               </div>
                                             
                                            <div class=" price_was" id="dvprice"  runat="server">
                                               <asp:Literal ID="ltrPriceAndDiscountCode" Text='<%# String.Format("{0}{1}", (GlobalFunctions.GetCurrencySymbol()), Eval("MinPrice")) %>' runat="server"></asp:Literal>
                                                     <div  id="spnor"  runat="server"></div> 
                                            <div id="spnPrice1"  runat="server"></div>  
                                          </div>
                                                 </div>                                     
                                         
                                      </div>
                                </div>
    </a>
                           </a>
			  </li>

		  </ItemTemplate>
	  </asp:repeater>


            </ul>
            <!-- END: OWL CAROUSEL START-->
        </div>
    </div>




</div>








