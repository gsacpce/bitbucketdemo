﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Products_filter_ajax" %>

<div id="divFilter" class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <!--PRICE FILTER-->
    <div class="panel customPanel panel-default" id="dvPriceFilter" runat="server">
        <a role="button" class="collapsed" data-toggle="collapse" data-target="#Price" aria-expanded="true">
            <div class="panel-heading customAccordions panel-heading-a" role="tab" id="headingOne">
                <h4 class="panel-title customAccordionsText" id="hPrice" runat="server">Price</h4>
            </div>
        </a>
        <div id="Price" class="panel-collapse collapse in" aria-expanded="true" aria-labelledby="headingOne">
            <div class="panel-body">
                <div class="SliderRange text-center">
                    <p  class="pageSmlText"><span id="pPriceRange" runat="server"></span></p>
                    <input id="priceRange" runat="server" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000"
                        data-slider-step="5" data-slider-value="[250,450]" />
                    <div class="row">
                        <b id="bMinPrice" runat="server" class="pageSmlText text-left col-md-5 col-sm-5 col-sm-offset-1 col-xs-offset-0 col-xs-6"></b>
                        <b id="bMaxPrice" runat="server" class="pageSmlText text-right col-md-5 col-sm-5 col-xs-6"></b>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!--END PRICE FILTER-->
    <!--SUB CATEGORY-->
    <div class="panel customPanel panel-default" id="dvSubCategoryFilter" runat="server">
        <a role="button" class="collapsed" aria-controls="SubCategory" href="#SubCategory" data-toggle="collapse" aria-expanded="false">
            <div class="panel-heading customAccordions panel-heading-a" role="tab">
                <h4 class="panel-title customAccordionsText" id="hSubCategory" runat="server"></h4>
            </div>
        </a>
        <div id="SubCategory" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">
            <div class="panel-body">
                <a id="aParentCategory" runat="server" class="pageText"></a>

                <asp:repeater id="rptCategories" runat="server" onitemdatabound="rptCategories_ItemDataBound">
                <ItemTemplate>
                     <div class="filter_row"><a id="aCategory" runat="server" class="hyperLinkSml"></a></div>
                </ItemTemplate>
            </asp:repeater>

            </div>
        </div>
    </div>
    <!--END SUB CATEGORY-->
    <!--COLOR FILTER -->
    <div class="panel customPanel panel-default" id="dvColorFilter" runat="server">
        <a role="button" class="collapsed" data-toggle="collapse" data-target="#Color" aria-expanded="true">
            <div class="panel-heading customAccordions panel-heading-a" role="tab" id="headingTwo">
                <h4 class="panel-title customAccordionsText" id="hColor" runat="server"></h4>
            </div>
        </a>
        <div id="Color" class="panel-collapse collapse" aria-expanded="true">
            <div class="panel-body">
                <asp:repeater id="rptColor" runat="server" onitemdatabound="rptColor_ItemDataBound">
                <ItemTemplate>
                    <div class="filter_row">
                        <label class="customLabel">
                            <input id="chkColorName" ClientIdMode="static" runat="server" type="checkbox">
                            <asp:Literal id="ltrColorName" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
            </div>
        </div>
    </div>
    <!--END COLOR FILTER -->
    <!--SECTION FILTER -->
    <div class="panel customPanel panel-default" id="dvSectionFilter" runat="server">
        <a class="collapsed"   role="button"  data-toggle="collapse" data-parent="#accordion" data-target="#Section" aria-expanded="false">
              <div class="panel-heading customAccordions panel-heading-a" role="tab">
                                            <h4 class="panel-title customAccordionsText" id="hSection" runat="server">Category</h4>
                                        </div>
        </a>
        <div id="Section" class="panel-collapse collapse" role="tabpanel"  aria-expanded="true">
            <div class="panel-body">
            <asp:repeater id="rptSection" runat="server" onitemdatabound="rptSection_ItemDataBound">
                <ItemTemplate>
                    <div class="filter_row">
                        <label class="customLabel">
                            <input id="chkSectionName" ClientIdMode="static" class="chkSectionName" runat="server" type="checkbox" value="">
                            <asp:Literal id="ltrSectionName" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
                </div>
        </div>
    </div>
    <!-- END SECTION FILTER -->
    <!--SIZE FILTER -->
    <div class="panel customPanel panel-default" id="dvCustomFilter1" runat="server">
        <a runat="server" role="button" class="collapsed" data-toggle="collapse" data-target="#Filter1" aria-expanded="true">
            <div class="panel-heading customAccordions panel-heading-a" role="tab">
                                            <%--<h4 class="panel-title customAccordionsText" id="h1" runat="server">Category</h4>--%>
                     <h4 class="panel-title customAccordionsText" id="hCustomFilter1" runat="server">Category</h4>
                                        </div>
        </a>
        <div id="Filter1" class="panel-collapse collapse" role="tabpanel"  aria-expanded="true">
            <div class="panel-body">
            <asp:repeater id="rptCustomFilter1" runat="server" onitemdatabound="rptCustomFilter1_ItemDataBound">
                <ItemTemplate>
                   <div class="filter_row">
                        <label class="customLabel">
                            <input id="chkCustomFilter1" ClientIdMode="static" class="chkCustomFilter1" runat="server" type="checkbox" value="">
                            <asp:Literal id="ltrCustomFilterName1" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
                </div>
        </div>
    </div>
     <!--END SIZE FILTER -->
    <!--PRODUCTION TIME FILTER -->
    <div class="panel customPanel panel-default" id="dvCustomFilter2" runat="server">
        <a  role="button" class="collapsed" data-toggle="collapse" data-target="#Filter2" aria-expanded="false">
            <div class="panel-heading customAccordions panel-heading-a" role="tab">                                           
                     <h4 class="panel-title customAccordionsText" id="hCustomFilter2" runat="server">Category</h4>
                                        </div>

        </a>
        <div id="Filter2" class="panel-collapse collapse" role="tabpanel"  aria-expanded="true">
             <div class="panel-body">
            <asp:repeater id="rptCustomFilter2" runat="server" onitemdatabound="rptCustomFilter2_ItemDataBound">
                <ItemTemplate>
                     <div class="filter_row">
                        <label class="customLabel">
                            <input id="chkCustomFilter2" ClientIdMode="static" class="chkCustomFilter2" runat="server" type="checkbox" value="">
                            <asp:Literal id="ltrCustomFilterName2" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
                 </div>
        </div>
    </div>
    <!--END PRODUCTION TIME FILTER -->
    <!--FILTER 1 -->
    <div class="panel customPanel panel-default"  id="dvCustomFilter3" runat="server">
        <a role="button" class="collapsed" data-toggle="collapse" data-target="#Filter3" aria-expanded="true">
            <div class="panel-heading customAccordions panel-heading-a" role="tab">                                           
                     <h4 class="panel-title customAccordionsText" id="hCustomFilter3" runat="server" >Category</h4>
                                        </div>

        </a>
        <div id="Filter3" class="panel-collapse collapse" role="tabpanel"  aria-expanded="true">
            <div class="panel-body">
             <asp:repeater id="rptCustomFilter3" runat="server" onitemdatabound="rptCustomFilter3_ItemDataBound">
                <ItemTemplate>
                     <div class="filter_row">
                         <label class="customLabel">
                            <input id="chkCustomFilter3" ClientIdMode="static" class="chkCustomFilter3" runat="server" type="checkbox" value="">
                            <asp:Literal id="ltrCustomFilterName3" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
                </div>
        </div>
    </div>
    <!--END FILTER 1 -->
    <!--FILTER 2 -->
    <div class="panel customPanel panel-default"  id="dvCustomFilter4" runat="server">
        <a  role="button"  class="collapsed" data-toggle="collapse" data-target="#Filter4" aria-expanded="true">

            <div class="panel-heading customAccordions panel-heading-a" role="tab">                                           
                     <h4 class="panel-title customAccordionsText" id="hCustomFilter4" runat="server" >Category</h4>
                                        </div>
        </a>
        <div id="Filter4" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">
            <asp:repeater id="rptCustomFilter4" runat="server" onitemdatabound="rptCustomFilter4_ItemDataBound">
                <ItemTemplate>
                     <div class="filter_row">
                         <label class="customLabel">
                            <input id="chkCustomFilter4" ClientIdMode="static" class="chkCustomFilter4" runat="server" type="checkbox" value="">
                            <asp:Literal id="ltrCustomFilterName4" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
        </div>
    </div>
    <!--FILTER 2 -->
    <!--FILTER 3 -->
    <div class="panel customPanel panel-default"  id="dvCustomFilter5" runat="server">
        <a  runat="server" role="button"  class="collapsed" data-toggle="collapse" data-target="#Filter5" aria-expanded="true">
            <div class="panel-heading customAccordions panel-heading-a" role="tab">                                           
                     <h4 class="panel-title customAccordionsText" id="hCustomFilter5" runat="server" >Category</h4>
                                        </div>
        </a>
        <div id="Filter5" class="panel-collapse collapse" role="tabpanel" aria-expanded="true">
            <asp:repeater id="rptCustomFilter5" runat="server" onitemdatabound="rptCustomFilter5_ItemDataBound">
                <ItemTemplate>
                     <div class="filter_row">
                         <label class="customLabel">
                            <input id="chkCustomFilter5" ClientIdMode="static" class="chkCustomFilter5" runat="server" type="checkbox" value="">
                            <asp:Literal id="ltrCustomFilterName5" runat="server"></asp:Literal>                           
                        </label>
                    </div>
                </ItemTemplate>
        </asp:repeater>
        </div>
    </div>
    <!--FILTER 3 -->
</div>
