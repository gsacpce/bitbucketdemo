﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Products_SaveImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                   <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Right click and save as"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 10px;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <img id="imgSaveImage" runat="Server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
