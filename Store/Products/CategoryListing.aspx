﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Products_CategoryListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divMainContent">
        <!-- Page Category Image-->
        <div class="container categoryImage">
            <img class="img-responsive" id="imgCategoryBanner" runat="server">
        </div>
        <!--END Page Category Image-->
        <!-- Page Category heading + Content-->
        <section id="CATEGORY_LISTING" class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="pageTitle">
                        <asp:Label ID="lblHeader" runat="server"></asp:Label></h3>
                </div>
                <div class="col-xs-12">
                    <h2 class="pageText">
                        <asp:Literal ID="lblDescription" runat="server"></asp:Literal></h2>
                </div>
            </div>
            <div class="row grid" id="divSubCategory">
                <ul>
                    <asp:Repeater ID="rptSubCategory" runat="server">
                        <ItemTemplate>
                            <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center">
                                <div class="prod_list_cell">
                                    <a id="hrfSubCategory" class="prod_list_image_outer" runat="server">
                                        <asp:Image CssClass="prod_list_image_inner" ID="imgSubCategory" runat="server" /></a>
                                    <div class="prod_list_text_outer">
                                        <p class=" prod_list_text_title productSmlName"><%#DataBinder.Eval(Container.DataItem, "CategoryName")%></p>
                                    </div>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>

        </section>
        <!-- END Page Category heading + Content-->
    </div>
<!-- Page CSS & JS --> 
<!--<link href="<%=host %>CSS/CategoryListing.css" rel="stylesheet"/>-->
<script src="<%=host %>JS/CategoryListing.js"></script>
<!-- END Page CSS & JS --> 
</asp:Content>


