﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" Inherits="Presentation.Products_ProductListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" ClientIDMode="Static">
    <asp:Literal ID="ltrJS" runat="server"></asp:Literal>
    <script type="text/javascript">
        var host = "<%=host %>";
        var SectionName = "<%= strSectionName%>";
        var CategoryName = "<%= strCategoryName%>";
        var SubCategoryName = "<%= strSubCategoryName%>";
        var SubSubCategoryName = "<%= strSubSubCategoryName%>";
        var PageSize = "<%= intPageSize%>";
        var blnDefaultListView = "<%=blnDefaultListView %>";
        var intTotalPages = 0;
        var defaultSortName = "<%=strSortName %>";
        var SortName = "<%=strSortName %>";
        var IsInfiniteScroll = "<%=blnIsInfiniteScroll %>";
        var IsGoogleMixIt = "<%=blnIsGoogleMixIt %>";
        var MiniumPrice = 0;
        var MaximumPrice = 0;
        var showfilters = "<%=blnShowFilters %>";
        var LanguageId = "<%=intLanguageId %>";
        var CurrencyId = "<%=intCurrencyId %>";
        var CurrencySymbol = "<%=strCurrencySymbol %>";
        var MinimumProductMsg = "<%= strCPMinimumProductMsg%>";
        var MaximumProductMsg = "<%=strCPMaximumProductMsg %>";
        var ProductComparison = "<%= strProductComparison%>";
        var SearchProductIds = "<%= strSearchProductIds%>";
        var SearchKeyword = "<%=strSearchKeyword %>";
        var NoResultFoundMessage = "<%=strNoResultFoundMessage %>";
        var ResultsFoundMessage = "<%=strResultsFoundMessage %>";
        var NoResultFoundMessage1 = "<%=strNoResultFoundMessage1 %>";
    </script>
    <div id="divMainContent">
        <section id="PRODUCT_LIST_PAGE" class="container">
            <!--Category Image-->
            <div class="row">
                <div class="col-xs-12 text-center">
                   <a href="" id="hrefBannerLink" runat="server"><img class="img-responsive" id="imgCategoryBanner"  src="" runat="server"></a>
                </div>
            </div>
            <!--END Category Image-->
            <!--Product Listing Container-->
            <%--<div class="row">
                    <div class="col-xs-12">
                        <h2 class="pageTitle" id="hCategoryTitle" runat="server"></h2>
                    </div>
                </div>--%>
            <div class="row">
                <!--Listing Filter Settings panel-->
                <div class="col-xs-12 col-sm-12 col-md-3" id="dvFilterContainer" runat="server">
                    <!-- FILTER COLUMN -->
                    <div class="hidden-xs">
                        <div id="DESKTOP_FILTER_POSITION">
                            <div id="STANDARD_FILTER">
                                <div class="filter_title" style="display: none;">
                                    <p id="pfilterTitle" runat="server" class="pageText">
                                    </p>
                                </div>
                                <p id="pfilterDesc" runat="server" class="pageText">
                                </p>
                                <div class="filter_reset"><a id="resetFilters" runat="server" class="btn btn-sm btn-default customBtn">Reset filters</a></div>
                                <div class="filter_outer">
                                    <div id="dvFilterData"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END Listing Filter Settings panel-->
                <!--Listing Product panel-->
                <div class="col-xs-12 col-sm-12 col-md-9 col-xl-9" id="dvProductContainer" runat="server">
                    <!--Category Description panel-->
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="no-margin pageText">
                                <asp:Literal ID="ltrCategoryDescription" runat="server"></asp:Literal>
                            </p>
                        </div>
                    </div>
                    <!--END Category Description panel-->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="container well" id="dvSearchMessage" runat="server"></div>
                        </div>
                    </div>
                    <!--Sorting and Pagination-->

                    <div id="SortingDataContainer" class="row listingSortContainer">
                        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 FilterSections" id="dvSectionIconContainer" runat="server" visible="false"></div>
                        <!--Total Items-->
                        <div class="col-md-12  col-sm-12 col-xs-12 hidden">
                            <asp:Literal ID="ltrTotalItems" runat="server"></asp:Literal>: <span id="totalitems" class="listingTotalItems"></span>
                        </div>
                        <!--Sort By-->
                        <!--Pagination-->
                        <div id="dvPagination" class="PaginationContainer col-xs-12 col-sm-12 col-md-7 col-lg-7 row product_list_controls">
                            <div class="col-xs-7 col-sm-6 col-md-5 col-lg-5">
                                <ul class="pagination pagination-sm">
                                    <li><a id="aFirst" href="javascript:void(0);" class="customPagination"><span aria-hidden="true">««</span></a></li>
                                    <li><a id="aPrev" href="javascript:void(0);" class="customPagination">«</a></li>
                                    <li><a id="aNext" href="javascript:void(0);" class="customPagination">»</a></li>
                                    <li><a id="aLast" href="javascript:void(0);" class="customPagination"><span aria-hidden="true">»»</span></a></li>
                                </ul>
                            </div>
                            <div class="col-xs-5 col-sm-6 col-md-7 col-lg-7">
                                <span class="productRecord pageSmlText  hidden-xs " id="records"></span>
                                <a href="javascript:void(0);" style="margin: 0 8px;" id="aViewAll" onclick="javascript:return ViewAllProducts();" class="btn btn-sm btn-primary list_button customActionBtn">
                                    <asp:Literal ID="ltrViewAll" runat="server"></asp:Literal>
                                </a>
                                <span id="dvPageNo" class="hidden-xs">
                                    <span class="pageSmlText">
                                        <asp:Literal ID="ltrPages" runat="server"></asp:Literal>
                                    </span>
                                    <span>
                                        <input type="text" id="currentpage" onchange="JumpToPage();" style="width: 20px;" maxlength="2">
                                    </span>
                                    <asp:Literal ID="ltrOf" runat="server"></asp:Literal>
                                    <span id="spnTotalPageCount"></span>
                                </span>
                            </div>
                        </div>

                        <!--Compare Button-->
                        <div id="compareBtn" class="col-xs-5 col-sm-3 col-md-3 col-lg-2 product_list_controls" style="display: block;" runat="server" visible="false">
                            <input id="btnCompare" runat="server" type="button" class="btn btn-sm btn-primary list_button customActionBtn">
                        </div>
                        <div class="col-xs-12 col-sm-12 pull-left hidden-md hidden-lg">
                            <asp:Label ID="lblMobileCategoryTitle" runat="server"></asp:Label><br />
                        </div>
                        <div class="col-xs-2 col-sm-2 pull-left hidden-sm hidden-md hidden-lg">
                            <a href="javascript:void(0);" id="aMobileFilter" class="btn btn-sm btn-primary">
                                <span class="glyphicon glyphicon-filter"></span>Filter
                            </a>
                        </div>
                        <!--END Compare Button-->
                        <!--Grid/List View-->
                        <div id="dvViewAsContainer" runat="server" class="col-xs-3 col-sm-1 col-md-1 col-lg-1 text-right product_list_controls list row" style="display: inline-block; padding: 0;">
                            <a id="alstView" runat="server" class="btn btn-sm btn-primary list_button customActionBtn"><span class="glyphicon glyphicon-align-justify"></span>&nbsp <%= strViewList %></a>
                            <a id="agrdView" runat="server" class="btn btn-sm btn-primary grid_button customActionBtn"><span class="glyphicon glyphicon-th-large"></span>&nbsp <% = strViewGrid %> </a>
                        </div>
                        <!--END  Grid/List View-->
                        <div id="dvSortingContainer" runat="server" class="listingSortBy col-xs-7 col-sm-3 col-md-3 col-lg-3 product_list_controls pull-right">
                            <select id="ddlSorting" class="form-control input-sm" runat="server" onchange="ChangeSorting(this);"></select>
                        </div>
                    </div>
                    <!--END Sorting and Pagination-->
                    <!--Product Listing Container-->
                    <div class="ProductListingContainer container">
                        <ul id="dvProductData" runat="server" class="row grid"></ul>
                    </div>
                    <div id="productsRandom" class="row grid">
                        <ul role="tablist" class="nav nav-tabs">
                            <!-- Nav tabs -->

                            <li class="active" role="presentation" id="ContentPlaceHolder1_liRelatedPrice">
                                <a role="tab" aria-controls="profile">popular items
                                </a>
                            </li>
                        </ul>
                        <asp:Repeater ID="rptRandomProducts" runat="server" OnItemDataBound="rptRandomProducts_ItemDataBound">
                            <ItemTemplate>

                                <a id="aProductImage" runat="server" href="javascript:void(0);">
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="prod_list_cell">
                                            <div class="prod_list_image_outer">
                                                <img class="prod_list_image_inner" src="" id="imgProduct" runat="server">
                                            </div>
                                            <div class="prod_list_text_outer">
                                                <div class="prod_list_text_title productSmlName" id="aProductName" runat="server"></div>
                                                <div class="prod_list_text_code productCode">
                                                    <asp:Literal ID="ltrProductCode" runat="server"></asp:Literal>
                                                </div>
                                                <div id="spnPrice" class="prod_list_text_price productPrice" runat="server"></div>
                                                <div class="prod_list_text_desc productDescription" id="dvDescription" runat="server"></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <!--END Product Listing Container-->
                    <!--Listing Pagination panel-->
                </div>
                <!--END Listing Product panel-->
            </div>
            <!--END Product Listing Container-->
        </section>
    </div>
    <input type="hidden" id="hidPageNo" value="1">
    <input type="hidden" id="hidColorIds" value="">
    <input type="hidden" id="hidSectionIds" value="">
    <input type="hidden" id="hidCustomFilter1" value="">
    <input type="hidden" id="hidCustomFilter2" value="">
    <input type="hidden" id="hidCustomFilter3" value="">
    <input type="hidden" id="hidCustomFilter4" value="">
    <input type="hidden" id="hidCustomFilter5" value="">
    <input type="hidden" id="hidCompareProducts" value="">
    <input type="hidden" id="hdcurview" value="" />

    <script src="<%=host %>js/ProductListing.js"></script>
</asp:Content>
