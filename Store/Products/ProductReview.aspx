﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/BlankMaster.master" Inherits="Presentation.Products_ProductReview" %>

<asp:Content ID="content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="dvProductReview">
        <asp:Repeater ID="rptProductReview" runat="server" OnItemDataBound="rptProductReview_ItemDataBound">
            <ItemTemplate>
                <div class="product_review_item">
                    <div class="product_review_left">
                        <div class="product_review_title pageText">
                            <asp:Literal ID="ltrReviewTitle" runat="server" Text='<%# Eval("ReviewTitle") %>'></asp:Literal>
                        </div>
                        <div class="product_rating_stars">
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating1" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating2" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating3" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating4" runat="server"></span>
                            <span class="glyphicon glyphicon glyphicon glyphicon-star-empty" aria-hidden="true" id="spnRating5" runat="server"></span>&nbsp;
                        </div>
                    </div>
                    <div class="product_review_right pageSmlText">
                        <asp:Literal ID="ltrReview" runat="server" Text='<%# Eval("Review") %>'></asp:Literal>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <asp:HiddenField ID="hdnTotalReview" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnPageStartRowNumber" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnPageEndRowNumber" runat="server" ClientIDMode="Static" /> 
    <asp:HiddenField ID="hdnAvgRating" runat="server" ClientIDMode="Static" />
</asp:Content>
