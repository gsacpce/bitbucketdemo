<div id="divHeader" clientidmode="Static" runat="server">
            <section id="HEADER_STANDARD">
                <div class="header_inner">
                    <div class="row">
                        <div id="top_page" class="navbar navbar-default header_panel " role="navigation">
                            <!-- HEADER START  -->
                            <div class="container">
                                <!--Moved from Bottom level html element to maintain mobile device compatible  -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <div class="hidden-sm hidden-md hidden-lg" style="display: block; float: right; margin: 0 10px 0 0; position: relative;">
                                    <asp:label id="lblMDBasketCounter" cssclass="basket_qty_lg menuBg menuLink" style="display: inline-block; margin-top: 13px;" runat="server"></asp:label>
                                    <a style="float: right;" href="ShoppingCart">

                                        <asp:image id="imgMDBasket" runat="server" cssclass="img-responsive" height="50"></asp:image>
                                    </a>
                                    
                                </div>
                                <!--Moved -->
                                <div class="col-md-4 column col-sm-4 col-xs-7"><div class="navbar-header headerLinkMenu" style="margin-top: 10px;">
                                        <!--Moved to TOP level html element to maintain mobile device compatible -->
                                        <div id="gmcontent1" itemtype="SiteLogo" data-value="0" runat="server"><br><span style="color:#702828">SiteLogo</span></div>


                                    </div></div>
                                <div class="col-md-8 column col-sm-8 col-xs-12"><div class="navbar-collapse collapse header-collapse" id="gmcontent2" itemtype="SiteLinks" data-value="0" runat="server"><br><span style="color:#702828">SiteLinks</span></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="HEADER_CATEGORIES">

                <div class="row">
                    <div class="container">
                        <nav class="navbar navbar-inverse menuBg">
                            <div class="navbar-header">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <button data-target=".category-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="hidden-sm hidden-md hidden-lg" style="display: block; float: right; margin-right: 10px">
                                    <a data-toggle="modal" href="#ModalSearch">
                                        <asp:image id="aSearch" clientidmode="Static" runat="server" cssclass="img-responsive" height="50"></asp:image>
                                    </a>
                                </div>
                                <div class="category_title hidden-sm hidden-md hidden-lg menuLink">Product categories</div>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse category-collapse " style="clear: left">
                                <div class="col-md-11 col-xs-12 col-sm-11 column"><ul data-smartmenus-id="1454495588593174" class="nav navbar-nav navbar-left categoryMenu sm-collapsible" id="ulCategoriesMenu" runat="server" itemtype="CategoriesMenu" data-value="0"><br><span style="color:#702828">CategoriesMenu</span></ul></div>
                                <div class="col-md-1 col-xs-1 col-sm-1 column"><div class="hidden-xs" style="display: block; float: right; margin-right: 10px" id="gmcontent5" itemtype="SiteSearch" data-value="0" runat="server"><br><span style="color:#702828">SiteSearch</span></div></div>
                            </div>
                            <!-- /NAVBAR COLLAPSE -->
                        </nav>
                    </div>
                </div>
            </section>
            <asp:literal id="ltlBreadCrumb" runat="server"></asp:literal>
         </div> 