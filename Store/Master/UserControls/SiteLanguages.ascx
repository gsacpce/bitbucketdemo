﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Presentation.Master_UserControls_SiteLanguages" %>
<ul class="nav navbar-nav navbar-right nomargin">
    <li class="dropdown dropdown-toggle sitelink" >
        <asp:LinkButton ID="lnkLanguage" runat="server" data-toggle="dropdown" OnClientClick="return false;"  CssClass="headerLink" Text="<%# BindResourceData() %>">
            <%=strLanguageTitle %>
            <span class="caret leftMargin"></span>            
        </asp:LinkButton>
            

        <ul id="ulSiteLanguages" class="dropdown-menu " runat="server">
            <asp:Repeater ID="rptSiteLanguage" runat="server" OnItemDataBound="rptSiteLanguage_ItemDataBound">
                <ItemTemplate>
                    <li id="liSiteLanguage" class="dropdown sitelink" runat="server">
                        <asp:LinkButton ID="lnkSiteLanguage" runat="server"  ClientIDMode="Static"
                                        Text='<%# Eval("LanguageName") %>'
                                        CssClass="dropdown-toggle">
                    
                        </asp:LinkButton>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    
   </li>
</ul>



