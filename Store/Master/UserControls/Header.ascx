<%@ Control Language="C#" AutoEventWireup="true" Inherits="Presentation.Master_UserControl_Header" %>
<%@ Register Src="~/Master/UserControls/Menu.ascx" TagName="Menu" TagPrefix="uc" %>
<%@ Register Src="~/Master/UserControls/SiteLinks.ascx" TagName="SiteLink" TagPrefix="uc" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
<style>
    .brand_exxon {
    height: 43px;
    width: 194px;
}
    .brand_mobil1 {
    height: 43px;
    width: 139px;
}
.brand_mobil1 {
    transition: all 0.5s ease 0s;
}
.brand_delvac1 {
    height: 43px;
    width: 247px;
}
.brand_delvac1 {
    transition: all 0.5s ease 0s;
}
.brand_shc {
    height: 43px;
    width: 150px;
}
.brand_shc {
    transition: all 0.5s ease 0s;
}
/*.navbar-right {
    float: right !important;
    margin-right: -82px;
}*/
</style>

<header id="header">
     <asp:HiddenField ID="hdnstorename" runat="server"></asp:HiddenField>
     


    <div class="All_Header" id="ALL_HEADER">

        <div class="container collapse" id="COOKIES" >
            <div class="row">
                <div class="col-xs-12" style="text-align: center;">
                    <span class="cookies_title" id="spnHeadingCookie" runat="server"></span>
                    <asp:Literal ID="ltMsgCookie" runat="server"></asp:Literal>
                    <a class="btn btn-sm btn-primary" onclick="createCookie($('#ucHeader1_hdnstorename').val(),'true',1700);" href="javascript:void(0)">
                        <asp:Literal ID="ltAcceptCookie" runat="server"></asp:Literal>
                    </a>
                    <a class="btn btn-sm btn-primary" onclick="cookie_close();" href="javascript:void(0)">
                        <asp:Literal ID="ltCloseCookie" runat="server"></asp:Literal>
                    </a>
                </div>
            </div>
        </div>


        <%--ALL_HEADER Static id required for the fetching height in Quickshop--%>
        <div id="divHeader" runat="server" class="mycanvas">
            <section id="HEADER_STANDARD">
                <div class="header_inner">
                    <div class="row sitelinks">
                        <div id="top_page" class="navbar navbar-default header_panel " role="navigation">
                            <!-- HEADER START  -->
                            <div class="container">
                                <!--Moved from Bottom level html element to maintain mobile device compatible  -->
                                <button id="hamburger_button" runat="server" type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <div id="basket_counter" runat="server" class="hidden-md hidden-lg basket hidden-sm" style="float: right;position: relative;">
                                   <a class="basketmargin" style="float: right;" id="aMDBasket" runat="server">
                                     <asp:Label ID="lblMDBasketCounter" CssClass="basket_qty_lg menuBg menuLink" Style="display: inline-block; margin-top: 13px;" runat="server"></asp:Label>
                                    
                                       <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                       <%--<asp:Image ID="imgMDBasket" runat="server" CssClass="img-responsive" Height="50"></asp:Image>--%>
                                    </a>

                                </div>
                                <!--Moved -->
                                <div class="col-md-4 column col-sm-4 col-xs-6">
                                    <div class="navbar-header headerLinkMenu" >
                                        <!--Moved to TOP level html element to maintain mobile device compatible -->
                                        <div id="gmcontent1" itemtype="SiteLogo" data-value="0" runat="server">
                                            <br>
                                            <span style="color: #702828">SiteLogo</span>
                                        </div>


                                    </div>
                               <%--     <div id="ucHeader1_gmcontent1" itemtype="SiteLogo" data-value="0">

</div>--%>
                                </div>
                                <div class="col-md-8 column col-sm-12 col-xs-12">
                                    <div class="navbar-collapse collapse header-collapse" id="gmcontent2" itemtype="SiteLinks" data-value="0" runat="server">
                                        <br>
                                        <span style="color: #702828">SiteLinks</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="HEADER_CATEGORIES" runat="server">

                <div class="row">
                    <div class="container">
                        <nav class="navbar navbar-inverse menuBg">
                            <div class="navbar-header">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <button id="product_hamburger_button" runat="server" data-target=".category-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="hidden-sm hidden-md hidden-lg search" style="display: block; float: right; margin-right: 10px"    runat="server">
                                    <%--<a id='aSearch' class='headerLink' href='javascript:void(0);'><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <a data-toggle="modal" href="#Modaearch">lS--%>

                                      <div id="divmobilesearch" runat="server"></div>
                                      <%--<asp:Image ID="aSearch" ClientIDMode="Static" runat="server" CssClass="img-responsive" Height="50"></asp:Image>--%>
                                   
                                        
                                         <%--</a>--%>
                                </div>
                                <div id="categories" runat="server" class="category_title hidden-md hidden-lg menuLink hidden-sm">Product categories</div>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse category-collapse " style="clear: left">
                                <div class="col-md-11 col-xs-12 col-sm-11 column">
                                    <ul class="nav navbar-nav navbar-left categoryMenu categoryScroll" id="ulCategoriesMenu"  itemtype="CategoriesMenu" data-value="0" runat="server">
                                        <%--<br>
                                        <span style="color: #702828">
                                        </span>--%>
                                        <script type="text/javascript">
                                            $(document).ready(function () {

                                                $('.categoryMenu li').find('a').each(function () {
                                                    //debugger;
                                                    $(this).addClass("menuLink");

                                                });
                                            });
                                        </script>

                                        <%--<li><a class="menuLink" href="<%=host %>/Category/2016 Sale">2016 Sale</a></li>--%>
                                        <li class="dropdown">
                                            <a href="../#" class="dropdown-toggle menuLink" data-toggle="dropdown"><%--ExxonMobil<span class="caret"></span>--%>
                                                <%--<img class="brand_exxon" src="../../Images/CategoryMenu/logo_exxon.png" />--%>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <%--<li><a href="<%=host %>/SubCategory/ExxonMobil/Office" class="menuLink">Office</a></li>--%>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="../#" class="dropdown-toggle menuLink" data-toggle="dropdown"><%--Mobil 1<span class="caret"></span>--%>
                                                <%--<img class="brand_mobil1" src="../../Images/CategoryMenu/logo_Mobil1.png" />--%>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <%--<li><a href="<%=host %>/SubCategory/Mobil 1/Clothing" class="menuLink">Clothing</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil 1/Promotional" class="menuLink">Promotional</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil 1/Office" class="menuLink">Office</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil 1/Stickers" class="menuLink">Stickers</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil 1/Mobil 1 40 Years" class="menuLink">Mobil 1 40 Years</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil 1/Mobil 1 Workshop Program" class="menuLink">Mobil 1 Workshop Program</a></li>
                                          --%>  </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="../#" class="dropdown-toggle menuLink" data-toggle="dropdown"><%--Mobil Delvac 1<span class="caret"></span>--%>
                                               <%-- <img class="brand_delvac1" src="../../Images/CategoryMenu/logo_delvac1.png" />--%>
                                            </a>
                                            <ul class="dropdown-menu">
                                               <%-- <li><a href="<%=host %>/SubCategory/Mobil Delvac 1/Clothing" class="menuLink">Clothing</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil Delvac 1/Promotional" class="menuLink">Promotional</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil Delvac 1/Office" class="menuLink">Office</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil Delvac 1/Stickers" class="menuLink">Stickers</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil Delvac 1/Mobil Delvac 90 Years" class="menuLink">Mobil Delvac 90 Years</a></li>
                                        --%>    </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a href="../#" class="dropdown-toggle menuLink" data-toggle="dropdown"><%--Mobil SHC<span class="caret"></span>--%>
                                               <%-- <img class="brand_shc" src="../../Images/CategoryMenu/logo_shc.png" />--%>
                                            </a>
                                            <ul class="dropdown-menu">
                                               <%-- <li><a href="<%=host %>/SubCategory/Mobil SHC/Clothing" class="menuLink">Clothing</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil SHC/Promotional" class="menuLink">Promotional</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil SHC/Office" class="menuLink">Office</a></li>
                                                <li><a href="<%=host %>/SubCategory/Mobil SHC/Stickers" class="menuLink">Stickers</a></li>
                                           --%> </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-1 col-xs-1 col-sm-1 column searchBox">
                                    <div class="hidden-xs" style="display: block; float: right; margin-right: 10px" id="gmcontent5" itemtype="SiteSearch" data-value="0" runat="server">
                                        <br>
                                        <span style="color: #702828">SiteSearch</span>
                                    </div>
                                </div>
                            </div>
                            <!-- /NAVBAR COLLAPSE -->
                        </nav>
                    </div>
                </div>
            </section>
            <asp:literal id="ltlBreadCrumb" runat="server"></asp:literal>
        </div>
    </div>
    <script type="text/javascript">
        var UFname='<%=UFName%>'
        var ULname = '<%=ULname%>'
        var ULanguage = '<%=ULanguage%>'
        var UUserType = '<%=UUserType%>'
        var UCurrency = '<%=UCurrency%>'
        var URegistrationCountry = '<%=URegistrationCountry%>'
        var Upoints = '<%=UPoints%>'
      </script>
</header>
