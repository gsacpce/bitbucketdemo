﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Presentation.Master_UserControls_SiteLinks" %>

<script>
    $(document).ready(function () {        
        
        $('#myCategory li').addClass('dropdown-submenu');
      

        $('#ucHeader1_ctl01_ulHeaderLinks').prepend('<li class="doom dropdown sitelink "></li>');
       
        $('#myCategory_new .dropdown-toggle, #myCategory_new .dropdown-menu').prependTo('.doom');
        //$('#myCategory_new .dropdown-menu').prependTo('.doom');
       
        $('.categoryMenu li').find('a').each(function () {
            //debugger;
            $(this).addClass("menuLink");
        });
    });
</script>
<script>
    $('#ucHeader1_ctl01_ulHeaderLinks').append('<li class="siteLinkSearch dropdown sitelink "></li>');
    $('.searchBox').prependTo('.siteLinkSearch');
    </script>
<style>
    .BasketLink img {
        position: relative;
top: -4px;
    }
   .dropdown-submenu {position: relative;}
   .dropdown-submenu>.dropdown-menu {top: 0;left: 100%;margin-top: -6px;margin-left: -1px;-webkit-border-radius: 0 6px 6px 6px;-moz-border-radius: 0 6px 6px;border-radius: 0 6px 6px 6px;}
   .dropdown-submenu:hover>.dropdown-menu {display: block;}
   .dropdown-submenu:hover>a:after {border-left-color: #fff;}
   .dropdown-submenu.pull-left {float: none;}
   .dropdown-submenu.pull-left>.dropdown-menu {left: -100%;margin-left: 10px;-webkit-border-radius: 6px 0 6px 6px;-moz-border-radius: 6px 0 6px 6px;border-radius: 6px 0 6px 6px;}
    #myCategory{padding:10px;}
    .siteLinkSearch.dropdown.sitelink {
    margin: 0 0 0 34px;
}
</style>

<ul class="nav navbar-nav navbar-right ellipsis nomargin" id="ulHeaderLinks" runat="server">
    <asp:Repeater ID="rptHeaderLinks" runat="server" OnItemDataBound="rptHeaderLinks_ItemDataBound">
        <ItemTemplate>
            <li id="liSiteLink" runat="server">

                <%--  <asp:LinkButton ID="lnkSiteLink" runat="server" CssClass="headerLink">
                    <%--<img src="../../Images/UI/icon_basket.png" height="50" id="imgBasket" runat="server" visible="false" />&nbsp;
                </asp:LinkButton>--%>
                <a class="headerLink" runat="server" id="lnkSiteLink"></a>
                <div id="quickbasket" runat="server" visible="false" clientidmode="static" style="width: 370px; top: 44px; z-index: 1111">
                    <div class="panel customPanel panel-default">
                        <div class="customPanel panel-heading"><%=Basket_MiniBasketHead1%> (<span id="spnItemCount" runat="server"></span> <%=Basket_MiniBasketHead2%><a href="javascript:close_quickbasket()" type="button" class="close customClose">&times;</a></div>
                        <div class="panel-body" id="dv1" runat="server">
                            <div class="basketItemListContainer">
                                <asp:Repeater ID="rptViewBasket" runat="server" OnItemDataBound="rptViewBasket_ItemDataBound" OnItemCommand="rptViewBasket_ItemCommand">
                                    <ItemTemplate>
                                        <div class="quickbasket_2_row">
                                            <!-- PRODUCT 1 -->
                                            <div class="quickbasket_2_image">
                                                <img style="width: 100%;" src="~/Images/Products/default.jpg" id="img1" runat="server" class="image_bg" />
                                            </div>
                                            <div class="quickbasket_2_text page customTableText">
                                                <%# DataBinder.Eval(Container.DataItem, "SKU") %><br>
                                                <%# DataBinder.Eval(Container.DataItem, "ProductName") %>
                                            </div>
                                            <div class="quickbasket_2_plus_minus customTableText text-center">
                                                <div class="input-group-sm ">
                                                    <asp:Label ID="lblProductQuantity" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="quickbasket_2_unit_price customTableText">
                                                <asp:Label ID="lblProductPrice" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Price") %>'></asp:Label>
                                            </div>
                                            <div class="quickbasket_2_line_total customTableText text-right">
                                                <asp:Label ID="lblTotalProductCost" runat="server"></asp:Label>
                                            </div>
                                            <div class="quickbasket_2_remove pull-left">

                                                <%--<asp:ImageButton ID="btnRemoveItem" CssClass="glyphicon glyphicon-remove customClose" runat="server" ImageUrl="/Images/cross.png" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShoppingCartProductId") %>' CommandName="DeleteItem" />--%>
                                                <asp:LinkButton ID="btnRemoveItem" CssClass="glyphicon glyphicon-remove customClose" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShoppingCartProductId") %>' CommandName="DeleteItem"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="divQBOuters">
                                <div class="quickbasket_totals_outer">
                                    <div class="quickbasket_totals_left customTableText"><%=Basket_MiniBasketTotalBeforeDelivery%></div>
                                    <div class="text-center customTableText">
                                        <asp:Label ID="lblTotalCheckoutAmount" runat="server"></asp:Label>
                                        <asp:Label  id="lblspnor"  runat="server"></asp:Label> 
                                        <asp:Label id="lblsPrice"  runat="server"></asp:Label> 
                                    </div>
                                </div>
                                <div class="quickbasket_totals_outer">
                                    <a class="btn btn-primary customActionBtn" id="aButton2" runat="server"><%=Basket_MiniBasketView%></a>
                                    <a class="btn btn-primary customActionBtn" id="aButton1" runat="server"><%=Basket_MiniBasketCheckout%></a>
                                </div>
                            </div>

                        </div>
                        <div class="panel-body" id="dv2" runat="server" visible="false">
                            <p class="pageText"><%=Basket_MiniBasketFillMeUp%></p>
                        </div>
                    </div>
                </div>

                <div id="quickbasket_new" runat="server" visible="false" clientidmode="static" style="width: 450px; top: 60px; z-index: 1111;position:absolute;left: -219px;">
                    <div class="panel customPanel panel-default">                        
                        <div class="panel-body" id="dv1_new" runat="server">
                            <div class="row">
                                <div class="col-xs-6">
                                    <h2>Basket</h2>
                                </div>
                                <div class="col-xs-6 text-right">
                                        <a class="btn btn-primary customActionBtn" id="aButton1_new" runat="server">
                                        <%=Basket_MiniBasketCheckout%></a>
                                 </div>
                             </div>
                            <div class="basketItemListContainer">
                                <div class="row">
                                    <asp:Label ID="PName" runat="server" CssClass="col-md-4" Text ="Product Name"></asp:Label>
                                    <asp:Label ID="PQuantity" runat="server" CssClass="col-md-4 text-right" Text ="Quantity"></asp:Label>
                                    <asp:Label ID="PPrice" runat="server" CssClass="col-md-4 text-right" Text ="Price"></asp:Label>
                                </div>
                                <asp:Repeater ID="rptViewBasket_new" runat="server" OnItemDataBound="rptViewBasket_new_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="quickbasket_2_row row">
                                            <!-- PRODUCT 1 -->                                            
                                            <div class="customTableText col-md-4">                                                
                                                <%# DataBinder.Eval(Container.DataItem, "ProductName") %>
                                            </div>
                                            <div class="customTableText col-md-4 text-right">
                                               <asp:Label ID="lblProductQuantity_new" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:Label>
                                            </div>                                            
                                            <div class="col-md-4 customTableText text-right">
                                                <asp:Label ID="lblTotalProductCost_new" runat="server"></asp:Label>
                                            </div>                                            
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="divQBOuters">
                                <div class="quickbasket_totals_outer">
                                    <div class="quickbasket_totals_left customTableText">
                                        <%=Basket_MiniBasketTotalBeforeDelivery%>
                                    </div>
                                    <div class="text-center customTableText">
                                        <asp:Label ID="lblTotalCheckoutAmount_new" runat="server"></asp:Label>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="panel-body" id="dv2_new" runat="server" visible="false">
                            <p class="pageText">
                                <%=Basket_MiniBasketFillMeUp%>
                            </p>
                        </div>
                    </div>
                </div>

                <%--<span class="ui-li-count ui-btn-corner-all countBubl" id="spnCounter" runat="server" visible="false">
                    <asp:Literal Visible="false" ID="ltrCounter" runat="server"></asp:Literal></span>--%>
                <ul class="nav navbar-nav navbar-right " id="ulChildSiteLinks" visible="false" runat="server">
                    <asp:Repeater ID="rptChildSiteLinks" runat="server" OnItemDataBound="rptChildSiteLinks_ItemDataBound">
                        <ItemTemplate>
                            <li id="liChildSiteLink" runat="server">
                                <%--<asp:LinkButton ID="lnkChildSiteLink" runat="server"></asp:LinkButton>--%>
                                <a id="lnkChildSiteLink" runat="server"></a>
                                <ul class="nav navbar-nav navbar-right" id="ulGrandChildSiteLinks" runat="server">
                                    <asp:Repeater ID="rptGrandChildSiteLinks" runat="server" OnItemDataBound="rptGrandChildSiteLinks_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <%-- <asp:LinkButton ID="lnkGrandChildSiteLink" runat="server"></asp:LinkButton>--%>
                                                <a id="lnkGrandChildSiteLink" runat="server"></a>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>

                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>

<ul class="" id="ulFooterLinks" runat="server">
    <asp:Repeater ID="rptFooterLinks" runat="server" OnItemDataBound="rptFooterLinks_ItemDataBound">
        <ItemTemplate>
            <li id="liSiteLink" runat="server">
                <%--<asp:LinkButton CssClass="footerLink" ID="lnkSiteLink" runat="server"></asp:LinkButton>--%>
                <a id="lnkSiteLink" class="footerLink" runat="server"></a>
                <%--<span class="" id="spnCounter" runat="server" visible="false">
                    <asp:Literal Visible="false" ID="ltrCounter" runat="server"></asp:Literal></span>
                    <ul class="" id="ulChildSiteLinks" runat="server">
                    <asp:Repeater ID="rptChildSiteLinks" runat="server" OnItemDataBound="rptChildSiteLinks_ItemDataBound">
                        <ItemTemplate>
                            <li id="liChildSiteLink" runat="server">
                                <asp:LinkButton ID="lnkChildSiteLink" runat="server"></asp:LinkButton>
                                <ul class="" id="ulGrandChildSiteLinks" runat="server">
                                    <asp:Repeater ID="rptGrandChildSiteLinks" runat="server" OnItemDataBound="rptGrandChildSiteLinks_ItemDataBound">
                                        <ItemTemplate>
                                            <li>
                                                <asp:LinkButton ID="lnkGrandChildSiteLink" runat="server">
                                                </asp:LinkButton>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>

                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>--%>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</ul>


<style>
    .countBubl {
        float: left;
        margin-top: -50px;
        margin-left: 60px;
        background: darkorange;
        color: #fff;
        padding: 2px;
    }
    .BasketLink:hover #quickbasket{display:block;}
</style>
<%--<script>
    $(".BasketLink").hover(
          function () {
              $("#quickbasket").show();
              var basketLeft = $(this).position();
              $("#quickbasket").css({
                  top: 40,
                  'z-index': '999999'
              });
              $(".navbar .nav li").css({
                  'z-index': 'inherit'
              });
          }, function () {
              $("#quickbasket").hide();
          });

    $(".shoppingcart").hover(
    function () {

    }, function () {
        $("#quickbasket").hide();
    }
  );

    $("body").hover(
   function () {
       $("#quickbasket").hide();
   }, function () {

   }
 );
    function close_quickbasket() {
        $("#quickbasket").mouseenter(function () {
            $("#quickbasket").show();
            $("#quickbasket").stop();
            $("#quickbasket").css("opacity","1");
        });
        $("#quickbasket").mouseleave(function () {
            $("#quickbasket").hide();
            $("#quickbasket").fadeOut(50);
        });
    }
</script>--%>
<%--<div style="background-image:url(../../Images/BT_group.jepg)"> 
        <img src="">
       <ul>
            <li style="position:absolute">
            </li>
            <li>
            </li>
            <li>
            </li>
       </ul>
   </div>--%>


