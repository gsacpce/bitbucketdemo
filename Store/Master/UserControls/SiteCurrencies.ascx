﻿<%@ Control Language="C#" AutoEventWireup="true"  Inherits="Presentation.Master_UserControls_SiteCurrencies" %>


<ul class="nav navbar-nav navbar-right sitelinks nomargin">
    <li class="dropdown sitelink dropdown-toggle" >
        <asp:LinkButton ID="lnkCurrency" runat="server" data-toggle="dropdown" OnClientClick="return false;"  CssClass="headerLink" Text="<%=strCurrencyTitle%>">
            <%=strCurrencyTitle %>
            <span class="caret leftMargin"></span>            
        </asp:LinkButton>
            

        <ul id="ulSiteLanguages" class="dropdown-menu sitelinks scroll" runat="server">
            <asp:Repeater ID="rptSiteCurrencies" runat="server"  OnItemDataBound="rptSiteCurrencies_ItemDataBound" ><%--OnItemCommand="rptSiteCurrencies_ItemCommand"--%>
                <ItemTemplate>
                    <li id="liSiteCurrency" class="dropdown  sitelink" runat="server">
                        <asp:HyperLink runat="server" id="lnkSiteCurrency" href="#" Text='<%# Eval("CurrencySymbol") + " - " + Eval("CurrencyName") %>' CSymb='<%# Eval("CurrencySymbol") %>' CId='<%# Eval("CurrencyId")%>'  CName=<%#Eval("CurrencyName") %>></asp:HyperLink>
                        <%--<asp:LinkButton runat="server" ID="lnkSiteCurrency" Text='<%# Eval("CurrencySymbol") + " - " + Eval("CurrencyName") %>'></asp:LinkButton>--%>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    
    </li>
</ul>

<script type="text/javascript">

    var CurrencyId, CurrencySymbol , LanguageId;
   

    function funCurrencyChange(CId,CSymb,LId)
    {
        //debugger;
        CurrencyId = CId;
        CurrencySymbol = CSymb;
        LanguageId = LId;
        /*
         data: {
                        CId: CId,
                        CCode: CCode
                    },
        data: {
                    CId: CurrencyId,
                    CCode: CurrencyCode
                },
        
       
        */
       <% if ( Session["User"] == null && PCount > 0 )
          {
       %>
        $('#myConfirmModal').find('#ConfirmationMessage').html('<%=strCurrencyChangeMessage%>');
        //$('#myConfirmModal').attr('rel', 'addbasket');
        $('#myConfirmModal').modal('show');
        //<%
          }  
          else
            {
        %>      
                $.ajax({
                    url: '<%=host%>home/home.aspx/ChangeDropdownCurrency',
                    data: "{'CId': '" + CId + "','CSymb' : '" + CSymb + "','LId' : '" + LId + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    async: false
                }).done(function (data) {
                    window.location.href = '<%=host%>Index';
                });
                
         <% }
            
        %>
    }

    

    $(document).on('click', '#btnConfirmYes', function () {
        //debugger;
            $.ajax({
                url: '<%=host%>home/home.aspx/ChangeDropdownCurrency',
                data: "{'CId': '" + CurrencyId + "','CSymb' : '" + CurrencySymbol + "','LId' : '" + LanguageId + "' }",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: false
            }).done(function (data) {               
                window.location.href = '<%=host%>Index';
            });
       
    });

    $(document).on('click', '#btnConfirmNo', function () {
        $('#myConfirmModal').hide();
       
    });

    $(document).on('click', '.loginlink', function () {
        //debugger;
        $.ajax({
            url: '<%=host%>home/home.aspx/LoginLinkClicked',
                data: "",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                cache: false,
                async: false
            }).done(function (data) {
                window.location.href = '<%=host%>Index';
            });

    });


</script>