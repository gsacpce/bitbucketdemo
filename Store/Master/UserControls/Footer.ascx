<%@ Control Language="C#" AutoEventWireup="true" Inherits="Presentation.Master_UserControls_Footer" %>

<div id="divFooter" runat="server">
    <!-- FOOTER ------------------------------------------------------------------------------------------------------------------------------------------------ -->
    <section id="STANDARD_FOOTER" class="mycanvas container-fluid">
        <div class="container">
            <footer class="footerBg footerpadding">
                <div class="row">
                        <div class="column col-md-2 col-sm-2 col-xs-2 " itemtype="Footerlogo" data-value="0" id="gmcontent4" runat="server">
                            <br>
                        </div>
                        <div class="column col-md-2 col-sm-2 col-xs-2 " itemtype="Footertext" data-value="0" id="gmcontent5" runat="server">
                            <br>
                        </div>
                        <div class="column col-md-2 col-sm-2 col-xs-2 copyright" itemtype="Footercopyright" data-value="0" id="gmcontent6" runat="server">
                            <br>
                        </div>

                        <div class="col-sm-12 col-xs-12 column   col-md-4 " itemtype="Footerlinks" data-value="0" id="gmcontent3" runat="server">
                            <br>
                        </div>

                    </div>
            </footer>
        </div>
    </section>
    <!-- FOOTER END -------------------------------------------------------------------------------------------------------------------------------------------- -->
          </div>
<%--<script>
    $(function () {
        var newH = $('.mycanvas').innerHeight();
        $('.maincontainer').css('padding-top', newH);
    });
</script>--%>
