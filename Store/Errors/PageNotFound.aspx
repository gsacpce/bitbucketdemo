﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" 
    Inherits="Presentation.Errors_PageNotFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- PAGE CONTENT ------------------------------------------------------------------------------------------------------------------------------------------ -->
   <section id="ERROR_PAGE">
        <!-- STATIC TEXT -->
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="pageTitle"><%=Errorpage_page_Title%></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <h3 class="pageSubTitle"><%=Errorpage_pageSubTitle%></h3>
                    <p class="pageText"><%=Errorpage_pageText1%></p>
                    <p class="pageText"><%=Errorpage_pageText2%></p>
                    <p class="pageText"><%=Errorpage_pageText3%></p>
                    <p class="pageText"><%=Errorpage_pageText4%></p>
                    <h3 class="pageSubTitle"><%=Errorpage_pageSubTitle2%></h3>
                    <p class="pageText"><%=Errorpage_pageSubTitle5%></p>
                    <p class="pageText"><%=Errorpage_pageText6%><a href="<%=host %>index"><span class="static_link hyperLink"><%=Errorpage_pageText10%></span></a></p>
                    <p class="pageText"><%=Errorpage_pageText7%> <a href="<%=host %>sitemap"><span class="static_link hyperLink"><%=Errorpage_pageText11%></span></a> <%=Errorpage_pageText12%> </p>
                    <h3 class="pageSubTitle"><%=Errorpage_pageText8%></h3>
                    <p class="pageText"><%=Errorpage_pageText9%> <a data-toggle="modal" href="#ModalContactUs"><span class="static_link hyperLink"><%=ContactUs_Contact_Us_Title%></span></a><%=Errorpage_pageText13%></p>
                </div>
                <!-- COLUMN END -->
            </div>
            <!-- ROW END -->
        </div>
        <!-- CONTAINER END -->
    </section>
    <!-- PAGE CONTENT ENDS ------------------------------------------------------------------------------------------------------------------------------------- -->
</asp:Content>

