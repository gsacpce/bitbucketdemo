﻿using PWGlobalEcomm.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PWGlobalEcomm.BusinessLogic
{
    public class UserTypesBL
    {
        public static int ManageUserTypes_SAED(UserTypesBE objUserTypesBE, Dictionary<string, string> dictParams, DBAction action)
        {
            Int16  i= 0;
            bool bres = false;

            Dictionary<string, string> dictReturn = new Dictionary<string, string>();
            dictReturn.Add("return", Convert.ToString(typeof(Int16)));

            //Dictionary<string, string> dictParams = new Dictionary<string, string>();
            //dictParams.Add("Name", objUserTypesBE.Name);
            //dictParams.Add("CreatedBy", objUserTypesBE.objUserTypeDetails.CreatedBy.ToString());
            //dictParams.Add("ModifiedBy", objUserTypesBE.objUserTypeDetails.ModifiedBy.ToString());
            //dictParams.Add("UserTypeID", objUserTypesBE.objUserTypeDetails.UserTypeID.ToString());
            dictParams.Add("Action", Convert.ToInt16(action).ToString());
            try
            {
                switch (action)
                {
                    case DBAction.Insert:
                        {
                            CommonDA.Insert(Constants.USP_ManageUserType_SAED, dictParams, ref dictReturn, true);
                            foreach (KeyValuePair<string, string> item in dictReturn)
                            {
                                if (item.Key == "return")
                                    i = Convert.ToInt16(item.Value);
                            }
                            break;
                        }
                    case DBAction.Update:
                        {
                            CommonDA.Update(Constants.USP_ManageUserType_SAED, dictParams, ref dictReturn, true);
                            foreach (KeyValuePair<string, string> item in dictReturn)
                            {
                                if (item.Key == "return")
                                    i = Convert.ToInt16(item.Value);
                            }
                            break;
                            //bres = CommonDA.Update(Constants.USP_ManageUserType_SAED, dictParams, true);
                            //if (bres)
                            //{
                            //    i = 1;
                            //}
                            //break;
                        }
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return i;
        }
        public static UserTypesBE getCollectionItem(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypesBE objuserType = new UserTypesBE();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("Action", Convert.ToInt16(DBAction.Select).ToString());

            try
            {
                if (HttpContext.Current.Cache["UserTypesBE"] == null)
                {
                    objuserType = UserTypesDA.getCollectionItem(spName, dic, true);
                    HttpContext.Current.Cache["UserTypesBE"] = objuserType;
                }
                else
                { objuserType = (UserTypesBE)HttpContext.Current.Cache["UserTypesBE"]; }
                return objuserType;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<UserTypesDetailsBE> GetAllUserTypeDetailsList(string sp_Name, int LanguageID)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageID", Convert.ToString(LanguageID));
            return UserTypesDA.GetAllUserTypeDetailsList(sp_Name, dictionaryInstance, true);
        }

        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   22 June 2016
        /// Scope   :   TO get all unique User Types
        /// </summary>
        /// <param name="spname"></param>
        /// <returns> List of all unique User Type Name </returns>

        public static List<UserTypesDetailsBE> GetAlluserTypeNames(string sp_Name)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("LanguageID", "1");
            return UserTypesDA.GetAlluserTypeNames(sp_Name, dictionaryInstance, true);
        }

        public static UserTypesDetailsBE GetAllUserTypeDetailsByUserTypeID(string spname, string LanguageID, UserTypesBE.UserTypeCatalogue objUserTypeCatalogue)
        {
            Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
            dictionaryInstance.Add("UserTypeID", objUserTypeCatalogue.UserTypeID.ToString());
            dictionaryInstance.Add("LanguageID", LanguageID);
            return UserTypesDA.GetAllUserTypeDetailsByUserTypeID(spname, dictionaryInstance, true);
        }

        public static int UpdateUsertTypeByUserTypeID(string spName, Dictionary<string, string> param)
        {
            int iResult = 0;
            bool bResult = false;
            try
            {
                bResult = CommonDA.Update(spName, param, true);
                if (bResult)
                {
                    iResult = 1;
                }
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return iResult;
        }


        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 19/06/2016
        /// Scope   : Get UserType data of Emailid import and Export
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static List<T> GetListUserTypes<T>(UserTypesBE.CustomUserTypes objCustomUserTypes)
        {


            List<T> lstUser = new List<T>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(Convert.ToInt16(DBAction.Select)));
                dictionaryInstance.Add("UserTypeId", Convert.ToString(objCustomUserTypes.UserTypeID));
                dictionaryInstance.Add("Languageid", Convert.ToString(objCustomUserTypes.LanguageId));

                lstUser = CommonDA.getCollectionItem<T>(Constants.USP_ManageUserTypeEmailValidation, dictionaryInstance, true);
                return lstUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstUser = null;
                return lstUser;
            }
        }


        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 19/06/2016
        /// Scope   : Get UserType data of Emailid import and Export
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static List<T> GetAssignedPaymentOptionForUserType<T>(UserTypesBE.UserTypePaymentDetails objUserTypePaymentDetails)
        {


            List<T> lstUser = new List<T>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("Action", Convert.ToString(Convert.ToInt16(DBAction.Select)));
                dictionaryInstance.Add("Languageid", Convert.ToString(objUserTypePaymentDetails.Languageid));
                dictionaryInstance.Add("UserTypeID", Convert.ToString(objUserTypePaymentDetails.UserTypeID));

                lstUser = CommonDA.getCollectionItem<T>(Constants.USP_GetUserTypePaymentOption, dictionaryInstance, true);
                return lstUser;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                lstUser = null;
                return lstUser;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 19/06/2016
        /// Scope   : insert Custom Mapping Import Fields
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static bool InsertCustomMappingImportFields(DataTable dtUserTypeCustomMappingImport, Int16 Action, UserTypesBE.CustomUserTypes objCustomUserTypes)
        {

            try
            {
                if (Action == Convert.ToInt16(DBAction.Insert))
                {
                    Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                    dictionaryInstance.Add("UserTypeCustomMappingImportTableType", dtUserTypeCustomMappingImport);
                    return UserTypesDA.Insert(Constants.USP_InsertUserTypeCustomMappingImport, dictionaryInstance, true);
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }


        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 19/06/2016
        /// Scope   : insert User Type Email White /Black List Data
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static bool InsertEmailValidateImportList(DataTable dtUserTypeEmailWhiteBlackList, Int16 Action, UserTypesBE.CustomUserTypes objCustomUserTypes)
        {

            try
            {
                if (Action == Convert.ToInt16(DBAction.Insert))
                {
                    Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                    dictionaryInstance.Add("UserTypeEmailValidateImportTableType", dtUserTypeEmailWhiteBlackList);
                    return UserTypesDA.Insert(Constants.USP_InsertUserTypeEmailImportList, dictionaryInstance, true);
                }
                else if (Action == Convert.ToInt16(DBAction.Update))
                {
                    Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                    dictionaryInstance.Add("Action", Convert.ToString(Action));
                    dictionaryInstance.Add("CreatedBy", Convert.ToString(objCustomUserTypes.CreatedBy));
                    dictionaryInstance.Add("IsWhiteList", Convert.ToString(objCustomUserTypes.IsWhiteList));
                    dictionaryInstance.Add("UserTypeID", Convert.ToString(objCustomUserTypes.UserTypeID));
                    dictionaryInstance.Add("Languageid", Convert.ToString(objCustomUserTypes.LanguageId));
                    return UserTypesDA.Insert(Constants.USP_ManageUserTypeEmailValidation, dictionaryInstance, true);
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 19/06/2016
        /// Scope   : insert User Type Email White /Black List Data
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static bool AEDUserTypePaymentOption(DataTable UserTypePaymentOptionTableType)
        {

            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("UserTypePaymentOptionTableType", UserTypePaymentOptionTableType);
                return UserTypesDA.Insert(Constants.USP_AEDUserTypePaymentOption, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }
        public static UserTypeMappingBE getCollectionItemUserTypeMapping(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            UserTypeMappingBE objUserTypeMappingBE = new UserTypeMappingBE();
            Dictionary<string, string> dic = new Dictionary<string, string>();
            try
            {
                objUserTypeMappingBE = UserTypesDA.getCollectionItemUserTypeMapping(spName, dic, true);
                return objUserTypeMappingBE;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        public static List<UserTypeMappingBE.UserTypeMasterOptions> GetAllUserTypeMasterOptions(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return UserTypesDA.GetAllUserTypeMasterOptions(spName, param, IsStoreConnectionString);
        }
        public static bool UpdateUserTypeOptionMaster(string spname, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return CommonDA.Update(spname, param, true);
        }

        public static UserTypeCountryMaster GetAllUserTypeCountryMaster(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return UserTypesDA.GetAllUserTypeCountryMaster(spName, param, IsStoreConnectionString);
        }

        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 23 June 2016
        /// Scope   : insert User Type Custom Fields to Table
        /// </summary>        
        /// <param name="Filter Data Table"></param>
        /// <param name="Filter Type"></param>
        /// <returns>returns count of Table</returns>
        public static bool InsertUserTypeCustomFields(DataTable dtUserTypeCustomFields)
        {
            Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
            try
            {
                dictionaryInstance.Add("UserTypeCustomFields", dtUserTypeCustomFields);
                return UserDA.Insert("", dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        public static bool SetCountryMaster(DataTable dtSequence)
        {
            bool IsSequenceSet = false;
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("DataW", dtSequence);

                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));

                UserTypesDA.SetCountryMaster(Constants.USP_SetCountryMasterUserType, dictionaryInstance, ref SequenceInstance, true);
                IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSequenceSet;
        }

        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 24 June 2016
        /// Scope   : Get All custom field Data for Given RegistrationCustomFieldID
        /// </summary>        
        /// <param name="RegistrationCustomFieldID"></param>
        /// <returns>returns List<RegistrationCustomFieldBE></returns>
        /// 
        public static List<UserTypeCustomFieldDataValue> GetAllCustomFieldDataByFieldID(int RegistrationCustomFieldID)
        {
            List<UserTypeCustomFieldDataValue> lstlstCustomFieldData = new List<UserTypeCustomFieldDataValue>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("RegistrationCustomFieldID", Convert.ToString(RegistrationCustomFieldID));
                lstlstCustomFieldData = UserTypesDA.GetAllCustomFieldDataByFieldID(Constants.USP_GetAllCustomFieldDataByFieldID, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstlstCustomFieldData;
        }

        public static List<UserTypeCustomMapping> GetCustomDetailsValue(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return UserTypesDA.GetCustomDetailsValue(spName, param, true);
        }

        public static List<RegistrationFieldDataMasterValidationBE> GetListCustomByRegID(string spName, Dictionary<string, string> param, bool IsStoreConnectionString)
        {
            return UserTypesDA.GetListCustomByRegID(spName, param, IsStoreConnectionString);
        }

        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 27 June 2016
        /// Scope   : Update the Custom Fields for the given RegistrationFieldsConfigurationId
        /// </summary>        
        /// <param name="RegistrationCustomFieldID"></param>
        /// <returns>returns bool bresult</returns>

        public static bool UpdateUserTypeRegistrationCustomFields(Dictionary<string, string> param)
        {
            bool IsSequenceSet = false;
            try
            {
                //Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                //dictionaryInstance.Add("DataW", dtSequence);

                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));

                UserTypesDA.UpdateUserTypeRegistrationCustomFields(Constants.USP_UpdateUserTypeRegistrationCustomFields, param, ref SequenceInstance, true);
                IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSequenceSet;
        }

        // Code Commented by SHRIGANESH SINGH as this TEST Method is no longer Required
        //public static bool DATATYPETEST(DataTable dtsequence)
        //{
        //    bool IsSequenceSet = false;
        //    try
        //    {
        //        Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
        //        dictionaryInstance.Add("DataW", dtsequence);

        //        Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
        //        SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));

        //        UserTypesDA.DATATYPETEST("USP_DATATYPETEST", dictionaryInstance, ref SequenceInstance, true);
        //        IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.WriteExceptionLog(ex);
        //    }
        //    return IsSequenceSet;
        //}

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 23-07-16
        /// Scope   : to get Assigned Category to the User
        /// assign category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        public static List<CategoryBE> GetAssignedCategoryToUsers(int UserTypeId, string Action)
        {
            List<CategoryBE> Categories = new List<CategoryBE>();
            try
            {


                Dictionary<string, string> _Result = new Dictionary<string, string>();
                _Result.Add("return", Convert.ToString(typeof(Int16)));

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserTypeId", Convert.ToString(UserTypeId));
                dictionaryInstance.Add("Action", Action);
                // dictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                //dictionaryInstance.Add("ProductIds", CategoryIds);

                Categories = CommonDA.getCollectionItem<CategoryBE>(Constants.USP_ManageUserTypeCategories, dictionaryInstance, ref _Result, true);


            }
            catch (Exception ex)
            {
                Categories = null;
                Exceptions.WriteExceptionLog(ex);
            }
            return Categories;
        }

        /// <summary>
        /// Author  : Vikram Singh
        /// Date    : 23-07-16
        /// Scope   :  Assigned Category to the User
        /// assign category
        /// </summary>
        /// <param name="categoryTypeId"></param>
        /// <param name="LanguageId"></param>
        /// <param name="catalogueId"></param>
        /// <returns>category list object after getting category details</returns>
        public static int AddCategoriesToUserTypes(int UserTypeId, string Action, string CategoryIds, int UserId)
        {

            try
            {


                Dictionary<string, string> _Result = new Dictionary<string, string>();
                _Result.Add("return", Convert.ToString(typeof(Int16)));

                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserTypeId", Convert.ToString(UserTypeId));
                dictionaryInstance.Add("Action", Action);
                dictionaryInstance.Add("CreatedBy", Convert.ToString(UserId));
                dictionaryInstance.Add("CategoryIds", CategoryIds);

                UserDA.Insert(Constants.USP_ManageUserTypeCategories, dictionaryInstance, ref _Result, true);
                if (_Result["return"] != null)
                {
                    return Convert.ToInt16(_Result["return"]);
                }
                else
                {
                    return 0;
                }



            }
            catch (Exception ex)
            {
                return 0;
                Exceptions.WriteExceptionLog(ex);
            }

        }

        public static List<StoreAndUserTypeCatalogueDetailsBE> GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID(string sp_Name, Int16 UserTypeID)
        {
            List<StoreAndUserTypeCatalogueDetailsBE> lstStoreAndUserTypeCatalogueDetails = new List<StoreAndUserTypeCatalogueDetailsBE>();
            try
            {
                Dictionary<string, string> dictionaryInstance = new Dictionary<string, string>();
                dictionaryInstance.Add("UserTypeID", UserTypeID.ToString());
                lstStoreAndUserTypeCatalogueDetails = UserTypesDA.GetAllStoreAndUserTypeCatalogueDetailsByUserTypeID(sp_Name, dictionaryInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return lstStoreAndUserTypeCatalogueDetails;
        }

        public static bool UpdateUserTypeCatalogueDetails(DataTable dtUpdateUserTypeCatalogueDetails)
        {
             try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("UpdateUserTypeCatalogueTableType", dtUpdateUserTypeCatalogueDetails);

                Dictionary<string, string> UserTypeCatalogueDetailsUpdatedInstance = new Dictionary<string, string>();
                UserTypeCatalogueDetailsUpdatedInstance.Add("IsUserTypeCatalogueDetailsUpdated", Convert.ToString(typeof(bool)));

                UserTypesDA.UpdateUserTypeCatalogueDetails(Constants.USP_UpdateUserTypeCatalogues, dictionaryInstance, ref UserTypeCatalogueDetailsUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(UserTypeCatalogueDetailsUpdatedInstance["IsUserTypeCatalogueDetailsUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 08/07/2016
        /// Scope   : Update Default Custom Field Name
        /// </summary>        
        /// <param name="Stored Procedure Name"></param>
        /// <param name="Dictionary"></param>
        /// <returns>returns bool </returns>
        public static bool UpdateCustomFieldNames(Dictionary<string,string> DictionayInstance)
        {
            try
            {
                return UserTypesDA.UpdateCustomFieldNames(Constants.USP_UpdateCustomFieldNames, DictionayInstance, true);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }

        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 12/07/2016
        /// Scope   : Update User Type Sequence
        /// </summary>        
        /// <param name="DataTable"></param>
        /// <returns>returns bool </returns>
        public static bool UpdateUserTypeSequence(DataTable dtUpdateSequence)
        {
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("UpdateUserTypeSequenceTableType", dtUpdateSequence);

                Dictionary<string, string> UserTypeSequenceUpdatedInstance = new Dictionary<string, string>();
                UserTypeSequenceUpdatedInstance.Add("IsUserTypeSequenceUpdated", Convert.ToString(typeof(bool)));

                UserTypesDA.UpdateUserTypeSequence(Constants.USP_UpdateUserTypeSequences,dictionaryInstance, ref UserTypeSequenceUpdatedInstance, true);
                return Convert.ToBoolean(Convert.ToString(UserTypeSequenceUpdatedInstance["IsUserTypeSequenceUpdated"]));
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return false;
            }
        }
        /// <summary>
        /// Author  : SHRIGANESH SINGH
        /// Date    : 20/07/2016
        /// Scope   : To Get All Custom Field Validation Rule
        /// </summary>        
        /// <param name="RegistrationFieldsConfigurationId"></param>
        /// <returns>returns UserTypeMappingBE.RegistrationFieldDataMasterValidationBE </returns>

        public static UserTypeMappingBE.RegistrationFieldDataMasterValidationBE GetAllCustomFieldValidationRule(int RegistrationFieldsConfigurationId)
        {
            try
            {
                UserTypeMappingBE.RegistrationFieldDataMasterValidationBE objCustomFieldValidation = new UserTypeMappingBE.RegistrationFieldDataMasterValidationBE();

                Dictionary<string, string> DictionaryInstance = new Dictionary<string, string>();
                DictionaryInstance.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationFieldsConfigurationId));

                objCustomFieldValidation = UserTypesDA.GetAllCustomFieldValidationRule(Constants.USP_GetAllCustomFieldValidationRule, DictionaryInstance, true);
                return objCustomFieldValidation;
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
                return null;
            }
        }

        /// <summary>
        /// Author  :   SHRIGANESH SINGH
        /// Date    :   22 July 2016
        /// Scope   :   To Insert the custom field values against the selected RegistrationCustomFieldID if its IsIdField is set to true 
        /// </summary>
        /// <param name="dtSequence"></param>
        /// <param name="RegistrationCustomFieldID"></param>
        /// <returns>bool  IsSequenceSet</returns>
        public static bool InsertCustomFieldValues(DataTable dtSequence, int RegistrationCustomFieldID)
        {
            bool IsSequenceSet = false;
            try
            {
                Dictionary<string, DataTable> dictionaryInstance = new Dictionary<string, DataTable>();
                dictionaryInstance.Add("DataW", dtSequence);

                Dictionary<string, string> param = new Dictionary<string, string>();
                param.Add("RegistrationFieldsConfigurationId", Convert.ToString(RegistrationCustomFieldID));

                Dictionary<string, string> SequenceInstance = new Dictionary<string, string>();
                SequenceInstance.Add("IsSequenceSet", Convert.ToString(typeof(bool)));

                UserTypesDA.InsertCustomFieldValues(Constants.USP_InsertCustomFieldValues, param, dictionaryInstance, ref SequenceInstance, true);
                IsSequenceSet = Convert.ToBoolean(SequenceInstance["IsSequenceSet"]);
            }
            catch (Exception ex)
            {
                Exceptions.WriteExceptionLog(ex);
            }
            return IsSequenceSet;
        }
     }
}
