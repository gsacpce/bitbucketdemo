﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Punchout_Default1" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        Paste your POSR cXML here or use the one provided:
        <br />
        <asp:Button ID="btnSubmit" runat="server" Text="GO!" OnClick="btnSubmit_Click" />
        <br />
        <asp:TextBox ID="txtcXML" runat="server" TextMode="MultiLine" Rows="30" Columns="100"></asp:TextBox>
    </form>
</body>
</html>
