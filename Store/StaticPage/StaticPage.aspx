﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" EnableViewStateMac="false" ViewStateEncryptionMode="Never" ViewStateMode="Disabled" Inherits="Presentation.StaticPage_StaticPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- <script>
        $(document).ready(function(){
            var vs = document.getElementById("__VIEWSTATE");
            vs.parentNode.removeChild(vs);
        });
        
    </script>--%>
    <section class="WebSection">  
        <div class="container">	      
        <div class="row">
		    <div class="col-xs-12">
			    <h1 id="spnHeading" class="pageTitle" runat="server" ></h1>
		    </div>
	    </div>
        <div class="detailsarea">
            <asp:Image ID="imgBannerImage" runat="server" Visible="false" />
            <asp:Literal ID="ltrContent" runat="server"></asp:Literal>
        </div>
        </div>
    </section>
       
    
   

   

</asp:Content>

