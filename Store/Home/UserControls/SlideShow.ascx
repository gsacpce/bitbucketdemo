﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Presentation.Home_UserControls_SlideShow" %>



<script type="text/javascript">
    $(document).ready(function () {

        $('.imageanchor a').each(function () {
            //debugger;
            if ($(this).attr("href") == undefined) {
                $(this).css("cursor", "auto");
            }

        });


    });
</script>

<style type="text/css">
    .owl_carousel_text::before {
        background: blue none repeat scroll 0 0;
        bottom: 0;
        content: "";
        left: 0;
        opacity: 0.6;
        position: absolute;
        right: 0;
        top: 0;
        z-index: -1;
    }
</style>


<div id="BOOTSTRAP_CAROUSEL" clientidmode="static" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- BOOTSTRAP SLIDER starts -->
                <ol class="carousel-indicators">
                    <!-- Indicators -->
                    <%--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>--%>
                    <asp:Repeater ID="rptOwlIndicators" runat="server" OnItemDataBound="rptOwlIndicators_ItemDataBound">
                        <ItemTemplate>
                            <li id="liIndicator" runat="server" data-target="#carousel-example-generic"></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ol>
                <div id="divBootstrapImages" clientidmode="static" runat="server" class="carousel-inner" role="listbox">
                    <!-- Wrapper for slides -->

                    <%-- <div class="item active"><img src="images/bootstrap_slider_1.jpg" width="100%" alt="..."><!-- SLIDE 1 -->
                        <div class="carousel-caption">Slide 1 text</div>
                    </div>--%>
                    <asp:Repeater ID="rptBootStrapImages" runat="server" OnItemDataBound="rptBootStrapImages_ItemDataBound">
                        <ItemTemplate>
                            <div class="item imageanchor" id="divBootImg" clientidmode="static" runat="server">
                                <asp:HyperLink ID="hyplImageNavigateUrl" runat="server">
                                    <asp:Image ID="imgSlider" runat="server" ImageUrl="" Style="width: 100%" /><!-- SLIDE 1 -->
                                </asp:HyperLink>
                                <div class="carousel-caption">
                                    <asp:Label ID="txtOverlay" runat="server" Text=''></asp:Label>
                                </div>
                            </div>
                        </ItemTemplate>

                    </asp:Repeater>



                </div>
                <!-- Wrapper for slides ends -->

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <!-- Controls -->
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- BOOTSTRAP SLIDER ENDS -->
        </div>
        <!-- COLUMN END -->
    </div>
    <!-- ROW END -->
</div>

<div id="OWL_CAROUSEL" runat="server" clientidmode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div id="divOwlImages" clientidmode="Static" runat="server" class="owl-carousel owl-theme">
                <!-- OWL CAROUSEL START-->

                <%--<div class="item">
                	<a href="product.html">
                      <div class="owl_carousel_group">
                          	<div class="owl_carousel_image"><img src="images/owl_carousel_1.png" width="100%"  alt=""/></div>
                  			<div class="h1_features owl_carousel_text">Red over the head&nbsp;headphones</div>
                      </div>
                    </a>
                </div>--%>
                <asp:Repeater ID="rptOwlImages" runat="server" OnItemDataBound="rptOwlImages_ItemDataBound">
                    <ItemTemplate>
                        <div class="item imageanchor">
                            <%--<asp:HyperLink ID="hyplnkSlideImage" runat="server" >--%>
                            <div class="owl_carousel_group">
                                <div class="owl_carousel_image">
                                    <asp:HyperLink ID="hyplImageNavigateUrl" runat="server">
                                        <asp:Image ID="imgSlider" CssClass="owl_carousel_image_css" runat="server" ImageUrl='' />
                                    </asp:HyperLink>
                                </div>
                                <%--BELOW DIV IS ADDED BY SHRIGANESH 29 JAN 2016 TO DISPLAY OVERLAY AREA--%>
                                <div id="divOverlayText" runat="server" class="h1_features owl_carousel_text">
                                    <asp:Label ID="txtOverlay" runat="server" Text=''></asp:Label>
                                </div>
                            </div>
                            <%--</asp:HyperLink>--%>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
            <!-- OWL CAROUSEL END -->
        </div>
        <!-- COLUMN END -->
    </div>
    <!-- ROW END -->

</div>

<div id="NERVESLIDER_CAROUSEL" clientidmode="static" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="slider_outer">
                <!-- NERVESLIDER CAROUSEL START-->
                <div class="slider_inner" id="divNerveImages" clientidmode="static" runat="server">
                    <%--<img src="images/nerve_slider_1.jpg" width="100%" alt="" />--%>
                    <asp:Repeater runat="server" ID="rptNerveImages" OnItemDataBound="rptNerveImages_ItemDataBound">
                        <ItemTemplate>
                            <div class="imageanchor">
                                <asp:HyperLink ID="hyplImageNavigateUrl" runat="server">
                                    <asp:Image ID="imgSlider" runat="server" Style="width: 100%" ImageUrl='' />
                                </asp:HyperLink>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>

            <!-- NERVESLIDER CAROUSEL END -->
        </div>
        <!-- COLUMN END -->
    </div>
    <!-- ROW END -->
</div>

<asp:Literal ID="ltrlSliderScript" runat="server"></asp:Literal>
<script>
    $(function () {
        var newheight1 = $('.slider_inner img').innerHeight();
        var newheight2 = $('.slider_inner').innerHeight();
        if (newheight1 < newheight2) {
            $('.slider_inner').css('height', newheight1);
        }
        else {
            $('.slider_inner').css('height', newheight2);
        }
        $(window).resize(function () {
            setTimeout(function () {
                var newheight1 = $('.slider_inner img').innerHeight();
                var newheight2 = $('.slider_inner').innerHeight();
                if (newheight1 < newheight2) {
                    $('.slider_inner').css('height', newheight1);
                }
                else {
                    $('.slider_inner').css('height', newheight2);
                }
            }, 500);
        });
    });
</script>
