﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Home_UserControls_Section" %>

<div class="container">
    <div id="mds-tpl-02" style="border: 0px solid #000000;">
        <div id="productdata">
            <section  id="NEW_ADDITIONS" >
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                    <asp:Label ID="lblSectionName" CssClass="heading1 pageHeading2" runat="server"></asp:Label>
                        </div>
                </div>
            </div>
            <div id="products" class="row grid"><!-- THUMNAILS START -->
                <asp:Repeater ID="rptProducts" runat="server" OnItemDataBound="rptProducts_ItemDataBound">
                    <ItemTemplate>
                    
                            <a id="aProductImage" runat="server" href="javascript:void(0);">
                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    <div class="prod_list_cell">
                                      <div class="prod_list_image_outer">
                                        <img class="prod_list_image_inner" src="" id="imgProduct" runat="server">
                                      </div>
                                      <div class="prod_list_text_outer">
                                        <div class="prod_list_text_title productSmlName" id="aProductName" runat="server"></div>
                                        <div class="prod_list_text_code productCode"><asp:Literal ID="ltrProductCode" runat="server"></asp:Literal></div>
                                        <div class="row prod_list_text_price productPrice">
                                            <div id="spnPrice" class="prod_list_text_price productPrice" runat="server"></div>
                                             <div class="prod_list_text_price productPrice" id="spnor"  runat="server"></div> 
                                            <div class="prod_list_text_price productPrice" id="spnPrice1"  runat="server"></div> 
                                            <div id="dvSectionIcons" runat="server">
                        </div>  
										<div class="price_now" id="spnstrike" runat="server"></div>
										</div>
										 
                                        <div class="prod_list_text_desc productDescription" id="dvDescription" runat="server"></div>
                                      </div>
                                </div>               
                                </div>
                           </a>
                    </ItemTemplate>
                </asp:Repeater>
            </div><!-- THUMNAILS END -->
            <input type="hidden" class="totalrecords" id="hidTotalRecords" runat="server" />
                </section>
        </div>
    </div>
</div>