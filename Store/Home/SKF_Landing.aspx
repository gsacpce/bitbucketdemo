﻿<%@ Page Title=""  Language="C#" AutoEventWireup="true" Inherits="Home_SKF_Landing" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SKF Landing Page</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1, user-scalable=no" />
    <link href="https://www.brand-estore.com/SKF/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.brand-estore.com/SKF/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://www.brand-estore.com/SKF/css/bootstrap-theme.css" rel="stylesheet">
    <script src="https://www.brand-estore.com/SKF/js/bootstrap.min.js"></script>
    <style>
        .heading1 {
            font-weight: 300;
            text-align: center;
            font-size: 40px;
            margin-top: 30px;
            margin-bottom: 15px;
        }
        .heading2 {
            font-weight: 300;
            text-align: center;
            font-size: 24px;
            margin-bottom: 5px;
        }
        h3 {
            font-size: 1.3em;
            line-height: 130%;
            font-weight: 300;
            margin: 15px 0 10px 0;
            padding: 0;
            color: #006bb7;
        }
        .login_currency_outer {
            display: block;
            margin-bottom: 15px;
            background-color: #0f58d6;
            border: 2px solid #0f58d6;
        }
        .login_currenct_description {
            font-size: 16px;
            text-align: center;
            color: #FFFFFF;
            padding-bottom: 30px;
            padding-top: 30px;
            padding-left: 30px;
            padding-right: 30px;
        }
        .login_currency_outer:hover {
            background-color: #fff;
            border: 2px solid #0f58d6;
            color: #0f58d6;
        }
        .login_currenct_description:hover {color: #0f58d6;}
    </style>
</head>
<body>
    <div id="ALL_HEADER">
        <section id="HEADER_STANDARD">
            <div class="header_inner">

                <div id="top_page" class="navbar navbar-default header_panel" role="navigation">
                    <!-- HEADER START  -->
                    <div class="navbar-header">
                        <a href="home.html"><img class="logo" src="https://www.brand-estore.com/SKF/Images/SiteLogo/sitelogo.png" alt="Mobil eShop"></a>
                    </div>

                </div>

            </div>
        </section>
    </div>
    <section id="LOGIN_PAGE">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="heading1">Welcome</div>
                    <div class="heading2">To the SKF Shop</div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h3>Please select</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <a style="text-decoration:none" href="https://www.brand-estore.com/SKF">
                        <div class="login_currency_outer">
                            <div class="login_currenct_description">SKF Europe Enter</div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6">
                    <a style="text-decoration:none" href="#">
                        <div class="login_currency_outer">
                            <div class="login_currenct_description">SKF N. America Coming soon</div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6">
                    <a style="text-decoration:none" href="#">
                        <div class="login_currency_outer">
                            <div class="login_currenct_description">SKF China Coming soon</div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-md-6">
                    <a style="text-decoration:none" href="#">
                        <div class="login_currency_outer">
                            <div class="login_currenct_description">SKF India Coming soon</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</body>
</html>
