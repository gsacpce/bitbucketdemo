﻿<%@ Page Title=""  Language="C#" AutoEventWireup="true" Inherits="Maersk_Landing" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<link href='https://www.maerskbrandstore.com/css/bootstrap-theme.css' rel='stylesheet'>
	
  <script>
      $('#ddlBusinessUnit').click(function () {
          $('#landingPage').removeClass('contentDiv');
          $('#landingPage').addClass('contentDiv1');

      })
</script>  
<style>
   .navbar-header {font-size: 0;position: relative;margin: -8px 0 0 0;padding: 20px 0 2px 0;}
	body{margin:0;}
    /*h1, h2, h4, h3, h5 {font-family: Zetta Sans, Serif;font-weight:400;}*/
    .imgWidth{width: 181px;}
	a:focus, a:hover {text-decoration: none !important;}
    .paddingLogo{padding: 20px 15px;}
    .navbar-header{font-size: 0;position: relative;}
    .navbar-text.navbar-center{padding: 0;right: 0;text-align: center;vertical-align: middle;width: 80%;}
	.contentDiv{margin: 250px  0 0 0;color: #fff; text-align: center;}
	h2 {font-size: 28px !important;color:#fff;line-height: 130%;margin-top: 20px;padding: 0;}
	select { background: url('/home/arrow.png') #83c1dd  no-repeat 93% 50%; -moz-appearance: none; -webkit-appearance: none; appearance: none; color:#fff; font-family: "Zetta"; }
	h3{color:#fff;}
	body {font-size: 15px;line-height: 22px;color: #282828;font-weight: 400;-webkit-font-smoothing: antialiased;min-height: 100%;overflow-x: hidden;}
	#ddlBusinessUnit{cursor: pointer;color: white;border:1px solid #83c1dd;padding: 0 20px;height: 50px;line-height: 50px;text-transform: uppercase; position: relative;font-size: 13px;width: 270px;}
	.paddingLogo img {width: 128px;height: 29px;}
	@media all and (min-width:320px) and (max-width:767px)
	{
			.contentDiv{margin: 95px 0 0 0 !important;}
			#ddlBusinessUnit{width:247px;}
	}
</style>
</head>
<body style="background-color: #63b3d4;">
    <form id="form1" runat="server">
    <div class="col-xs-12">
        <nav class="navbar navbar-default navbar-fixed-bottom" style="background-color: #fff;
            width: 100%;">
                <div class="navbar-header">

                    <a class="navbar-brand paddingLogo" href="#">
                        <img class="img-responsive imgWidth" src="https://www.maerskbrandstore.com/Images/SiteLogo/sitelogo.png" /></a>
                      <div class="navbar-text navbar-center"><a href="#" class="navbar-link"><h4 class="text-uppercase"> maersk  Brand store</h4></a></div>
                </div>
              
                

            </nav>
        <div id="landingPage" class="col-xs-12 contentDiv">
            <h2>Login to access Maersk Brand Store</h2>
            <h3>Select your Business Unit</h3>
            <div style="margin-top: 25px;" class="col-xs-12 dropdown">                
                   <asp:DropDownList ID="ddlBusinessUnit" runat="server" class="col-xs-12 btn btn-default dropdown-toggle" AutoPostBack="true" style="font-size: 12px;" OnSelectedIndexChanged="ddlBusinessUnit_SelectedIndexChanged">
                    <asp:ListItem Text="Select Business Unit " Value="0"></asp:ListItem>                                        
                    <asp:ListItem Text="Maersk Group" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Maersk Container Industry" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Maersk Drilling" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Maersk FPS.Os" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Maersk H2S Safety Service" Value="5"></asp:ListItem>                                        
                    <asp:ListItem Text="Maersk Line" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Maersk Line, Limited" Value="7"></asp:ListItem>
                    <asp:ListItem Text="Maersk Oil" Value="8"></asp:ListItem>
                    <asp:ListItem Text="Maersk Supply Service" Value="9"></asp:ListItem>
                    <asp:ListItem Text="Maersk Tankers" Value="10"></asp:ListItem>                                        
                    <asp:ListItem Text="Maersk Training" Value="11"></asp:ListItem>
                    <asp:ListItem Text="A.P. Møller Holding A/S " Value="12"></asp:ListItem>
                    <asp:ListItem Text="APM Terminals" Value="13"></asp:ListItem>
                    <asp:ListItem Text="Damco" Value="14"></asp:ListItem>
                    <asp:ListItem Text="MCC Transport" Value="15"></asp:ListItem>                                        
                    <asp:ListItem Text="Mercosul Line" Value="16"></asp:ListItem>
                    <asp:ListItem Text="Safmarine" Value="17"></asp:ListItem>
                    <asp:ListItem Text="Seago Line" Value="18"></asp:ListItem>
                    <asp:ListItem Text="Sealand" Value="19"></asp:ListItem>
                    <asp:ListItem Text="Svitzer" Value="20"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
