﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.SiteMap_SiteMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section id="SITE_MAP_PAGE">
    <div class="container">
    
    
        <div class="row">
            <div class="col-xs-12">
                <h2 class="pageTitle"><%=SiteMap_Page_Heading%> </h2>
            </div>
        </div>


        <div class="row">
        <div id="dvSiteMapContent" runat="server"></div>
            <%--<div class="col-xs-12 col-sm-4">
                <h3 class="pageSubTitle">Products</h3>
                <ul>
                    <li><a class="smlHyperLink">Category 1</a></li>
                    <li><a class="smlHyperLink">Category 2</a></li>
                    <li><a class="smlHyperLink">Category 3</a></li>
                    <li><a class="smlHyperLink">Category 4</a></li>
                    <li><a class="smlHyperLink">Category 5</a></li>
                    <li><a class="smlHyperLink">Category 6</a></li>
                    <li><a class="smlHyperLink">Quickshop</a></li>
                </ul>
            </div>
            
            <div class="col-xs-12 col-sm-4">
                <h3 class="pageSubTitle">My Account</h3>
                <ul>
                    <li><a class="smlHyperLink">Basket</a></li>
                    <li><a class="smlHyperLink">Log in</a></li>
                    <li><a class="smlHyperLink">Register</a></li>
                    <li><a class="smlHyperLink">Edit profile</a></li>
                    <li><a class="smlHyperLink">Order history</a></li>
                    <li><a class="smlHyperLink">Special request</a></li>
                </ul>
            </div>
            
            <div class="col-xs-12 col-sm-4">
                <h3 class="pageSubTitle">Help</h3>
                <ul>
                    <li><a class="smlHyperLink">Home</a></li>
                    <li><a class="smlHyperLink">Contact us</a></li>
                    <li><a class="smlHyperLink">Terms &amp; Conditions</a></li>
                    <li><a class="smlHyperLink">Privacy policy</a></li>
                    <li><a class="smlHyperLink">FAQ</a></li>
                </ul>
            </div>             
            --%>
            </div><!-- COLUMN END -->
        </div><!-- ROW END -->
    <!-- CONTAINER END -->
</section>
</asp:Content>

