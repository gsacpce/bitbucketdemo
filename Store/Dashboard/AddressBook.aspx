﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Dashboard_AddressBook" %>

<script runat="server">

   
</script>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=host %>JS/jquery-ui.min.js"></script>
    <script src="<%=host %>JS/paging.js"></script>
    <script src="<%=host %>JS/bootbox.min.js"></script>

    <style type="text/css">
        .bootbox-close-button.close {
            display: none;
        }

        .modal-header {
            border-bottom: 1px solid #e5e5e5;
        }

        /*.modal-body {
            padding: 15px;
        }

        .customPanel.panel-heading, .customPanel thead, .customPanel.modal-header, .customPanel .modal-title {
            background-color: #606060 !important;
            color: #b0b0b0 !important;
        }

        .close {
            opacity: 1;
        }

        .pageSubSubTitle {
            color: #a0a0a0 !important;
            font-size: 14px !important;
            font-style: normal !important;
        }

        .modal-title {
            line-height: 1.42857;
            margin: 0;
        }*/
    </style>

    <style type="text/css">
        .edit {
            display: none;
        }

        .header {
            background-color: Gray;
            font-weight: bold;
            color: White;
            text-align: center;
        }

        .highlight {
            background-color: blue;
        }

        .hover {
            background-color: #00f;
            color: #fff;
        }

        .page {
            margin: 5px;
        }

        a {
            cursor: pointer;
            color: #2164f3;
        }

        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }

        .paging-nav {
            text-align: left;
            padding-top: 2px;
        }

            .paging-nav a {
                margin: auto 1px;
                text-decoration: none;
                display: inline-block;
                padding: 1px 7px;
                background: #91b9e6;
                color: white;
                border-radius: 3px;
            }

            .paging-nav .selected-page {
                background: #187ed5;
                font-weight: bold;
            }

        .redColor {
            color: #FF0000;
            position: absolute;
            width: auto;
        }

        h3 {
            font-size: 1.3em;
            line-height: 130%;
            font-weight: 300;
            margin: 15px 0 10px 0;
            padding: 0;
            color: #222;
        }

        .dashboard tr:nth-of-type(2n) {
            background-color: #f6f6f6;
        }

        .dashboard td {
            color: rgb(34, 34, 34);
            padding: 0.5em 0;
        }

        .dashboard table {
            font-size: .9em;
            line-height: 18px;
        }

        .remove {
            display: none;
        }
    </style>
    <script type="text/javascript">

        $(function () {
            $('#GridView1').paging({
                limit: 4,
                rowDisplayStyle: 'block',
                activePage: 0,
                rows: []
            });
            $('#GridView1').next().prependTo('.tableSet');
            //$('#POINT_TRANSACTIONS_BOTTOM div.paging-nav').addClass('pagination', 'pagination-sm');
            //$('#POINT_TRANSACTIONS_BOTTOM div.paging-nav')[0].className = 'pagination pagination-sm';
            $('.paging-nav a:nth-child(2)').addClass('selected-page');



        });
        $(document).ready(function () {

            var aText = $('#GridView1 .emailid').text().trim();
            if (aText === "Billing Address") {
                $('.hideDelete').css('display', 'none');
            }

        });

        //    //  $('#disp').css("display", "none");

        //    //code for paging
        //    $('#disp').hide();

        //    var rows = $('#GridView1').find(' tbody  .record ').length;
        //    var no_rec_per_page = 4;
        //    var no_pages = Math.ceil(rows / no_rec_per_page);
        //    var $pagenumbers = $('<div id="pages"></div>');
        //    for (i = 0; i < no_pages; i++) {
        //        $('<a href="#" class="page">' + (i + 1) + '</a>').appendTo($pagenumbers);
        //    }
        //    $pagenumbers.insertBefore('#GridView1');
        //    $('.page').hover(function () {
        //        $(this).addClass('hover');
        //    }, function () {
        //        $(this).removeClass('hover');
        //    });
        //    $('#GridView1').find(' tbody .record ').hide();
        //    var tr = $('#GridView1   tbody  .record  ');
        //    for (var i = 0; i <= no_rec_per_page - 1; i++) {
        //        $(tr[i]).show();
        //    }
        //    $('a').click(function (event) {
        //        $('#GridView1').find(' tbody .record ').hide();
        //        for (i = ($(this).text() - 1) * no_rec_per_page; i <= $(this).text() * no_rec_per_page - 1; i++) {
        //            $(tr[i]).show();

        //        }
        //    });
        //});


        $(document).ready(function () {

            $('.btn1').click(function () {
                var ID = $(this).attr("id");
                $('#result').hide();
                //var pos = $(this).offset();
                var width = $(this).width();

                //$('#disp').css({
                //    right: (pos.left + width) + -260 + 'px',
                //    top: pos.top - -25 + 'px'
                //    // alert(top);
                //});
                var td = $(this).closest('tr').children('td');
                var sr = $.trim(td.eq(0).text());
                if (sr == "Billing Address")
                    var val = "U";
                else
                    var val = "UD";

                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/AddressBook.aspx/GetAddressData',
                    dataType: 'JSON',
                    data: "{'id': " + ID + ",'option': '" + val + "'}",
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: true,
                }).done(function (data) {
                    //$("#Text2").val(data.d);
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        $("#myModal").find('input[id="txtId"]').val(ID);
                        $("#myModal").find('input[id="txtType"]').val(val);
                        $("#myModal").find('input[id="txtCompany"]').val(json[0].PreDefinedColumn2)
                        $("#myModal").find('input[id="txtAdd1"]').val(json[0].PreDefinedColumn3)
                        $("#myModal").find('input[id="txtAdd2"]').val(json[0].PreDefinedColumn4)
                        $("#myModal").find('input[id="txtTown"]').val(json[0].PreDefinedColumn5)
                        $("#myModal").find('input[id="txtCounty"]').val(json[0].PreDefinedColumn6)
                        $("#myModal").find('input[id="txtPostcode"]').val(json[0].PreDefinedColumn7)
                        $("#myModal").find('select[id="ddlCountry"]').val(json[0].PreDefinedColumn8)
                    }
                })
                //  $('#disp').show("slow");
                //alert($('#disp').html());
                $('#myModal').find('.modal-body').html($('#disp fieldset').html());
                $('#myModal').find('.modal-title').html('<h3>Add / Edit New Address</h3>');
                //$('#myModal').find('.modal-title').hide();
                //$('#myModal').find('.modal-header').hide();
                $('#myModal').find('.modal-footer').hide();
                $('#myModal').modal('show');
                //$('#myModal').find('.modal-body').removeClass('modalBottom');
                $('#myModal').find('.modal-body').css('padding', '0');

            });
        });

        $(document).ready(function () {
            //code for updation
            $(document).on('click', '#btn1', function () {
                debugger;
                $('#result').show();
                var id = $("#myModal").find('input[id="txtId"]').val();
                var Option = $("#myModal").find('input[id="txtType"]').val();
                var Company = $("#myModal").find('input[id="txtCompany"]').val();
                var Add1 = $("#myModal").find('input[id="txtAdd1"]').val();
                var Add2 = $("#myModal").find('input[id="txtAdd2"]').val();
                var Town = $("#myModal").find('input[id="txtTown"]').val();
                var county = $("#myModal").find('input[id="txtCounty"]').val();
                var Postcode = $("#myModal").find('input[id="txtPostcode"]').val();
                var Country = $("#myModal").find('select[id="ddlCountry"]').val();
                var CountryIndex = $("#myModal").find('select[id="ddlCountry"]').get(0).selectedIndex;
                if (Add1.trim().length > 0 && Company.trim().length > 0 && Town.trim().length > 0 && Postcode.trim().length > 0 && Country.trim().length > 0 && CountryIndex != 0) {

                    $.ajax({
                        type: "POST",
                        data: "{'id': " + id + ", 'option': '" + Option + "', 'company': '" + Company + "', 'add1': '" + Add1 + "', 'add2': '" + Add2 + "', 'town': '" + Town + "', 'county': '" + county + "', 'postcode': '" + Postcode + "', 'country': " + Country + "}",
                        dataType: "json",
                        contentType: 'application/json;charset=utf-8',
                        url: '<%=host %>' + 'Dashboard/AddressBook.aspx/UpdateUserAddressData',
                         success: function (data) {
                             if (data.d != "[]") {
                                 var json = $.parseJSON(data.d);
                                 $("#first" + id).html(json[0].Id);
                                 $("#second" + id).html(json[0].userid);
                                 $("#third" + id).html(json[0].AddressTitle);
                                 $("#fourth" + id).html(json[0].Address);
                                 $("#fifth" + id).html(json[0].flag);
                                 $('#mySuccessModal').find('#SuccessMessage').html(strAddressUpdate);
                                 $('#mySuccessModal').modal('show');
                                 //alert('Updated successfully');
                                 $('#myModal').modal('hide');


                             }
                         }

                     });
                 }
                 else {
                     $('#myWarningModal').find('#WarningMessage').html(strMandatory);
                     $('#myWarningModal').modal('show');
                     //alert("Please enter all mandatory fields");
                 }
             });
         });

        $(document).ready(function () {
            debugger;
             $('.del').click(function () {
                 var rid = $(this).attr("id");

                 var trid = $(this).parents(".record");

                 var td = $(this).closest('tr').children('td');
                 var sr = $.trim(td.eq(0).text());
                 if (sr == "Billing Address")
                     var val = "U";
                 else
                     var val = "UD";
                 if (val == "U") {
                     $('#myWarningModal').find('#WarningMessage').html(strBillingAddrMsg);
                     $('#myWarningModal').modal('show');
                     // alert('You can not delete billing address');
                     // return false;
                 }
                 else {

                     bootbox.confirm({
                         message: strDeleterAlert,
                         buttons: {
                             confirm: {
                                 label: 'Yes',
                                 className: 'btn-success'
                             },
                             cancel: {
                                 label: 'No',
                                 className: 'btn-danger'
                             }
                         },
                         callback: function (result) {
                             //  console.log('This was logged in the callback: ' + result);
                             if (result == true) {
                                 //if (confirm("Do you want to delete this record?")) {

                                 $.ajax({
                                     type: "POST",
                                     data: "{'id': " + rid + "}",
                                     dataType: "json",
                                     contentType: 'application/json;charset=utf-8',
                                     url: '<%=host %>' + 'Dashboard/AddressBook.aspx/DeleteUserAddressData',
                                    success: function (data) {
                                        // alert(data);
                                        if (data.d == "Success") {
                                            trid.css("background-color", "blue");
                                            trid.fadeOut(1000, function () {
                                                trid.remove();
                                            });
                                        }
                                    }
                                });
                            }
                            else { }
                        }
                    });
                }
            });
        });

        $(document).ready(function () {
            $('#txtSearch').keyup(function (e) {
                SearchGridData();
                e.preventDefault();
            });
        });
        function SearchGridData() {
            var counter = 0;
            //Get the search text
            var searchText = $('#<%=txtSearch.ClientID %>').val().toLowerCase();
            //Hide No record found message
            $('#<%=lblMessage.ClientID %>').hide();
            //Hode all the rows of gridview
            $('#<%=GridView1.ClientID %> tr:has(td)').hide();
            if (searchText.length > 0) {
                //Iterate all the td of all rows
                $('#<%=GridView1.ClientID %> tr:has(td)').children().each(function () {
                        var cellTextValue = $(this).text().toLowerCase();
                        //Check that text is matches or not
                        if (cellTextValue.indexOf(searchText) >= 0) {
                            $(this).parent().show();
                            counter++;
                        }
                    });
                    if (counter == 0) {
                        //Show No record found message
                        $('#<%=lblMessage.ClientID %>').show();
                }
            }
            else {
                    //Show All the rows of gridview
                $('#<%=GridView1.ClientID %> tr:has(td)').show();
                }
            }
    </script>

    <div id="div1" runat="server" class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 id="ContentPlaceHolder1_spnHeading" class="pageTitle"><span id="spnHeading" runat="server"></span></h1>
            </div>
        </div>
        <div class="row dashboard">
            <div class="col-xs-12">
                <div class="my-account-back-link">
                    <a href="<%=host %>Dashboard/MyAccount2.aspx"><i class="fa fa-long-arrow-left"></i>&nbsp <span id="spnBackToMyAcc" runat="server"></span></a>
                </div>
            </div>
            <div class="col-xs-12">
                <div id="POINT_TRANSACTIONS_BOTTOM" class="col-xs-12 tableSet">
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title"><i class="fa fa-user"></i><span class="heading_buttons"><span id="spnAddressTitle" runat="server"></span></span></h4>
                        </div>
                        <div id="POINT_TRANSACTIONS_COLLAPSE" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-3"><span id="spnAddress" runat="server"></span></div>
                                    <div class="col-xs-9 form-group modal_address_search_outer">
                                        <asp:TextBox ID="txtSearch" ClientIDMode="Static" Style="display: inline-block;" runat="server" class="form-control modal_address_search"></asp:TextBox>
                                        <asp:Label ID="lblMessage" runat="server" Text="No Record" Font-Bold="true" ForeColor="Red" Style="display: none"></asp:Label>

                                        <i class="fa fa-search" aria-hidden="true" style="display: inline-block;"></i>

                                    </div>
                                </div>
                                <div class="row" style="padding-top: 5px;">
                                    <div class="col-xs-12">
                                        <asp:GridView ID="GridView1" AutoGenerateColumns="false" runat="server" ClientIDMode="Static"
                                            CellPadding="4" ShowHeader="false"
                                            GridLines="None" RowStyle-CssClass="record" Width="100%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" ItemStyle-CssClass="useid" Visible="false">
                                                    <ItemTemplate>
                                                        <span id='first<%# Eval("Id") %>' class='text'><%# Eval("Id") %> </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="userid" ItemStyle-CssClass="useid" Visible="false">
                                                    <ItemTemplate>
                                                        <span id='second<%# Eval("Id") %>' class='text'><%# Eval("userid") %> </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AddressTitle" ItemStyle-CssClass="emailid">
                                                    <ItemTemplate>
                                                        <i class="fa fa-map-marker"></i>&nbsp;<span id='third<%# Eval("Id") %>' class='text'><%# Eval("AddressTitle") %> </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Address" ItemStyle-CssClass="firstname" ControlStyle-Width="300px">
                                                    <ItemTemplate>
                                                        <span id='fourth<%# Eval("Id") %>' class='text'><%# Eval("Address") %> </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="flag" ItemStyle-CssClass="firstname" Visible="false">
                                                    <ItemTemplate>
                                                        <span id='fifth<%# Eval("Id") %>' class='text'><%# Eval("flag") %> </span>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <a id='<%# Eval("Id") %>' class='btn1'><i class="fa fa-edit"></i></a>
                                                        <%--<input type="button" id='<%# Eval("Id") %>' value="Edit" class='btn1' />--%>
                                                        <a id='<%# Eval("Id") %>' rel="delete" class='del hideDelete'><i class="fa fa-trash"></i></a>
                                                        <%--<input type="button" id='<%# Eval("Id") %>' value="Delete" class='del' />--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--     <asp:TemplateField HeaderText="Delete" HeaderStyle-Width="80px" ItemStyle-Width="270px">
                                                <ItemTemplate>
                                                    <input type="button" id='<%# Eval("Id") %>' value="Delete" class='del' />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="disp" style="display: none;">
        <fieldset style="width: 600px; height: 100px;">
            <input id="txtId" type="text" class='edit text-left' />
            <input id="txtType" type="text" class='edit text-left' />
            <table class="table table-responsive form-horizontal">
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnCompany" runat="server"></span><span class="redColor">*</span></label></td>
                    <td style="text-align: left">
                        <%--<asp:TextBox ID="txtCompany" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>--%>
                        <input id="txtCompany" type="text" name="txtCompany" class="form-control" />

                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnAddressLine1" runat="server"></span><span class="redColor">*</span></label></td>
                    <td style="text-align: left">
                        <input id="txtAdd1" type="text" class="form-control" /></td>
                </tr>
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnAddressLine2" runat="server"></span></label>
                    </td>
                    <td style="text-align: left">
                        <input id="txtAdd2" type="text" class="form-control" /></td>
                </tr>
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnTown" runat="server"></span><span class="redColor">*</span></label></td>
                    <td style="text-align: left">
                        <input id="txtTown" type="text" class="form-control" /></td>
                </tr>
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnCounty" runat="server"></span></label>
                    </td>
                    <td style="text-align: left">
                        <input id="txtCounty" type="text" class="form-control" /></td>
                </tr>
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnPostcode" runat="server"></span><span class="redColor">*</span></label></td>
                    <td style="text-align: left">
                        <input id="txtPostcode" type="text" class="form-control" /></td>
                </tr>
                <tr>
                    <td>
                        <label class="col-xs-8 control-label"><span id="spnCountry" runat="server"></span><span class="redColor">*</span></label></td>
                    <td style="text-align: left">
                        <asp:DropDownList ID="ddlCountry" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td></td>

                    <td>
                        <input type="button" id="btn2" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" value='<%=strCancel %>' />
                        <input type="button" id="btn1" class="btn btn-primary customActionBtn" value='<%=strSaveAddress %>' />
                    </td>
                </tr>

            </table>
        </fieldset>
    </div>

    <%--Added By Snehal - 18 03 2017--%>
    <script type="text/javascript">
        var strMandatory = "<%=strMandatory %>";
        var strBillingAddrMsg = "<%=strBillingAddrMsg %>";
        var strAddressUpdate = "<%=strAddressUpdate %>";
        var strDeleterAlert = "<%=strDeleterAlert %>";            
    </script>
</asp:Content>

