﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Presentation.Dashboard_ProductView" %>
<div class="container-fluid">
    <div class="row  product_outer">
        <div id="slider-thumbs" class="col-lg-5 col-md-5 col-sm-5 col-xs-12 product_image_panel">
            <%--<div class="col-md-12" id="slider-thumbs">--%>
                <img id="imgProduct" class="product_feature" runat="server" />
            <%--</div>--%>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 product_text_panel_top">
            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="dashboard_qview_title">
                <asp:Literal ID="ltrProductTitle" runat="server"></asp:Literal>
            </div>
            <div class="dashboard_qview_code">
                <asp:Literal ID="ltrProductCode" runat="server"></asp:Literal>
            </div>
            <div class="dashboard_qview_description">
                <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
            </div>
            <div class="product_detail_button">
                <a id="aViewDetails" runat="server" class="btn btn-primary customButton"><%=strViewProduct %></a>
            </div>

        </div>
    </div>
</div>
<style>
    .product_image_panel {padding: 30px 0 20px;}
    .product_text_panel_top {padding: 30px 0 0 30px;}
</style>
