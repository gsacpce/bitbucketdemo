﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Dashboard_BudgetTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="<%=host %>JS/jquery-ui.min.js"></script>
    <script src="<%=host %>JS/paging.js"></script>
    <script src="<%=host %>JS/moment.js"></script>
    <style type="text/css">
        .dashboard table .three_equal_column {
            color: black;
            width: 33.33%;
        }

        .dashboard .top_tables_headings {
            font-size: 36px;
            padding-left: 0;
            padding-right: 0px;
            border-bottom: 2px dashed rgba(0, 0, 0, 0.2);
            padding-bottom: 0.5em;
            margin-bottom: 0;
        }

        table, td, th {
            border: none !important;
        }

        div {
            display: block;
        }

        .nicescrollX {
            max-height: none;
            padding: 0;
        }

        .paging-nav {
            text-align: left;
            padding-top: 2px;
        }

        .dashboard tr:nth-of-type(2n) {
            background-color: #f6f6f6;
        }

        .paging-nav a {
            margin: auto 1px;
            text-decoration: none;
            display: inline-block;
            padding: 1px 7px;
            background: #91b9e6;
            color: white;
            border-radius: 3px;
        }

        .paging-nav .selected-page {
            background: #187ed5;
            font-weight: bold;
        }

        .sortAsc {
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
            width: 200px;
        }

        .sortDesc {
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
            width: 200px;
        }

        .grid {
            font-family: Arial;
            font-size: 10pt;
            width: 600px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            GetBudgetDetails()
            GetAllBudgetTransactionDetails()
        });

        function sortTable(table, col, reverse) {
            var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
                tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
                i;
            reverse = -((+reverse) || -1);

            tr = tr.sort(function (a, b) { // sort rows


                if (!isNaN(a.cells[col].textContent) && !isNaN(b.cells[col].textContent))
                    return reverse * ((+a.cells[col].textContent) - (+b.cells[col].textContent))
                return reverse // `-1 *` if want opposite order
                     * (a.cells[col].textContent.trim() // using `.textContent.trim()` for test
                         .localeCompare(b.cells[col].textContent.trim())
                        );
            });
            for (i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
        }

        function makeSortable(table) {
            var th = table.tHead, i;
            th && (th = th.rows[0]) && (th = th.cells);
            if (th) i = th.length;
            else return; // if no `<thead>` then do nothing
            while (--i >= 0) (function (i) {
                var dir = 1;
                th[i].addEventListener('click', function () { sortTable(table, i, (dir = 1 - dir)) });
            }(i));
        }

        function makeAllSortable(parent) {
            parent = parent || document.body;
            var t = parent.getElementsByTagName('table'), i = t.length;
            while (--i >= 0) makeSortable(t[i]);
            $(".pop").niceScroll();
        }

        function GetCurrencySymbol(Curr) {
            var currency_symbols = {
                'USD': '$', // US Dollar
                'EUR': '€', // Euro
                'DKK': 'DKK', // Danish krone
                'GBP': '£', // British Pound Sterling
                'NOK': 'NOK', // Norwegian krone
                'SEK': 'SEK', // Swedish krona
                'CHF': 'C', // Swiss franc
                'TRY': '₤', // Turkish new lira
                'GERMANYEUR': '€', // German Euro
                'GERMANYSEK': 'SEK', // German Krona
                'JPY': '¥', // Japanese Yen          
                'ILS': '₪', // Israeli New Sheqel
                'INR': '₹', // Indian Rupee
                'KRW': '₩', // South Korean Won
                'NGN': '₦', // Nigerian Naira
                'PHP': '₱', // Philippine Peso
                'PLN': 'zł', // Polish Zloty
                'PYG': '₲', // Paraguayan Guarani
                'THB': '฿', // Thai Baht
                'UAH': '₴', // Ukrainian Hryvnia
                'VND': '₫', // Vietnamese Dong
                'CRC': '₡', // Costa Rican Colón
            };
            var currency_name = Curr;
            return currency_symbols[currency_name];
        }

        function GetBudgetDetails() {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetBudgetDetails',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () { }
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        $("#spnAvailable").closest('.panel-default').find('.loaderX').hide()
                        $("#spnSpent").html(GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOrdersInvoiced)
                        $("#spnPending").html(GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOrdersNotInvoiced)
                        $("#spnAvailable").html(GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOutstandingBudget)
                    }
                })
            }
            catch (e) {
            }
        }

        function GetAllBudgetTransactionDetails() {
            try {

                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/BudgetTransaction.aspx/GetBudgetTransactionDetails',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () { },
                    success: function (data) {
                        $("#gvBudgetTransaction").closest('.panel-default').find('.loaderX').hide()
                        $("#gvBudgetTransaction").empty();
                        $("#gvBudgetTransaction").append("<thead><tr><th class='sales_column_1'>"+strDate+"<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_2'>"+strOrderNumber+"<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_3'>"+strTotal+"</th></tr></thead>");
                        $("#gvBudgetTransaction").append('<tbody id="appendX"></tbody>')
                        for (var i = 0; i < data.d.length; i++) {
                            var parsedDate = moment(data.d[i].OrderDate);
                            var formattedDate = parsedDate.format('DD/MM/YYYY');
                            $("#appendX").append("<tr><td>" + formattedDate + "</td><td><a href='javascript:void(0);' id='aOrderHistory' currSymbol=" + GetCurrencySymbol(data.d[i].OrderCurrency.trim()) + "  SalesOrderID=" + data.d[i].SalesOrderID + ">" + data.d[i].SalesOrderID + "</a></td><td>" + GetCurrencySymbol(data.d[i].OrderCurrency.trim()) + data.d[i].OrderValue + "</td></tr>");
                        }
                        $('#gvBudgetTransaction').paging({
                            limit: 10,
                            rowDisplayStyle: 'block',
                            activePage: 0,
                            rows: []
                        });
                        $('#gvBudgetTransaction').next().prependTo('.tableSet');

                        $('.paging-nav a:nth-child(2)').addClass('selected-page');
                        makeAllSortable();

                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            catch (e) {
            }
        }

        $(document).on('click', '#aOrderHistory', function (e) {
            var id = $(this).attr("SalesOrderID");
            var currSym = $(this).attr("currSymbol");
            try {
                $.ajax({
                    type: "POST",
                    url: '<%=host %>' + 'Dashboard/BudgetTransaction.aspx/RedirectOrderHistory',
                     dataType: 'JSON',
                     data: "{'salesorderid': " + id + ",'CurrSymbol':'" + currSym + "'}",
                     contentType: 'application/json;charset=utf-8',
                     cache: false,
                     async: true
                 }).done(function (response) {
                     if (response.d = "success") {
                         window.location.href = '<%=host%>' + 'Orders/OrderItem.aspx';
                }
                 });
        } catch (e) { }
            finally { }
        });

    </script>

    <div id="div1" runat="server" class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 id="ContentPlaceHolder1_spnHeading" class="pageTitle"><span id="spnHeading" runat="server"></span></h1>
            </div>
        </div>

        <div class="row dashboard">
            <div class="col-xs-12">
                <div class="my-account-back-link">
                    <a href="<%=host %>Dashboard/MyAccount2.aspx"><i class="fa fa-long-arrow-left"></i><span id="spnBackToMyAcc" runat="server"></span></a>
                </div>
            </div>
            
                <div id="POINT_TRANSACTIONS_TOP" class="col-xs-12 tableSet">
                    <br />
                    <div class="panel panel-default">
                        <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                        <div class="panel-heading textless_panel" role="tab"></div>
                        <div class="panel-body left_right_no_pad tablesorter">

                            <div class="col-xs-12 col-md-4">
                                <div class="three_equal_column">
                                    <h1 class="top_tables_headings"><span id="spnSpent"></span></h1>
                                </div>
                                <div><span id="spnSpent1" runat="server"></span></div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="three_equal_column">
                                    <h1 class="top_tables_headings"><span id="spnPending"></span></h1>
                                </div>
                                <div><span id="spnPending1" runat="server"></span></div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="three_equal_column">
                                    <h1 class="top_tables_headings"><span id="spnAvailable"></span></h1>
                                </div>
                                <div><span id="spnAvailable1" runat="server"></span></div>
                            </div>
                        </div>


                    </div>
                </div>
           
            <div id="POINT_TRANSACTIONS_BOTTOM" class="col-xs-12">
                <div class="panel panel-default">
                    <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                    <div class="panel-heading panelHeading">
                        <h4 class="panel-title"><i class="fa fa-dollar"></i>&nbsp <span id="spnBudgetTrans" runat="server"></span></h4>
                        <div class="collapse_triggers">
                            <a data-toggle="collapse" href="#POINT_TRANSACTIONS_COLLAPSE" id="headingOne">
                                <%-- <asp:Label ID="Label1" runat="server" Text="Total Orders"></asp:Label>--%>
                                <i class="fa fa-minus-circle"></i>
                            </a>
                        </div>
                    </div>
                    <%-- <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title"><i class="fa fa-star"></i><span class="heading_buttons">Budget Transactions</span></h4>
                        </div>--%>
                    <div id="POINT_TRANSACTIONS_COLLAPSE" class="panel-collapse collapse in">
                        <div class="panel-body nicescrollX" style="min-height: 180px;">
                            <div id="Points">
                                <asp:GridView ID="gvBudgetTransaction" runat="server" ClientIDMode="Static" CssClass="grid">
                                    <HeaderStyle Font-Bold="true" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

     <%--Added By Snehal - 18 03 2017--%>
        <script type="text/javascript">
            var strDate = "<%=strDate %>";
            var strOrderNumber = "<%=strOrderNumber %>";
            var strTotal = "<%=strTotal %>";
            </script>
</asp:Content>
