﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Dashboard_DashboardIndeed" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" ClientIDMode="Static">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    <%--<script src="<%=host %>JS/jquery.tablesorter.min.js"></script>--%>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <%--    <script src="<%=host %>JS/jquery-ui.js"></script>--%>
    <%--  <script src="<%=host %>JS/jquery-1.12.4.js"></script>--%>
    <%--    <link href="<%=host %>CSS/Progressbar.css" rel="stylesheet" />
    <link href="<%=host %>CSS/styleProgress.css" rel="stylesheet" />--%>
    <script>
        function sortTable(table, col, reverse) {
            var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
                tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
                i;
            reverse = -((+reverse) || -1);

            tr = tr.sort(function (a, b) { // sort rows


                if (!isNaN(a.cells[col].textContent) && !isNaN(b.cells[col].textContent))
                    return reverse * ((+a.cells[col].textContent) - (+b.cells[col].textContent))
                return reverse // `-1 *` if want opposite order
                     * (a.cells[col].textContent.trim() // using `.textContent.trim()` for test
                         .localeCompare(b.cells[col].textContent.trim())
                        );
            });
            for (i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
        }

        function makeSortable(table) {
            var th = table.tHead, i;
            th && (th = th.rows[0]) && (th = th.cells);
            if (th) i = th.length;
            else return; // if no `<thead>` then do nothing
            while (--i >= 0) (function (i) {
                var dir = 1;
                th[i].addEventListener('click', function () { sortTable(table, i, (dir = 1 - dir)) });
            }(i));
        }

        function makeAllSortable(parent) {
            parent = parent || document.body;
            var t = parent.getElementsByTagName('table'), i = t.length;
            while (--i >= 0) makeSortable(t[i]);
            $(".pop").niceScroll();
        }

    </script>
    
  <style type="text/css">
       
            table, td, th {
            border: none !important;
        }
.dashboard .dash_progress_parent {
    display: block;
    float: left;
    width: 0;
    margin: 0;
    padding: 0;
}
.dashboard .total_sales_left {
    display: block;
    float: left;
    /*width: 67%;*/
    background-color: #0000cc;
    margin: 0;
    padding: 0;
}
.dashboard .total_sales_right {
    display: block;
    float: left;
    /*width: 100%;*/
    background-color: #cd29c0;
    margin: 0;
    padding: 0;
}
    </style>    
   

    <script type="text/javascript">
        $(document).ready(function () {
            // $("#gvQTRRegion").tablesorter();
            $(".dash_progress_parent").animate({ width: '100%' }, 1000);
            GetTotalOrderData();
            GetCurQtrSale(1);
            //GetAllData();
            GetAllQTROrderByRegion('QTR')
            $("#SALES_REGION_QTR_BTN").addClass("selected");

            GetAllQTROrderByTeam('QTR')
            $("#SALES_TEAM_QTR_BTN").addClass("selected");

            GetAllQTRNewProducts('QTR', '5')
            $("#NEW_PRODUCTS_QTR_BTN").addClass("selected");
            $("#NEW_PRODUCTS_5_BTN").addClass("selected");

            GetAllQTRTopProducts('QTR', '5')
            $("#TOP_PRODUCTS_QTR_BTN").addClass("selected");
            $("#TOP_PRODUCTS_5_BTN").addClass("selected");

            // $("#progressbar").progressbar({ value: 30, max: 50 });
            GetAllOrderUsage('QTR')
            $("#ORDER_USAGE_QTR_BTN").addClass("selected");
        });


        $(document).on('change', '#ddlCurrency', function () {
            try {
                var RegionVal = $("a.activelink.selected").text();

                var TeamVal = $("a.activeTeamlink.selected").text();
                GetCurQtrSale(this.value);
                GetAllQTROrderByRegion(RegionVal);
                GetAllQTROrderByTeam(TeamVal);

                var TopVal = $("a.activeToplink.selected").text();
                var TopFilter = $("a.activeTopFilterlink.selected").text();
                GetAllQTRTopProducts(TopVal, TopFilter);
            }
            catch (e) { }
            finally { }
        });

        function GetTotalOrderData() {

            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetTotalOrderData',
                    dataType: 'JSON',
                    //data: JSON.stringify({ pageNo: pageNo }),
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {}
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        $("#spnCurQtrTotal").closest('.panel-default').find('.loaderX').hide()
                        $("#spnCurQtrTotal").html(json[0].Cur_Qtr_Total_Orders);
                        $("#spnLastQtrTotal").html(json[0].Lst_Qtr_Total_Orders);
                        if (parseFloat(json[0].Percent) > 0) {
                            $("#UpArrow").html("<i class=\"fa fa-arrow-up\" aria-hidden=\"true\"></i> " + json[0].Percent);
                        }
                        else {
                            $("#DownArrow").html("<i class=\"fa fa-arrow-down\" aria-hidden=\"true\"></i> " + json[0].Percent);
                        }
                    }
                })
            }
            catch (e) {
            }
        }

        function GetCurQtrSale(id) {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetCurQtrSale',
                    dataType: 'JSON',
                    //data: JSON.stringify({ pageNo: pageNo }),
                    data: "{'CurrId': " + id + "}",
                    //  data: "{'CurrCode': 'GBP'}",
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {
                        //alert('geeti');
                    }
                }).done(function (data) {
                    if (data.d != "[]") {

                        var json = $.parseJSON(data.d);
                        $("#spnCurQtrSale").closest('.panel-default').find('.loaderX').hide()
                        $("#spnCurQtrSale").html(json[0].Cur_Qtr_Sales);
                        $("#CurrSymbol").html(json[0].CurrencySymbol);
                        $("#CurrSymbol1").html(json[0].CurrencySymbol);
                        $("#spnProjectedSale").html(json[0].FeatureDefaultValue + ' Projected Sales');
                        //  $("#progressbar").progressbar({ value: json[0].Cur_Qtr_Sales, max: json[0].FeatureDefaultValue });
                        //document.getElementById("progressbar").value = json[0].Cur_Qtr_Sales;
                        //document.getElementById("progressbar").max = json[0].FeatureDefaultValue;

                        if (parseInt(json[0].Cur_Qtr_Sales) > parseInt(json[0].FeatureDefaultValue)) {
                            var Barwidth = (json[0].FeatureDefaultValue / json[0].Cur_Qtr_Sales) * 100;

                            $("#progressbar").css('width', '100%');
                            $("#progressbarProj").css('width', Barwidth + '%');
                            //document.getElementById("progressbar").value = json[0].Cur_Qtr_Sales;
                            //document.getElementById("progressbar").max = json[0].Cur_Qtr_Sales;

                            //document.getElementById("progressbarProj").value = json[0].FeatureDefaultValue;
                            //document.getElementById("progressbarProj").max = json[0].Cur_Qtr_Sales;
                        }
                        else {
                            var Barwidth = (json[0].Cur_Qtr_Sales / json[0].FeatureDefaultValue) * 100;

                            $("#progressbarProj").css('width', '100%');;
                            $("#progressbar").css('width', Barwidth + '%');

                            //document.getElementById("progressbar").value = json[0].Cur_Qtr_Sales;
                            //document.getElementById("progressbar").max = json[0].FeatureDefaultValue;


                            //document.getElementById("progressbarProj").value = json[0].FeatureDefaultValue;
                            //document.getElementById("progressbarProj").max = json[0].FeatureDefaultValue;
                        }
                    }

                })
            }
            catch (e) {
            }
        }

        function GetAllQTROrderByRegion(val) {
            try {
                var e = document.getElementById("ddlCurrency");
                var id = e.options[e.selectedIndex].value;

                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetSalesOrderByRegion',
                    dataType: 'JSON',
                    data: "{'ValueRange': '" + val + "','CurrId': " + id + "}",
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {
                        //alert('geeti');
                    },
                    success: function (data) {
                        $("#gvQTRRegion").closest('.panel-default').find('.loaderX').hide()
                        $("#gvQTRRegion").empty();
                        $("#gvQTRRegion").append("<thead><tr><th>" + strRegion + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th>" + strTotalOrders + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th>" + strTotalSales + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr></thead>");
                        $("#gvQTRRegion").append('<tbody class="pop" id="pop1"></tbody>');
                        for (var i = 0; i < data.d.length; i++) {
                            $("#pop1").append("<tr><td>" + data.d[i].Groupvalue + "</td><td>" + data.d[i].Totalorders + "</td><td>" + data.d[i].TotalSales + "</td></tr></tbody>");
                        }
                        //alert(makeAllSortable())
                        makeAllSortable();
                       
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            catch (e) {
            }
        }

        //$(".heading_buttons").click(function () {
        //    $(".heading_buttons").removeClass("selected");
        //    $(this).addClass("selected");
        //});
        function click(val, id) {

            $(".activelink").removeClass("selected");
            $("#" + id).addClass("selected");
            GetAllQTROrderByRegion(val);
        }

        function Teamclick(val, id) {
            $(".activeTeamlink").removeClass("selected");
            $("#" + id).addClass("selected");
            GetAllQTROrderByTeam(val);
        }

        function Orderclick(val, id) {
            $(".activeOrderlink").removeClass("selected");
            $("#" + id).addClass("selected");
            GetAllOrderUsage(val);
        }

        function ProductNewclick(val, id) {
            $(".activeNewlink").removeClass("selected");
            $("#" + id).addClass("selected");
            var FilterVal = $("a.activeNewFilterlink.selected").text();
            GetAllQTRNewProducts(val, FilterVal);
        }

        function ProductNewFilterclick(FilterVal, id) {
            $(".activeNewFilterlink").removeClass("selected");
            $("#" + id).addClass("selected");
            var val = $("a.activeNewlink.selected").text();
            GetAllQTRNewProducts(val, FilterVal);
        }

        function ProductTopclick(val, id) {
            $(".activeToplink").removeClass("selected");
            $("#" + id).addClass("selected");
            var FilterVal = $("a.activeTopFilterlink.selected").text();
            GetAllQTRTopProducts(val, FilterVal);
        }

        function ProductTopFilterclick(FilterVal, id) {
            $(".activeTopFilterlink").removeClass("selected");
            $("#" + id).addClass("selected");
            var val = $("a.activeToplink.selected").text();
            GetAllQTRTopProducts(val, FilterVal);
        }



        function GetAllQTROrderByTeam(val) {
            try {
                var e = document.getElementById("ddlCurrency");
                var id = e.options[e.selectedIndex].value;
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetSalesOrderByTeam',
                    dataType: 'JSON',
                    data: "{'ValueRange': '" + val + "','CurrId': " + id + "}",
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {},
                    success: function (data) {
                        $("#gvQTRTeam").closest('.panel-default').find('.loaderX').hide()
                        $("#gvQTRTeam").empty();
                        $("#gvQTRTeam").append("<thead><tr><th>" + strTeam + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th>" + strTotalOrders + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th>" + strTotalSales + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr></thead>");
                        $("#gvQTRTeam").append('<tbody class="pop" id="pop2"></tbody>');
                        for (var i = 0; i < data.d.length; i++) {
                            $("#pop2").append("<tr><td>" + data.d[i].Groupvalue + "</td><td>" + data.d[i].Totalorders + "</td><td>" + data.d[i].TotalSales + "</td></tr></tbody>");


                        //$("#gvQTRTeam").empty();
                        //$("#gvQTRTeam").append("<tr><th class='sales_column_1'>Team<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_2'>Total Orders<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_3'>Total Sales<div class='sort_icon'><i class='fa fa-sort-desc'></i></div></th></tr>");
                        //for (var i = 0; i < data.d.length; i++) {
                        //    $("#gvQTRTeam").append("<tr><td>" + data.d[i].Groupvalue + "</td><td>" + data.d[i].Totalorders + "</td><td>" + data.d[i].TotalSales + "</td></tr>");
                        }
                        makeAllSortable();
                        
                    },
                    error: function (result) {
                        //alert(result);
                        alert("Error");
                    }
                });
            }
            catch (e) {
            }
        }


        function GetAllQTRNewProducts(val, FilterVal) {

            try {
                var host = "<%=host %>";
                //  alert(host);
                var e = document.getElementById("ddlCurrency");
                var id = e.options[e.selectedIndex].value;
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetNewProducts',
                    dataType: 'JSON',
                    data: "{'ValueRange': '" + val + "','CurrId': " + id + ",'Filter': " + FilterVal + "}",
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () { },
                    success: function (data) {
                        $("#gvQTRNewProduct").closest('.panel-default').find('.loaderX').hide()
                        $("#gvQTRNewProduct").empty();
                        $("#gvQTRNewProduct").append("<thead><tr><th>" + strProduct + "</th><th>" + strProductName + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr></thead>");
                        $("#gvQTRNewProduct").append('<tbody class="pop" id="pop3"></tbody>');
                        for (var i = 0; i < data.d.length; i++) {

                            $("#pop3").append("<tr><td><img src='" + host + "/Images/Products/Thumbnail/" + data.d[i].DefaultImageName + "'></td><td><a href='javascript:void(0);' id='aQuickView1' productid=" + data.d[i].ProductId + ">" + data.d[i].ProductName + "</a></td></tr></tbody>");
                        }
                        //alert(makeAllSortable())
                        makeAllSortable();



                        //$("#gvQTRNewProduct").empty();
                        //$("#gvQTRNewProduct").append("<tr><th class='sales_column_1'colspan=\"2\">Product<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr>");
                        //for (var i = 0; i < data.d.length; i++) {

                        //    $("#gvQTRNewProduct").append("<tr><td><img src='" + host + "/Images/Products/Thumbnail/" + data.d[i].DefaultImageName + "'></td><td><a href='javascript:void(0);' id='aQuickView1' productid=" + data.d[i].ProductId + ">" + data.d[i].ProductName + "</a></td></tr>");
                        //    //$("#gvQTRNewProduct").append("<tr><td>" + data.d[i].ProductName + "</td></tr>");

                        //}

                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            catch (e) {
            }
        }


        function GetAllQTRTopProducts(val, FilterVal) {

            try {
                var host = "<%=host %>";
                var e = document.getElementById("ddlCurrency");
                var id = e.options[e.selectedIndex].value;
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetTopProducts',
                    dataType: 'JSON',
                    data: "{'ValueRange': '" + val + "','CurrId': " + id + ",'Filter': " + FilterVal + "}",
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () { },
                    success: function (data) {
                        $("#gvQTRTopProduct").closest('.panel-default').find('.loaderX').hide()
                        $("#gvQTRTopProduct").empty();
                        $("#gvQTRTopProduct").append("<thead><tr><th>" + strProduct + "</th><th>" + strProductName + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th>" + strUnits + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th>" + strSales + "<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr></thead>");
                        $("#gvQTRTopProduct").append('<tbody class="pop" id="pop4"></tbody>');
                        for (var i = 0; i < data.d.length; i++) {
                            $("#pop4").append("<tr><td><img src='" + host + "/Images/Products/Thumbnail/" + data.d[i].DefaultImageName + "'></td><td><a href='javascript:void(0);' id='aQuickView1' productid=" + data.d[i].ProductId + ">" + data.d[i].ProductName + "</a></td><td>" + data.d[i].Units + "</td><td>" + data.d[i].Sales + "</td></tr></tbody>");
                        }
                        //alert(makeAllSortable())
                        makeAllSortable();

                        //$("#gvQTRTopProduct").empty();
                        //$("#gvQTRTopProduct").append("<tr><th class='sales_column_1' colspan=\"2\">Product<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_2'>Units<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_3'>Sales<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr>");
                        //for (var i = 0; i < data.d.length; i++) {
                        //    $("#gvQTRTopProduct").append("<tr><td><img src='" + host + "/Images/Products/Thumbnail/" + data.d[i].DefaultImageName + "'></td><td><a href='javascript:void(0);' id='aQuickView1' productid=" + data.d[i].ProductId + ">" + data.d[i].ProductName + "</a></td><td>" + data.d[i].Units + "</td><td>" + data.d[i].Sales + "</td></tr>");
                        //    //$("#gvQTRTopProduct").append("<tr><td>" + data.d[i].ProductName + "</td></tr>");
                        //}
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            catch (e) {
            }
        }

        function GetAllOrderUsage(val) {
            google.load("visualization", "1", { packages: ["corechart"], callback: drawChart });
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var options = {
                    width: '100%',
                    legend: 'none',
                    height: 200,
                    colors: ['#0000cc'],

                    bar: { groupWidth: "50%" },
                    // tooltip: { isHTML: true, text: 'value' }
                    tooltip: { trigger: 'none' },
                    trendlines: { 0: {} },
                    chartArea: { 'width': '92%', 'height': '60%', 'top': '9%' },
                    hAxis: { allowHTML: true },
                    vAxis: { textPosition: 'none', minValue: 0, gridlines: { color: 'transparent' } }
                };

                debugger;

                $.ajax({

                    type: "POST",
                    url: '<%=host %>' + 'Dashboard/DashboardIndeed.aspx/GetOrderUsage',
                        data: "{'ValueRange': '" + val + "'}",
                        // data: "{'ValueRange': '" + $("a.activeTeamlink.selected").text() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () { },
                        success: function (r) {
                            
                            var data = google.visualization.arrayToDataTable(r.d);
                            var chart = new google.visualization.ColumnChart(document.getElementById('chart'));

                            google.visualization.events.addListener(chart, 'error', function (googleError) {
                                google.visualization.errors.removeError(googleError.id);
                                document.getElementById("error_msg").innerHTML = strNoOrderUsageMsg;
                            });

                            chart.draw(data, options);

                        },                       
                    failure: function (r) {
                            $('#chart').addClass('displayNone');

                        },
                        error: function (r) {
                            $('#chart').addClass('displayNone');
                        }
                    });
                }
            }

            $(document).on('click', '#aQuickView1', function (e) {
                var id = $(this).attr("productid");
                var LanguageId = "<%=LanguageId %>";
            var CurrencyId = "<%=CurrencyId %>";
            $('#myModal').find('.modal-body').html('');
            $('#dvLoader').show();
            try {
                $.ajax({
                    type: "POST",
                    url: '<%=host %>' + 'Dashboard/ProductView.aspx',
                    data: {
                        productid: id,
                        languageid: LanguageId,
                        currencyid: CurrencyId,
                    },
                    //data: "{'productid': " + id + ",'languageid': " + LanguageId + ",'currencyid': " + CurrencyId + "}",              
                    cache: false,
                    async: true
                }).done(function (response) {
                    $('#dvLoader').hide();
                    $('#myModal').find('.modal-dialog').addClass('modal-lg');
                    $('#myModal').find('.modal-body').html(response);
                    //  $('#myModal').find('.modal-title').html($('#myModal').find('.modal-body').find('#hidProductName').val());
                    $('#myModal').find('.modal-title').hide();
                    $('#myModal').find('.modal-header').hide();
                    $('#myModal').find('.modal-footer').hide();
                    $('#myModal').modal('show');
                    $('#myModal').find('.modal-body').removeClass('modalBottom');
                    $('#myModal').find('.modal-body').css('padding', '0');

                });
            } catch (e) { }
            finally { $('#dvLoader').hide(); }
        });


    </script>

    <div id="div1" runat="server" class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 id="ContentPlaceHolder1_spnHeading" class="pageTitle"><span id="spnHeading" runat="server"></span></h1>
            </div>
        </div>
        <div class="row dashboard">
            <div class="col-xs-12">
                <div class="row currency_selector">
                    <div class="col-xs-12">
                        <div class="form-group pull-right">
                            <asp:label runat="server" class="" id="lblCurrency">Currency :</asp:label>
                            <%--<div style="float: left" id="divCurr" runat="server">--%>
                            <asp:DropDownList ID="ddlCurrency" runat="server" ClientIDMode="Static" CssClass="form-control" Style="width: auto; display: inline-block">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 ">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-clipboard"></i>&nbsp<span id="spnTotalOrder" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse1" id="a1">
                                        <%-- <asp:Label ID="lbl1" runat="server" Text="Total Orders"></asp:Label>--%>
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse1" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                 <div class="panel-body" style="height:223px; max-height: 223px;">
                              <%--  <div class="panel-body" style="height:265px; max-height: 240px;">--%>
                                    <h5><span id="spnCurQtrTotal"></span></h5>
                                    <p>
                                        <span class="highlight_red" id="DownArrow"></span>
                                        <span class="highlight_green" id="UpArrow"></span>
                                        <span id="spnFromLastQtr" runat="server"></span> (<span id="spnLastQtrTotal"></span>)
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-dollar"></i>&nbsp <span id="spnTotalSales" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse2" id="a2">
                                        <%-- <asp:Label ID="Label1" runat="server" Text="Total Orders"></asp:Label>--%>
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse2" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body">
                                    <h5><span id="CurrSymbol1"></span><span id="spnCurQtrSale"></span></h5>
                                    <p><span id="CurrSymbol"></span><span id="spnProjectedSale" runat="server"></span></p>
                                     <div class="dash_progress_text_above">
                                     <div class="dash_text_triple_left"><span id="spnTotal" runat="server"></span></div>
                                         </div>
                                      <div class="dash_progress_parent">
                                        <div id="progressbar" class="dash_progress total_sales_left"></div>
                                    </div>
                                     <div class="dash_progress_text_above">
                                     <div class="dash_text_triple_left"><span id="spnProjected" runat="server"></span></div>
                                         </div>
                                     <div class="dash_progress_parent">
                                        <div id="progressbarProj" class="dash_progress total_sales_right"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-map-marker"></i>&nbsp 
                                    <span class="heading_buttons" id="spnOrderByRegion" runat="server"></span>
                                    <br class="buttons_newline">
                                    <a id="SALES_REGION_QTR_BTN" class="heading_buttons activelink selected" href="javascript:click('QTR','SALES_REGION_QTR_BTN')">QTR</a>
                                    <a id="SALES_REGION_YTD_BTN" class="heading_buttons activelink" href="javascript:click('YTD','SALES_REGION_YTD_BTN')">YTD</a>
                                </h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse3" id="a3">
                                        <%--  <span id="">Sales/Order by Region</span></a>

                        <a id="SALES_REGION_QTR_BTN" class="activelink" href="javascript:click('QTR','SALES_REGION_QTR_BTN')">QTR</a>
                        <a id="SALES_REGION_YTD_BTN" class="activelink" href="javascript:click('YTD','SALES_REGION_YTD_BTN')">YTD</a>--%>
                                        <i class="fa fa-minus-circle"></i>

                                    </a>
                                </div>
                            </div>
                            <div id="collapse3" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <%--<table rules="all" id="dummyheader" style="border-collapse: collapse;" cellspacing="0" border="1" runat="server">
                                    <thead>
                                        <tr>
                                            <th scope="col">Region</th>
                                            <th scope="col">Total Orders</th>
                                            <th scope="col">Total Sales</th>
                                        </tr>
                                    </thead>
                                </table>--%>
                                <div class="panel-body nicescrollX">
                                    <div id="divQTR" clientidmode="Static"  runat="server">
                                        <asp:GridView ID="gvQTRRegion" runat="server" ClientIDMode="Static"  AllowSorting="true" CssClass="grid">
                                             <HeaderStyle Font-Bold="true" />
                                           <%-- <Columns>
                                                <asp:BoundField ItemStyle-Width="200px"
                                                    DataField="Groupvalue" HeaderText="Region" />
                                                <asp:BoundField ItemStyle-Width="200px"
                                                    DataField="Totalorders" HeaderText="Total Orders" />
                                                <asp:BoundField ItemStyle-Width="200px"
                                                    DataField="TotalSales" HeaderText="Total Sales" />
                                            </Columns>--%>
                                        </asp:GridView>
                                    </div>
                                    <div id="divYTD" clientidmode="Static">
                                        <asp:GridView ID="gvYTDRegion" runat="server" ClientIDMode="Static"  AllowSorting="true">
                                            <HeaderStyle Font-Bold="true" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title">
                                    <i class="fa fa-users"></i>&nbsp
                                    <span class="heading_buttons" id="spnOrderByTeam" runat="server"></span>
                                    <br class="buttons_newline">
                                    <a id="SALES_TEAM_QTR_BTN" class="heading_buttons activeTeamlink selected" href="javascript:Teamclick('QTR','SALES_TEAM_QTR_BTN')">QTR</a>
                                    <a id="SALES_TEAM_YTD_BTN" class="heading_buttons activeTeamlink" href="javascript:Teamclick('YTD','SALES_TEAM_YTD_BTN')">YTD</a>
                                </h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse4">
                                        <%--<span id="">Sales/Order by Team</span></a>
                           <a id="SALES_TEAM_QTR_BTN" class="activeTeamlink" href="javascript:Teamclick('QTR','SALES_TEAM_QTR_BTN')">QTR</a>
                           <a id="SALES_TEAM_YTD_BTN" class="activeTeamlink" href="javascript:Teamclick('YTD','SALES_TEAM_YTD_BTN')">YTD</a>--%>
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse4" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body left_right_no_pad nicescrollX">
                                    <div id="divQTRTeam" clientidmode="Static">
                                        <asp:GridView ID="gvQTRTeam" runat="server" ClientIDMode="Static"  AllowSorting="true"  CssClass="grid">
                                         
                                        </asp:GridView>
                                    </div>
                                    <div id="divYTDTeam" clientidmode="Static">
                                        <asp:GridView ID="gvYTDTeam" runat="server" ClientIDMode="Static"  AllowSorting="true">
                                          
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title">
                                    <i class="fa fa-pie-chart"></i>&nbsp 
                                    <span class="heading_buttons" id="spnOrderUsage" runat="server"></span>
                                    <br class="buttons_newline">
                                    <a id="ORDER_USAGE_QTR_BTN" class="heading_buttons activeOrderlink selected" href="javascript:Orderclick('QTR','ORDER_USAGE_QTR_BTN')">QTR</a>
                                    <a id="ORDER_USAGE_YTD_BTN" class="heading_buttons activeOrderlink" href="javascript:Orderclick('YTD','ORDER_USAGE_YTD_BTN')">YTD</a>
                                </h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse7" id="a3">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse7" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body" style="height: 260px; min-height: 300px;">
                                     <div id="error_msg"></div>
                                    <div id="chart" style="width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title">
                                    <i class="fa fa-certificate"></i>&nbsp 
                                    <span class="heading_buttons" id="spnNewProducts" runat="server"></span>
                                    <br class="buttons_newline">
                                    <a id="NEW_PRODUCTS_QTR_BTN" class="heading_buttons activeNewlink selected" href="javascript:ProductNewclick('QTR','NEW_PRODUCTS_QTR_BTN')">QTR</a>
                                    <a id="NEW_PRODUCTS_YTD_BTN" class="heading_buttons activeNewlink" href="javascript:ProductNewclick('YTD','NEW_PRODUCTS_YTD_BTN')">YTD</a>
                                    <a id="NEW_PRODUCTS_5_BTN" class="heading_buttons activeNewFilterlink selected" href="javascript:ProductNewFilterclick('5','NEW_PRODUCTS_5_BTN')">5</a>
                                    <a id="NEW_PRODUCTS_10_BTN" class="heading_buttons activeNewFilterlink" href="javascript:ProductNewFilterclick('10','NEW_PRODUCTS_10_BTN')">10</a>
                                    <a id="NEW_PRODUCTS_20_BTN" class="heading_buttons activeNewFilterlink" href="javascript:ProductNewFilterclick('20','NEW_PRODUCTS_20_BTN')">20</a>
                                </h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse5" id="a3">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse5" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body">
                                    <div id="divQTRNewProduct" clientidmode="Static">
                                        <asp:GridView ID="gvQTRNewProduct" runat="server" ClientIDMode="Static" AllowSorting="true" CssClass="grid">
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </div>
                                    <div id="divYTDNewProduct" clientidmode="Static">
                                        <asp:GridView ID="gvYTDNewProduct" runat="server" ClientIDMode="Static" AllowSorting="true">
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title">
                                    <i class="fa fa-trophy"></i>&nbsp
                                    <span class="heading_buttons" id="spnTopProducts" runat="server"></span>
                                    <br class="buttons_newline">
                                    <a id="TOP_PRODUCTS_QTR_BTN" class="heading_buttons activeToplink selected" href="javascript:ProductTopclick('QTR','TOP_PRODUCTS_QTR_BTN')">QTR</a>
                                    <a id="TOP_PRODUCTS_YTD_BTN" class="heading_buttons activeToplink" href="javascript:ProductTopclick('YTD','TOP_PRODUCTS_YTD_BTN')">YTD</a>
                                    <a id="TOP_PRODUCTS_5_BTN" class="heading_buttons activeTopFilterlink selected" href="javascript:ProductTopFilterclick('5','TOP_PRODUCTS_5_BTN')">5</a>
                                    <a id="TOP_PRODUCTS_10_BTN" class="heading_buttons activeTopFilterlink" href="javascript:ProductTopFilterclick('10','TOP_PRODUCTS_10_BTN')">10</a>
                                    <a id="TOP_PRODUCTS_20_BTN" class="heading_buttons activeTopFilterlink" href="javascript:ProductTopFilterclick('10','TOP_PRODUCTS_20_BTN')">20</a>
                                </h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse6" id="a3">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse6" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body">
                                    <div id="divQTRTopProduct" clientidmode="Static">
                                        <asp:GridView ID="gvQTRTopProduct" runat="server" ClientIDMode="Static" AllowSorting="true" CssClass="grid">
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </div>
                                    <div id="divYTDTopProduct" clientidmode="Static">
                                        <asp:GridView ID="gvYTDTopProduct" runat="server" ClientIDMode="Static" AllowSorting="true">
                                            <HeaderStyle />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .pop {
    display: list-item;
    max-height: 333px;
    overflow: auto;
}
        .nicescrollX{max-height:none;padding:0;}
        thead, tbody tr {
    display: table;
    table-layout: fixed;
    width: 100%;
}
       
    </style>
    <script>
        $(window).resize(function () {


        });
        $(window).load(function () {
            $('.loaderX').hide();
            //$(document).ready(function () {
            //    $(".dash_progress_parent").animate({ width: '100%' }, 1000);
            //});
        });</script>

    <%--Added By Snehal - 18 03 2017--%>
        <script type="text/javascript">
            var strRegion = "<%=strRegion %>";
            var strTotalOrders = "<%=strTotalOrders %>";
            var strTotalSales = "<%=strTotalSales %>";
            var strTeam = "<%=strTeam %>";
            var strProduct = "<%=strProduct %>";
            var strProductName = "<%=strProductName %>";
            var strUnits = "<%=strUnits %>";
            var strSales = "<%=strSales %>";
            var strNoOrderUsageMsg = "<%=strNoOrderUsageMsg %>";

            </script>
</asp:Content>

