﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Dashboard_PointsTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>--%>
    <script src="<%=host %>JS/jquery-ui.min.js"></script>
    <script src="<%=host %>JS/paging.js"></script>
    <style type="text/css">
        .dashboard table .three_equal_column {
            color: black;
            width: 33.33%;
        }

        .dashboard .top_tables_headings {
            font-size: 36px;
            padding-left: 0;
            padding-right: 0px;
            border-bottom: 2px dashed rgba(0, 0, 0, 0.2);
            padding-bottom: 0.5em;
            margin-bottom: 0;
        }

        table, td, th {
            border: none !important;
        }

        div {
            display: block;
        }

        .nicescrollX {
            max-height: none;
            padding: 0;
        }

        .paging-nav {
            text-align: left;
            padding-top: 2px;
        }

            .paging-nav a {
                margin: auto 1px;
                text-decoration: none;
                display: inline-block;
                padding: 1px 7px;
                background: #91b9e6;
                color: white;
                border-radius: 3px;
            }

            .paging-nav .selected-page {
                background: #187ed5;
                font-weight: bold;
            }

        .sortAsc {
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
            width: 200px;
        }

        .sortDesc {
            background-repeat: no-repeat;
            background-position: center right;
            cursor: pointer;
            width: 200px;
        }

        .grid {
            font-family: Arial;
            font-size: 10pt;
            width: 600px;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            GetUserPointsData()
            GetAllPointsTransactionDetails()
        });

        function sortTable(table, col, reverse) {
            var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
                tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
                i;
            reverse = -((+reverse) || -1);

            tr = tr.sort(function (a, b) { // sort rows


                if (!isNaN(a.cells[col].textContent) && !isNaN(b.cells[col].textContent))
                    return reverse * ((+a.cells[col].textContent) - (+b.cells[col].textContent))
                return reverse // `-1 *` if want opposite order
                     * (a.cells[col].textContent.trim() // using `.textContent.trim()` for test
                         .localeCompare(b.cells[col].textContent.trim())
                        );
            });
            for (i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
        }

        function makeSortable(table) {
            var th = table.tHead, i;
            th && (th = th.rows[0]) && (th = th.cells);
            if (th) i = th.length;
            else return; // if no `<thead>` then do nothing
            while (--i >= 0) (function (i) {
                var dir = 1;
                th[i].addEventListener('click', function () { sortTable(table, i, (dir = 1 - dir)) });
            }(i));
        }

        function makeAllSortable(parent) {
            parent = parent || document.body;
            var t = parent.getElementsByTagName('table'), i = t.length;
            while (--i >= 0) makeSortable(t[i]);
            $(".pop").niceScroll();
        }


        function GetUserPointsData() {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetUserPointsData',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: true,
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);
                        $("#spnTotalPoints").closest('.panel-default').find('.loaderX').hide()
                        $("#spnTotalPoints").html(json[0].points);
                        $("#spnBdayPoints").html(json[0].Birthday);
                        $("#spnAnvPoints").html(json[0].Anniversary);
                        $("#spnBDate").html(json[0].Pointsbirth_date);
                        $("#spnADate").html(json[0].PointsHire_Date);
                    }
                })
            }
            catch (e) {
            }
        }

        function GetAllPointsTransactionDetails() {
            try {

                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/PointsTransaction.aspx/GetPointsTransactionDetails',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () { },
                    success: function (data) {
                        $("#gvPointsTransaction").closest('.panel-default').find('.loaderX').hide()
                        $("#gvPointsTransaction").empty();
                        $("#gvPointsTransaction").append("<thead><tr><th class='sales_column_1'>"+strDate+"<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_2'>"+strEvent+"<div class='sort_icon'><i class='fa fa-sort'></i></div></th><th class='sales_column_3'>"+strPoints+"<div class='sort_icon'><i class='fa fa-sort'></i></div></th></tr></thead>");
                        $("#gvPointsTransaction").append('<tbody id="appendX"></tbody>')
                        for (var i = 0; i < data.d.length; i++) {
                            $("#appendX").append("<tr><td>" + data.d[i].dateProcessed + "</td><td>" + data.d[i].Reason + "</td><td>" + data.d[i].points + "</td></tr>");
                        }
                        $('#gvPointsTransaction').paging({
                            limit: 5,
                            rowDisplayStyle: 'block',
                            activePage: 0,
                            rows: []
                        });
                        $('#gvPointsTransaction').next().prependTo('.tableSet');

                        $('.paging-nav a:nth-child(2)').addClass('selected-page');
                        makeAllSortable();

                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            catch (e) {
            }
        }

    </script>

    <div id="div1" runat="server" class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 id="ContentPlaceHolder1_spnHeading" class="pageTitle"><span id="spnHeading" runat="server"></span></h1>
            </div>
        </div>

        <div class="row dashboard">
            <div class="col-xs-12">
                <div class="my-account-back-link">
                    <a href="<%=host %>Dashboard/MyAccount2.aspx"><i class="fa fa-long-arrow-left"></i><span id="spnBackToMyAcc" runat="server"></span></a>
                </div>
            </div>
            <div class="col-xs-12">
                <div id="POINT_TRANSACTIONS_TOP" class="col-xs-12 tableSet">
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading textless_panel" role="tab" id="headingOne"></div>
                        <div class="panel-body left_right_no_pad tablesorter">
                            <div class="col-xs-12 col-md-4">
                                <div class="three_equal_column ">
                                    <h1 class="top_tables_headings"><span id="spnTotalPoints"></span>pt(s)</h1>
                                </div>
                                <div><span id="spnTotalBalance" runat="server"></span></div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="three_equal_column">
                                    <h1 class="top_tables_headings"><span id="spnBdayPoints"></span>pt(s)</h1>
                                </div>
                                <div><span id="spnBirthdayAward" runat="server"></span> on <span id="spnBDate"></span></div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <div class="three_equal_column">
                                    <h1 class="top_tables_headings"><span id="spnAnvPoints"></span>pt(s)</h1>
                                </div>
                                <div><span id="spnAnnvAward" runat="server"></span> on <span id="spnADate"></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="POINT_TRANSACTIONS_BOTTOM" class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"><i class="fa fa-star"></i><span class="heading_buttons" id="spnPonitsTrans" runat="server"></span></h4>
                    </div>
                    <div id="POINT_TRANSACTIONS_COLLAPSE" class="panel-collapse collapse in">
                        <div class="panel-body nicescrollX">
                            <div id="Points">
                                <asp:GridView ID="gvPointsTransaction" runat="server" ClientIDMode="Static" CssClass="grid">
                                    <HeaderStyle Font-Bold="true" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
     <%--Added By Snehal - 18 03 2017--%>
        <script type="text/javascript">
            var strDate = "<%=strDate %>";
            var strEvent = "<%=strEvent %>";
            var strPoints = "<%=strPoints %>";
            </script>
</asp:Content>

