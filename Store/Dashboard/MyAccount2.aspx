﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/MasterPage.master" AutoEventWireup="true" Inherits="Presentation.Dashboard_MyAccount2" %>

<script runat="server">

</script>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 
    <script>
        $(window).load(function () {
            GetContactInformation()
            GetOrderHistory()
            GetCustomerAddressCount()
            GetUserPointsData()
            GetBudgetDetails()
        });

        $(document).on('click', '#aMarket', function () {
            return UpdateMarketPreference();
        });

        function UpdateMarketPreference() {
            try {
                if ($("#rdProfile_MarketingOption1").is(":checked")) {
                    var val = true;
                }
                if ($("#rdProfile_MarketingOption2").is(":checked")) {
                    var val = false;
                }

                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/UpdateMarketPreference',
                    dataType: 'JSON',
                    data: "{'option': '" + val + "'}",
                    //data: JSON.stringify({ pageNo: pageNo }),
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,

                }).success(function (data) {
                    alert(data.d);
                })
            }
            catch (e) {
            }
        }


        function GetContactInformation() {

            var emailid = '<%=emailid %>';
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetContactInformation',
                    dataType: 'JSON',
                    //data: "{'emailid': '" + emailid + "'}",
                    //data: JSON.stringify({ pageNo: pageNo }),
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {
                        //alert('geeti');
                    }
                }).done(function (data) {
                    if (data.d != "[]") {
                        var json = $.parseJSON(data.d);

                        $("#spnName").closest('.panel-default').find('.loaderX').hide()
                        $("#divContactInfo").addClass('dispalyBlock');
                        $("#divNoContactInfo").addClass('displayNone');
                        //alert($("#spnName").closest('.loaderX').html())
                        $("#spnName").html(json[0].Name);
                        $("#spnAddLine1").html(json[0].Address_Line_1);
                        $("#spnAddLine3").html(json[0].AddLine3);
                        $("#spnEmailid").html(json[0].Work_Email);
                        $("#spnWorkPhoneNumber").html(json[0].Work_PhoneNumber);
                        $("#hrefBillingAddressBook").attr("href", '<%=host%>' + 'Dashboard/AddressBook.aspx?mode=B')
                    }
                    else
                    {                        
                            $("#spnName").closest('.panel-default').find('.loaderX').hide();
                            $("#divNoContactInfo").toggleClass('dispalyBlock , panel_fixed_height');
                            $("#spnNoContactInfo").html(strNoContactInfo);
                            $("#divContactInfo").toggleClass('displayNone');
                            $("#divNoContactInfo").removeClass('displayNone');
                    }
                })
            }
            catch (e) {

                $("#spnName").closest('.panel-default').find('.loaderX').hide();
                $("#divNoContactInfo").toggleClass('dispalyBlock , panel_fixed_height');
                $("#spnNoContactInfo").html(strNoContactInfo);
                $("#divContactInfo").toggleClass('displayNone');
                $("#divNoContactInfo").removeClass('displayNone');
            }
        }

        function GetOrderHistory() {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetCustomerOrdersCount',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {

                    }
                }).done(function (data) {
                    $("#spnOrderCount").closest('.panel-default').find('.loaderX').hide();
                    $("#spnOrderCount").html(data.d);
                    $("#hrefOrderHistory").attr("href", '<%=host %>' + 'MyAccount/OrderHistory')
                })
            }
            catch (e) {
            }
        }

        function GetCurrencySymbol(Curr) {
            var currency_symbols = {
                'USD': '$', // US Dollar
                'EUR': '€', // Euro
                'DKK': 'DKK', // Danish krone
                'GBP': '£', // British Pound Sterling
                'NOK': 'NOK', // Norwegian krone
                'SEK': 'SEK', // Swedish krona
                'CHF': 'C', // Swiss franc
                'TRY': '₤', // Turkish new lira
                'GERMANYEUR': '€', // German Euro
                'GERMANYSEK': 'SEK', // German Krona
                'JPY': '¥', // Japanese Yen          
                'ILS': '₪', // Israeli New Sheqel
                'INR': '₹', // Indian Rupee
                'KRW': '₩', // South Korean Won
                'NGN': '₦', // Nigerian Naira
                'PHP': '₱', // Philippine Peso
                'PLN': 'zł', // Polish Zloty
                'PYG': '₲', // Paraguayan Guarani
                'THB': '฿', // Thai Baht
                'UAH': '₴', // Ukrainian Hryvnia
                'VND': '₫', // Vietnamese Dong
                'CRC': '₡', // Costa Rican Colón
            };
            var currency_name = Curr;
            return currency_symbols[currency_name];
        }

        //function GetMonthName(monthNumber) {
        //    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        //    return months[monthNumber - 1];
        //}

        function GetMonthName(monthnumber) {
            if (monthnumber != "") {
                 var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                return months[monthnumber - 1];
            }
            else
            {
                return "";
            }
        }

        function GetLastDateOfMonth() {
            var date = new Date();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var lastDate = (lastDay.getDate());
            return lastDate;
        }

        function GetBudgetDetails() {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetBudgetDetails',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {

                    }
                }).done(function (data) {

                    if (data.d != "") {
                        $("#spnTotal").closest('.panel-default').find('.loaderX').hide();
                        $("#divBudget").addClass('dispalyBlock');
                        $("#divNoBudget").addClass('displayNone');
                        //var Btotal = 0;
                        //var Bspent=0;
                        //var Bpending = 0;
                        //var Bavailable=0;
                        //for (var i = 0; i < data.d.length; i++) {
                        //    Btotal = parseFloat(Btotal) + parseFloat(data.d[i].WebCurrTotalBudget);
                        //    Bspent = parseFloat(Bspent) + parseFloat(data.d[i].WebCurrOrdersInvoiced);
                        //    Bspent = parseFloat(Bspent) + parseFloat(data.d[i].WebCurrOrdersNotInvoiced);
                        //    Bavailable = parseFloat(Bavailable) + parseFloat(data.d[i].WebCurrOutstandingBudget);
                        //}
                        var json = $.parseJSON(data.d);

                        $("#spnTotal").html(GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrTotalBudget)
                        $("#spnExpiration").html(GetMonthName(json[0].EndPeriod) + ' ' + GetLastDateOfMonth() + ', ' + json[0].YearEnd)
                        $("#spnSpent").html(GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOrdersInvoiced)
                        $("#dvSpent").attr('data-original-title', strSpent + ":" + GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOrdersInvoiced);

                        $("#spnPending").attr('data-original-title', strPending + ":" + GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOrdersNotInvoiced)
                        $("#spnAvailable").attr('data-original-title', strAvailable + ":" + GetCurrencySymbol(json[0].WebCurrencySymbol) + json[0].WebCurrOutstandingBudget)
                        $(".total").width(Math.ceil((json[0].WebCurrTotalBudget / json[0].WebCurrTotalBudget) * 100) + '%');
                        var spent = (json[0].WebCurrOrdersInvoiced / json[0].WebCurrTotalBudget) * 100;
                        var pending = (json[0].WebCurrOrdersNotInvoiced / json[0].WebCurrTotalBudget) * 100;
                        var available = (json[0].WebCurrOutstandingBudget / json[0].WebCurrTotalBudget) * 100;

                        var spent1, pending1, available1;
                        if (spent < 1) {
                            spent1 = Math.ceil((json[0].WebCurrOrdersInvoiced / json[0].WebCurrTotalBudget) * 100);
                        }
                        else {
                            spent1 = Math.floor((json[0].WebCurrOrdersInvoiced / json[0].WebCurrTotalBudget) * 100);
                        }

                        if (pending < 1) {
                            pending1 = Math.ceil((json[0].WebCurrOrdersNotInvoiced / json[0].WebCurrTotalBudget) * 100);
                        }
                        else {
                            pending1 = Math.floor((json[0].WebCurrOrdersNotInvoiced / json[0].WebCurrTotalBudget) * 100);
                        }

                        if (available < 1) {
                            available1 = Math.ceil((json[0].WebCurrOutstandingBudget / json[0].WebCurrTotalBudget) * 100);
                        }
                        else {
                            available1 = Math.floor((json[0].WebCurrOutstandingBudget / json[0].WebCurrTotalBudget) * 100);
                        }

                        var max;
                        if (spent1 + pending1 + available1 > 100) {
                            max = Math.max(spent, pending, available);
                        }

                        if (spent1 == max) {

                            $(".spent").width(spent1 - 1 + '%');
                        }
                        else {
                            $(".spent").width(Math.ceil(spent1) + '%');
                        }

                        if (pending1 == max) {
                            $(".pending").width((pending1 - 1) + '%');
                        }
                        else {
                            $(".pending").width(Math.ceil(pending1) + '%');
                        }
                        if (available1 == max) {
                            $(".available").width((available1 - 1) + '%');
                        }
                        else {
                            $(".available").width(Math.ceil(available1) + '%');
                        }

                        $("#hrefBudgetTran").attr("href", '<%=host%>' + 'Dashboard/BudgetTransaction.aspx')
                    }
                    else {
                        $("#spnTotal").closest('.panel-default').find('.loaderX').hide();
                        $("#divNoBudget").toggleClass('dispalyBlock , panel_fixed_height');
                        $("#spnNoBudget").html(strNoBudgetMsg);
                        $("#divBudget").toggleClass('displayNone');
                        $("#divNoBudget").removeClass('displayNone');
                    }
                })
            }
            catch (e) {
                $("#spnTotal").closest('.panel-default').find('.loaderX').hide();
                $("#divBudget").removeClass('dispalyBlock');
                $("#divNoBudget").removeClass('displayNone');
                $("#divNoBudget").toggleClass('dispalyBlock , panel_fixed_height');
                $("#spnNoBudget").html(strNoBudgetMsg);
                $("#divBudget").toggleClass('displayNone');         
            }
        }

        function GetCustomerAddressCount() {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetCustomerAddressCount',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {
                        //alert('geeti');
                    }
                }).done(function (data) {
                    $("#spnAddrsCount").closest('.panel-default').find('.loaderX').hide();
                    $("#spnAddrsCount").html(data.d);
                    $("#hrefAddressBook").attr("href", '<%=host%>' + 'Dashboard/AddressBook.aspx?mode=S')
                })
            }
            catch (e) {
            }
        }

        function GetUserPointsData() {
            try {
                $.ajax({
                    type: 'POST',
                    url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/GetUserPointsData',
                    dataType: 'JSON',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    beforeSend: function () {
                        //alert('geeti');
                    }
                }).done(function (data) {
                    if (data.d != "") {
                        $("#spnBdayPoint").closest('.panel-default').find('.loaderX').hide();
                        $("#divPoints").addClass('dispalyBlock');
                        $("#divNoPoints").addClass('displayNone');
                        var json = $.parseJSON(data.d);
                        $("#spnPointCount").html(json[0].points);
                        $("#spnBdayPoint").html(json[0].Birthday);
                        $("#spnAnvPoint").html(json[0].Anniversary);
                        $("#spnbDate").html(json[0].birth_date);
                        $("#spnADate").html(json[0].Hire_Date);
                        $("#hrefPointTran").attr("href", '<%=host%>' + 'Dashboard/PointsTransaction.aspx')

                    }
                    else
                    {
                        $("#spnBdayPoint").closest('.panel-default').find('.loaderX').hide();
                        $("#divNoPoints").toggleClass('dispalyBlock , panel_fixed_height');
                       // $("#spnNoPoints").html(strNoPointsMsg);
                        $("#spnNoPoints").html('No Points allocated for you');
                        $("#divPoints").toggleClass('displayNone');
                        $("#divNoPoints").removeClass('displayNone');
                    }
                })
            }
            catch (e) {
                $("#spnBdayPoint").closest('.panel-default').find('.loaderX').hide();
                $("#divPoints").removeClass('dispalyBlock');
                $("#divNoPoints").removeClass('displayNone');
                $("#divNoPoints").toggleClass('dispalyBlock , panel_fixed_height');
                $("#spnNoPoints").html('No Points allocated for you');
               // $("#spnNoPoints").html(strNoPointsMsg);
                $("#divPoints").toggleClass('displayNone');
            }
        }


        $(function () {
            $("#btnPassUpdate").click(function () {
                if (Page_IsValid) {
                    var oldpass = $('.oldpassword').val();
                    var newpass = $('.newpassword').val();
                    try {
                        $.ajax({
                            type: 'POST',
                            url: '<%=host %>' + 'Dashboard/MyAccount2.aspx/UpdatePassword',
                            dataType: 'JSON',
                            data: "{'oldpass': '" + oldpass + "','newpass': '" + newpass + "'}",
                            contentType: 'application/json;charset=utf-8',
                            cache: false,
                            async: false,

                        }).done(function (data) {
                            if (data.d == 0) {
                                $('#myWarningModal').find('#WarningMessage').html(strIncorrectPass);
                                $('#myWarningModal').modal('show');
                            }
                            else {
                                if (data.d == 1) {

                                    $('#mySuccessModal').find('#SuccessMessage').html(strPassUpdate);
                                    $('#mySuccessModal').modal('show');
                                }
                                else {
                                    $('#myErrorModal').find('#ErrorMessage').html(strErrorPass);
                                    $('#myErrorModal').modal('show');
                                }
                            }
                        })
                    }
                    catch (e) {
                    }
                }
            });
        });


    </script>


    <div id="div1" runat="server" class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 id="ContentPlaceHolder1_spnHeading" class="pageTitle"><span id="spnHeading" runat="server"></span></h1>
            </div>
        </div>
        <div class="row dashboard">
            <div class="col-xs-12">
                <%--<div class="row">--%>
                    <div class="col-sm-6 ">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-user"></i>&nbsp <span id="spnContactInformation" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse1" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse1" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div id="divNoContactInfo" class="panel-body displayNone">
     <p><span id="spnNoContactInfo" runat="server" clientidmode="Static"></span></p>
    </div>
                                <div id="divContactInfo" class="panel-body panel_fixed_height">
                                    <p><span id="spnName"></span></p>
                                    <p><span id="spnAddLine1"></span></p>
                                    <p><span id="spnAddLine3"></span></p>
                                    <p><span id="spnEmailid"></span></p>
                                    <p><span id="spnWorkPhoneNumber"></span></p>
                                    <p class="panel_bottom"></p>
                                    <p class="panel_bottom"><a id="hrefBillingAddressBook" class="btn btn-primary customActionBtn" href="#"><span id="spnBillingAddress" runat="server"></span></a></p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6" id="budgetDiv">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-user"></i>&nbsp  <span id="spnBudget" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse7" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse7" runat="server" class="panel-collapse collapse in" clientidmode="Static">
<div id="divNoBudget" class="panel-body displayNone">
     <p><span id="spnNoBudget" runat="server" clientidmode="Static"></span></p>
    </div>
                                <div id="divBudget" class="panel-body panel_fixed_height">
                                    <p><span id="Span1" runat="server">Total Budget</span></p>
                                    <p><span id="spnCurrBudget" runat="server"></span></p>
                                    <h5 id="spnTotal"></h5>
                                    <p><span id="spnExpirationText" runat="server"></span>:<span id="spnExpiration"></span></p>                                   
                                    <div class="dash_progress_parent total">
                                        <div class="dash_progress budget_left spent" data-toggle="tooltip" id="dvSpent"></div>
                                        <div class="dash_progress budget_centre pending" data-toggle="tooltip" id="spnPending"></div>
                                        <div class="dash_progress budget_right available" data-toggle="tooltip" id="spnAvailable"></div>
                                    </div>                                   
                                    <p class="panel_bottom" style="margin: 0 0 0 0;"><a id="hrefBudgetTran" class="btn btn-primary customActionBtn"><span id="spnBudgetTrans" runat="server"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>

               <%-- </div>--%>

                <div class="row" style="display: none;">

                    <div class="col-sm-6 ">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-star"></i>&nbsp <span id="spnPoints" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse2" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse2" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div id="divNoPoints" class="panel-body displayNone">
     <p><span id="spnNoPoints" runat="server" clientidmode="Static"></span></p>
    </div>
                                <div id="divPoints" class="panel-body panel_fixed_height">
                                    <p><span id="spnPointsText" runat="server"></span></p>
                                    <h5><span id="spnPointCount"></span>pts</h5>
                                    <p>
                                        <span id="spnBdayAward" runat="server"></span>: <span id="spnBdayPoint"></span>pts on <span id="spnbDate"></span>
                                        <br>
                                        <span id="spnAnvAward" runat="server"></span>: <span id="spnAnvPoint"></span>pts on <span id="spnADate"></span>
                                    </p>
                                    <p class="panel_bottom"><a id="hrefPointTran" class="btn btn-primary customActionBtn"><span id="spnPointsTrans" runat="server"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="panel panel-default">
                            <%--<div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>--%>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-star"></i>&nbsp<span id="spnApproval" runat="server"></span> Approvals</h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse8" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse8" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body panel_fixed_height">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



               <%-- <div class="row">--%>
                    <div class="col-sm-6 ">

                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-history"></i>&nbsp <span id="spnOrderHistory" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse3" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse3" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body panel_fixed_height">
                                    <p><span id="spnOrder1" runat="server"></span></p>
                                    <p>
                                        <h5 id="spnOrderCount"></h5>
                                    </p>
                                    <p><span id="spnOrder2" runat="server"></span></p>
                                    <p class="panel_bottom"><a id="hrefOrderHistory" href="#" class="btn btn-primary customActionBtn"><span id="spnViewOrder" runat="server"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="panel panel-default">
                            <div class="loaderX"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-user"></i>&nbsp <span id="spnAddressBook" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse4" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse4" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body panel_fixed_height">
                                    <p><span id="spnAddrs1" runat="server"></span></p>
                                    <p>
                                        <h5 id="spnAddrsCount"></h5>
                                    </p>
                                    <p><span id="spnAddrs2" runat="server"></span></p>
                                    <p class="panel_bottom"><a id="hrefAddressBook" class="btn btn-primary customActionBtn" href="#"><span id="spnViewAddress" runat="server"></span></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <%--</div>
                <div class="row">--%>
                    <div class="col-sm-6 ">
                        <div class="panel panel-default">

                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-envelope"></i>&nbsp <span id="spnMarketPre" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse5" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse5" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body panel_fixed_height">
                                    <div class="card_form">
                                        <div class="form-group">
                                            <div class="radio">
                                                <asp:RadioButton ClientIDMode="Static" GroupName="radio" ID="rdProfile_MarketingOption1" runat="server"></asp:RadioButton>
                                            </div>
                                            <div class="radio">
                                                <asp:RadioButton ClientIDMode="Static" GroupName="radio" ID="rdProfile_MarketingOption2" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <p class="panel_bottom"><a class="btn btn-primary customActionBtn" id="aMarket" href="#"><span id="spnUpdate" runat="server"></span></a></p>
                                    <%--                    <p class="panel_bottom">
                                        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" /></p>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="panel panel-default" id="divChangePass" runat="server">

                            <div class="panel-heading panelHeading">
                                <h4 class="panel-title"><i class="fa fa-key"></i>&nbsp <span id="spnChangePassword" runat="server"></span></h4>
                                <div class="collapse_triggers">
                                    <a data-toggle="collapse" href="#collapse6" id="">
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div id="collapse6" runat="server" class="panel-collapse collapse in" clientidmode="Static">
                                <div class="panel-body panel_fixed_height">
                                    <div class="card_form">
                                        <div class="form-group clearfix row">
                                            <asp:Label ID="lblOldPassword" runat="server" class="col-sm-3 col-md-4 control-label customLabel" for="name"></asp:Label>
                                            <div class="col-sm-9 col-md-8">
                                                <asp:TextBox ID="txtOldPass" runat="server" ClientIDMode="Static" class="form-control customInput oldpassword" TextMode="Password"></asp:TextBox>

                                                <asp:RequiredFieldValidator ValidationGroup="Group" ID="RequiredFieldValidator1" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtOldPass" class="text-danger" runat="server" ErrorMessage="Please Enter Password"></asp:RequiredFieldValidator>
                                                <%--<span id="spnOldPass" class="text-danger" style="display:none;">Please Enter Password</span>--%>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix row">
                                            <asp:Label ID="lblNewPassword" runat="server" class="col-sm-3 col-md-4 control-label customLabel " for="name"></asp:Label>
                                            <div class="col-sm-9 col-md-8">
                                                <asp:TextBox ID="txtNewPass" ClientIDMode="Static" runat="server" class="form-control customInput newpassword" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="Group" ID="RequiredFieldValidator2" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtNewPass" class="text-danger" runat="server" ErrorMessage="Please Enter New Password"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ValidationGroup="Group" Display="Dynamic" ControlToValidate="txtNewPass" class="text-danger" ID="RegularExpressionValidator2" ValidationExpression="^[\s\S]{8,}$" runat="server" ErrorMessage="Minimum 8 characters required."></asp:RegularExpressionValidator>
                                                <%--<asp:RangeValidator Type="Integer" ID="RangeValidator1" ControlToValidate="txtNewPass" MinimumValue="8" MaximumValue="250" runat="server" ErrorMessage="Password should contain minimum 8 characters"></asp:RangeValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix row">
                                            <asp:Label ID="lblConfPassword" runat="server" class="col-sm-3 col-md-4 control-label customLabel " for="name"></asp:Label>
                                            <div class="col-sm-9 col-md-8">
                                                <asp:TextBox ID="txtConfirmPass" ClientIDMode="Static" runat="server" class="form-control customInput confpassword" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ValidationGroup="Group" ID="RequiredFieldValidator3" Display="Dynamic" class="text-danger" SetFocusOnError="true" ControlToValidate="txtConfirmPass" runat="server" ErrorMessage="Please Enter Confirm Password"></asp:RequiredFieldValidator>
                                                <asp:CompareValidator ValidationGroup="Group" ID="CompareValidator1" Display="Dynamic" class="text-danger" SetFocusOnError="true" runat="server" ControlToCompare="txtNewPass" ControlToValidate="txtConfirmPass" ErrorMessage="Password Mismatch"></asp:CompareValidator>

                                                <%--<span id="spnConfPass" class="text-danger" style="display:none;">Please Enter Confirm Password</span>
                                    <span id="spnConfMisMatch" class="text-danger" style="display:none;">Password Mismatch</span>--%>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix row">
                                            <span id="" class="col-sm-12 col-md-9 control-label customLabel"></span>
                                        </div>
                                    </div>
                                    <p class="panel_bottom">
                                        <asp:Button ValidationGroup="Group" ID="btnPassUpdate" class="btn btn-primary customActionBtn" runat="server" ClientIDMode="Static" />
                                        <%--<p class="panel_bottom"><a class="btn btn-primary ValidationGroup="Group" customActionBtn" id="btnPassUpdate"  href='javascript:void(0);'>Update</a></p>--%>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <%--</div>--%>
            </div>
        </div>
    </div>
    <style>
        .btn-primary {
            background-color: #4072ff !important;
            color: #fff !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>


    <script type="text/javascript">
        var strSpent = "<%=strSpent %>";
        var strPending = "<%=strPending %>";
        var strAvailable = "<%=strAvailable %>";
        var strIncorrectPass = "<%=strIncorrectPass %>";
        var strPassUpdate = "<%=strPassUpdate %>";
        var strErrorPass = "<%=strErrorPass %>";
        var strNoBudgetMsg = "<%=strNoBudgetMsg %>";
        var strNoContactInfo = "<%=strNoContactInfo %>";
        var IsBudgetActive = "<%=IsBudgetActive %>";
        
        
        
    </script>
        <script>
            $(document).ready(function () {
                debugger;
                $(".dash_progress_parent").animate({ width: '100%' }, 1000);
                if (IsBudgetActive == "False") {
                  
                    $('#budgetDiv').removeClass('budgetDivDispaly')
                    $('#budgetDiv').addClass('budgetDiv')

                }
            });
    </script>
    <style>
        .budgetDivDispaly{display:block;}
        .budgetDiv{display:none;}
    </style>

</asp:Content>




